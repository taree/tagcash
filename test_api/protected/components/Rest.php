<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rest
 *
 * @author girishr
 */
class Rest {

    public $_content_type = "application/json";
    private $_code = 200;

    public static function sendResponse($status, $body) {
        $rest = new Rest();
        // set the status
        $rest->_code = ($status) ? $status : 200;
        // set the headers
        $rest->setHeaders();
        // send the body
        echo $body;
        Yii::app()->end();
    }

    private function _getStatusCodeMessage() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    private function setHeaders() {
        header("HTTP/1.1 " . $this->_code . " " . $this->_getStatusCodeMessage());
        header("Content-Type:" . $this->_content_type);
    }

    public static function quickResponse($code, $msg) {
        $response = array();
        $response['error'] = $msg;
        $response['status'] = 'success';

        if($code != 200){
            $response['status'] = 'failed';
        }

        Rest::sendResponse($code, json_encode($response, JSON_NUMERIC_CHECK));
        self::sendResponse($status, $body);
    }

}

?>
