<?php

class Common {

	public static function displayError($message) {
		echo "<div class='error-flash'>" . $message . "</div>";
	}

	public static function displaySuccess($message) {
		echo "<div class='success-flash'>" . $message . "</div>";
	}

	public static function displayWarning($message) {
		echo "<div class='warning-flash'>" . $message . "</div>";
	}

	public static function displayInfo($message) {
		echo "<div class='info-flash'>" . $message . "</div>";
	}

	public static function displayFlash($messageArray) {
		if ($messageArray) {
			foreach ($messageArray as $key => $message) {
				if ($key == 'success') {
					self::displaySuccess($message);
				} elseif ($key == 'notice') {
					self::displayWarning($message);
				} elseif ($key == 'error') {
					self::displayError($message);
				} elseif ($key == 'info') {
					self::displayInfo($message);
				}
			}
		}
	}

	public static function pre($obj, $exit = false) {
		echo "<pre>";
		print_r($obj);
		echo "</pre>";
		if ($exit == true) {
			exit();
		}
	}
	
	
	public static function showFile($fileName,$content,$mimeType=null,$terminate=true){
			if($mimeType===null)
			{
					if(($mimeType=CFileHelper::getMimeTypeByExtension($fileName))===null)
							$mimeType='text/plain';
			}
			header('Pragma: public');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header("Content-type: $mimeType");
			if(ob_get_length()===false)
					header('Content-Length: '.(function_exists('mb_strlen') ? mb_strlen($content,'8bit') : strlen($content)));
			header("Content-Disposition: inline; filename=\"$fileName\"");
			header('Content-Transfer-Encoding: binary');

			if($terminate)
			{
					// clean up the application first because the file downloading could take long time
					// which may cause timeout of some resources (such as DB connection)
					Yii::app()->end(0,false);
					echo $content;
					exit(0);
			}
			else
					echo $content;
	}


	public static function disableYiiLog(){
		foreach (Yii::app()->log->routes as $route){
			if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute)
			{
					$route->enabled = false;
			}
		} 
	}
	
	public static function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	public static function convertToBoolean($val){
		if($val){
			return true;
		} 
		return false;
	}

	public static function convertForOutput($inputArr){
		$outputArr = array();
		foreach ($inputArr as $key => $value) {
			if($key == 'user_id'){
				$newkey = $key;
			}
			else if($key == 'community_id'){
				$newkey = $key;
			}
			else if(preg_match('/^user_/', $key)){
				$newkey = preg_replace('/^user_/', '', $key);
			}
			else{
				$newkey = $key;
			}

			$outputArr[$newkey] = $inputArr[$key];
		}

		return $outputArr;
	}

	public static function randomString($length){
		$outputArr = array();
		$chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
		shuffle($chars);
		$random_string = implode(array_slice($chars, 0, $length));
		return $random_string;
	}
	
	public static function isValidLatitude($latitude){
		if (preg_match("/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/", $latitude)) {
			return true;
		}
		else {
			return false;
		}
	}

	public static function isValidLongitude($longitude){
		if(preg_match("/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/",
			$longitude)) {
			return true;
		} 
		else {
			return false;
		}
	}

	public static function in_array_r($needle, $haystack, $strict = false) {
		foreach ($haystack as $item) {
			if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && SELF::in_array_r($needle, $item, $strict))) {
				return true;
			}
		}
		return false;
	}

	public static function getCurrentTime($country_id){
		$country_code = Countries::getCountryName($country_id,true);
		$zones = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $country_code);
		$now = new DateTime("now", new DateTimeZone($zones[0]));
		return $now->format('Y-m-d H:i:s');

		/*$country = str_replace(' ', '', $country);
		$city = str_replace(' ', '', $city);
		$geocode_stats = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=$city+$country,&sensor=false");
		$output_deals = json_decode($geocode_stats);
		$latLng = $output_deals->results[0]->geometry->location;
		$lat = $latLng->lat;
		$lng = $latLng->lng;    
		$google_time = file_get_contents("https://maps.googleapis.com/maps/api/timezone/json?location=$lat,$lng&timestamp=1331161200&key=AIzaSyAtpji5Vk271Qu6_QFSBXwK7wpoCQLY-zQ");
		$timez = json_decode($google_time);
		$d = new DateTime("now", new DateTimeZone($timez->timeZoneId));
		return  $d->format('H : i: s');*/
	}
        /*
         * 
         * function for trasnfering tagcoin from address A to B
         * author : Deepu Thomas
         */
        public static function tagCoinTrasfer($fromAdd,$toAdd,$tagcoinAmount){
        // determine if the sender has enough balance on his/her wallet
        $tagCoinRpcObj= new BitcoinServiceProvider();
        $balance = floatval($tagCoinRpcObj->getBalance($fromAdd));
        $result = array();
        if( $balance < $tagcoinAmount ){
            return (array('error'=>true,'code' => 1134,'message'=>'not_enough_coin_error_code_code_T.C_LB'));
        }
        $bitcoin  = new BitcoinServiceProvider();
        $sender = $fromAdd;
        // beneficiary user
        $beneficiaryUserId = $toAdd;
       // retrieve a new wallet address to begin the transfer
        $beneficiaryWalletAddress = $bitcoin->getaccountaddress($beneficiaryUserId);
        // $transactionId = $bitcoin->sendTo($beneficiaryWalletAddress,$tagcoinAmount);
        $transactionId = $bitcoin->sendfrom($sender,$beneficiaryWalletAddress,$tagcoinAmount);
        // grab the transfer fee
        $transferFee = abs($bitcoin->gettransactionfee($transactionId));
        // charge the transaction fee
        $res=$bitcoin->move('SYSTEM', $sender, $transferFee);
        if($res == true){
         return array('error'=>false,'message'=>'tagcoin_trasaction_successful');  
        }else{
            $actualError=$bitcoin->error; 
           return array('error'=>true,'message'=>'trasaction_failed_please_contact_support_error_code_TC.failed.201');
        }
        }
        /*
         * 
         * tagcoin rate for given wallet id
         * ie: given wallet id to corresponding tag value
         * author : Deepu Thomas
         * 
         */
        public static function tagCoinRate($walletId){
        $tagCoinValueForGivenWallet = RewardTagValue::model()->findByAttributes(array('wallet_type' => $walletId));
        if ($tagCoinValueForGivenWallet !== null)
            $tagCoinValueForGivenWallet = floatval($tagCoinValueForGivenWallet->tag_value) / floatval($tagCoinValueForGivenWallet->unit);
        else
            $tagCoinValueForGivenWallet= null;  
        return $tagCoinValueForGivenWallet;
        }
        /*
         * 
         * get default preference of user and community
         * @author : Deepu Thomas
         * 
         */
        public static function getPerference($id=0,$type=1){
        if($type ==1 ){
            $preferences=  UserPreferences::model()->findByPk($id);
        }
        if($type ==2){
            $preferences= CommunityPreferences::model()->findByPk($id);
        }
        if($preferences->attributes){
            return $preferences->attributes;
            }else{
                return FALSE;
            }
        }
        /*
         * 
         * get default wallet of specific currency of system, if not exising create new one for system
         * 
         */
        public static function getSystemDefaultWallet($walletTypeId,$adminId,$autoCreate=true){
            $wallet=  WalletBalances::model()->with('walletType')->findByAttributes(array(
                       'balance_type'=>3,
                       'wallet_type_id'=>$walletTypeId,
                       'balance_id'=>$adminId
                       
                    ));
            if($wallet)
                return true;
            if(!$wallet && $autoCreate){
            $walletInfo=  WalletTypes::model()->findByPk($walletTypeId);            
            if($walletInfo && $walletInfo->wallet_type == 0){ //allowing only normal wallet
            $walletBalance= new WalletBalances;
            $walletBalance->balance_id=$adminId;
            $walletBalance->balance_type=3;
            $walletBalance->balance_amount=0;
            $walletBalance->wallet_type_id=$walletTypeId;
            if($walletBalance->save(false)){
            return true;
            }else{
              return false;   
            }
            
            }else{
                return false;
            }
            }elseif($autoCreate ==false){
                return false;
            }
            
        }
        /*
         * 
         * retunrs fb_id from chatfuel
         * 
         */
public static function getFbId(){
   $fb_id=Yii::app()->request->getPost('fb_id');
   $id= $fb_id ? $fb_id : Yii::app()->request->getPost('chatfuel_user_id');
   return $id;
}
     /*
     * 
     * returns name of given id
     * @author : Deepu Thomas
     * 
     */

    public static function getName($id = 0, $type = 1) {
        if ($type == 1) {
            $user = Users::model()->findByPk($id);
            if ($user) {
                $name = $user->user_firstname . ' ' . $user->user_lastname;
                $e = explode('@', $user->user_email);
                $name = trim($name) ? $name : $e[0];
            }
        } else if ($type == 2) {
            $community = Communities::model()->findByPk($id);
            if ($community) {
                $name = $community->community_name;
                $e = explode('@', $community->community_email);
                $name = trim($name) ? $name : $e[0];
            }
        }
        return $name;
    }

}

?>
