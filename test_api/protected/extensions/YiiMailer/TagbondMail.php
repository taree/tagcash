<?php

class TagbondMail {
	public function sendTagbondMail($settings=array(), $data=array(), $response=array(),$multiple = null){
		$mail = new YiiMailer();
		$mail->IsSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$sitesettings = SiteSettings::model()->find(array("select"=>"admin_email,admin_email_password,web_email,name"));
		$mail->Username = $sitesettings['admin_email'];
		$mail->Password = $sitesettings['admin_email_password'];
		$mail->setView($settings['view']);
		$mail->setData($data);
		$mail->render();
		$mail->ContentType = 'text/html';
		$mail->From = $sitesettings['web_email'];
		$mail->FromName = 'TAG System';
		$mail->Subject = $settings['subject'];

		if(isset($data['pdfFile'])){
			$mail->AddAttachment($data['pdfFile'].".pdf");	
		}
		if($multiple){
			foreach ($multiple as $key => $value) {
				$mail->AddBCC($value);
			}
		}else{
			$mail->AddAddress($data['email']);
		}

		if(isset($data['cc'])){
			$mail->AddCC($data['cc']);
		}
		
		if ($mail->Send()){
			return true;
		} else {
			return false;
		}
	
	}
}
?>