<?php
/**
* Curl wrapper for Yii
* @author hackerone
*/
class Curl extends CComponent{
	public $proxy;
	
	//urban airship options
	public $uaOptions;
	public $uaKeys;
	public $uaNotification;

	private $_ch;

	// config from config.php
	public $options;

	// default config
	private $_config = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_AUTOREFERER    => true,         
		CURLOPT_CONNECTTIMEOUT => 10,
		CURLOPT_TIMEOUT        => 10,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:5.0) Gecko/20110619 Firefox/5.0'
		);

	private function _exec($url){

		$this->setOption(CURLOPT_URL, $url);
		$c = curl_exec($this->_ch);
		if(!curl_errno($this->_ch))
			return $c;
		else
			throw new CException(curl_error($this->_ch));

		return false;

	}

	public function get($url, $params = array()){
		$this->setOption(CURLOPT_HTTPGET, true);

		return $this->_exec($this->buildUrl($url, $params));
	}

	public function post($url, $data = array()){
		$this->setOption(CURLOPT_POST, true);
		$this->setOption(CURLOPT_POSTFIELDS, $data);

		return $this->_exec($url);
	}

	public function put($url, $data, $params = array()){        

		// write to memory/temp
		$f = fopen('php://temp', 'rw+');
		fwrite($f, $data);
		rewind($f);

		$this->setOption(CURLOPT_PUT, true);
		$this->setOption(CURLOPT_INFILE, $f);
		$this->setOption(CURLOPT_INFILESIZE, strlen($data));
		
		return $this->_exec($this->buildUrl($url, $params));
	}

	public function delete($url, $params = array()) {

		$this->setOption(CURLOPT_RETURNTRANSFER, true);
		$this->setOption(CURLOPT_CUSTOMREQUEST, 'DELETE');

		return $this->_exec($this->buildUrl($url, $params));
	}

	public function buildUrl($url, $data = array()){
		$parsed = parse_url($url);
		isset($parsed['query'])?parse_str($parsed['query'],$parsed['query']):$parsed['query']=array();
		$params = isset($parsed['query'])?array_merge($parsed['query'], $data):$data;
		$parsed['query'] = ($params)?'?'.http_build_query($params):'';
		if(!isset($parsed['path']))
			$parsed['path']='/';

		return $parsed['scheme'].'://'.$parsed['host'].$parsed['path'].$parsed['query'];
	}
	
	public function setOptions($options = array()){
		curl_setopt_array( $this->_ch , $options);

		return $this;
	}

	public function setOption($option, $value){
		curl_setopt($this->_ch, $option, $value);

		return $this;
	}

	public function setHeaders($header = array())
	{
		if($this->_isAssoc($header)){
			$out = array();
			foreach($header as $k => $v){
				$out[] = $k .': '.$v;
			}
			$header = $out;
		}

		$this->setOption(CURLOPT_HTTPHEADER, $header);
		
		return $this;
	}


	private function _isAssoc($arr)
	{
		return array_keys($arr) !== range(0, count($arr) - 1);
	}

	public function getError()
	{
		return curl_error($this->_ch);
	}

	public function getInfo()
	{
		return curl_getinfo($this->_ch);
	}

	// initialize curl
	public function init(){
		try{
			$this->_ch = curl_init();
			$options = is_array($this->options)? ($this->options + $this->_config):$this->_config;
			$this->setOptions($options);

			$ch = $this->_ch;
			
			// close curl on exit
			Yii::app()->onEndRequest = function() use(&$ch){
				curl_close($ch);
			};
		}catch(Exception $e){
			throw new CException('Curl not installed');
		}
	}

	/* abilash added */
	public function setProxy($proxy){
		$this->setOption(CURLOPT_PROXY, $proxy);

		return $this;
	}

	public function setAuthentication($username, $password){
		$this->setOption(CURLOPT_USERPWD, $username . ':' . $password);

		return $this;
	}

	public function setUaDefaults(){
		if(!$this->proxy && Yii::app()->params['proxy']){
			$this->proxy = Yii::app()->params['proxy'];
		}

		$this->setProxy($this->proxy);

		$this->setHeaders(array(
			"Content-Type"=>"application/json",
			"Accept"=>"application/vnd.urbanairship+json; version=3;"
			));

		$this->setAuthentication($this->uaOptions['APPKEY'], $this->uaOptions['PUSHSECRET']);

		//default notification
		if(!$this->uaNotification){
			$this->uaNotification = array(
				"ios" => array(),
				"android" => array()
				);

			$this->uaNotification["ios"] = array(
				'badge' => "+1",
				'sound' => "cat.caf",
				);

			$this->uaNotification["ios"]["alert"] = "Invalid message. Please contact admin";
			$this->uaNotification["android"]["alert"] = "Invalid message. Please contact admin";
		}

		return $this;
	}

	public function setUaKey($key = 'tagbond'){
		if(!$this->proxy && Yii::app()->params['proxy']){
			$this->proxy = Yii::app()->params['proxy'];
		}

		$this->setProxy($this->proxy);

		$this->setHeaders(array(
			'Content-Type'=>'application/json',
			'Accept'=>'application/vnd.urbanairship+json; version=3;'
			));

		//dbug::p($this->uaKeys[$key]);
		$this->setAuthentication($this->uaKeys[$key]['APPKEY'], $this->uaKeys[$key]['PUSHSECRET']);

		//default notification
		if(!$this->uaNotification){
			$this->uaNotification = array(
				'ios' => array(),
				'android' => array()
				);

			$this->uaNotification['ios'] = array(
				'badge' => '+1',
				'sound' => 'cat.caf',
				);

			$this->uaNotification['ios']['alert'] = 'Invalid message. Please contact admin';
			$this->uaNotification['android']['alert'] = 'Invalid message. Please contact admin';
		}

		return $this;
	}

	public function setUaAlert($alert){
		$this->uaNotification['ios']['alert'] = $alert;
		$this->uaNotification['android']['alert'] = $alert;
	}

	public function pushUa(){
		$url = 'https://go.urbanairship.com/api/push/';

		//platform
		$platform = array('ios','android');
		if($this->uaOptions['platform']){
			$platform = $this->uaOptions['platform'];
		}

		$push = array(
			'notification'=>$this->uaNotification,
			'device_types'=>$platform
			);

		//audience
		$push['audience'] = 'all';
		// alias
		if($this->uaOptions['alias']){
			$push['audience'] = array();
			$push['audience']['alias'] = $this->uaOptions['alias'];
		}
		// ios tokens
		else if($this->uaOptions['device_tokens']){
			$push['audience'] = array();
			$push['audience']['device_token'] = $this->uaOptions['device_token'];
		}
		// android ids
		else if($this->uaOptions['apid']){
			$push['audience'] = array();
			$push['audience']['apid'] = $this->uaOptions['apid'];
		}

		//echo json_encode($push); exit;
		$output = $this->post($url, json_encode($push));

		return $output;
	}
}