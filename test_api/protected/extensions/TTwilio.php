<?php
spl_autoload_unregister(array('YiiBase','autoload'));
require Yii::app()->basePath.'/extensions/phpcomposer/vendor/twilio/sdk/Services/Twilio.php';
spl_autoload_register(array('YiiBase','autoload'));


class TTwilio extends CApplicationComponent
{
	private $proxy;
	private $sid;
	private $token;
	private $client;
	private $from;

	public function setProxy($proxy){
		$this->proxy = $proxy;
	}

	public function init(){
		$this->client = new Services_Twilio($this->sid, $this->token);
	}

	public function setCredentials($sid, $token){
		$this->sid = $sid;
		$this->token = $token;
	}

	public function setFrom($from){
		$this->from = $from;
	}

	public function sendSMS($to, $message){
		if(!$this->client){
			$this->init();
		}

		$this->client->account->messages->create(array( 
			'To' => $to,
			'From' => $this->from,
			'Body' => $message,
		));
	}

}
?>