<?php
include(Yii::app()->basePath.'/extensions/tfacebook/components/facebook.php');

class TFacebook extends CApplicationComponent
{
	public $facebook;
	public $appId;
	public $secret;
	public $proxy;

	public function init(){
		$this->facebook = new Facebook(array(
			'appId'  => $this->appId,
			'secret' => $this->secret,
		));
		if($this->proxy){
			Facebook::$CURL_OPTS[CURLOPT_PROXY] = $this->proxy;
		}
	}

	public function getFacebook(){
		return $this->facebook;
	}

	public function showFacebook(){
		print_r($this->facebook);
	}
}
?>