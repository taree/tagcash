<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitdcd641cbf181421d6835f83525c03200
{
    public static $prefixesPsr0 = array (
        'L' => 
        array (
            'League\\OAuth2\\Server' => 
            array (
                0 => __DIR__ . '/..' . '/league/oauth2-server/src',
            ),
        ),
    );

    public static $classMap = array (
        'ezcDbException' => __DIR__ . '/..' . '/zetacomponents/database/src/exceptions/exception.php',
        'ezcDbFactory' => __DIR__ . '/..' . '/zetacomponents/database/src/factory.php',
        'ezcDbHandler' => __DIR__ . '/..' . '/zetacomponents/database/src/handler.php',
        'ezcDbHandlerMssql' => __DIR__ . '/..' . '/zetacomponents/database/src/handlers/mssql.php',
        'ezcDbHandlerMysql' => __DIR__ . '/..' . '/zetacomponents/database/src/handlers/mysql.php',
        'ezcDbHandlerNotFoundException' => __DIR__ . '/..' . '/zetacomponents/database/src/exceptions/handler_not_found.php',
        'ezcDbHandlerOracle' => __DIR__ . '/..' . '/zetacomponents/database/src/handlers/oracle.php',
        'ezcDbHandlerPgsql' => __DIR__ . '/..' . '/zetacomponents/database/src/handlers/pgsql.php',
        'ezcDbHandlerSqlite' => __DIR__ . '/..' . '/zetacomponents/database/src/handlers/sqlite.php',
        'ezcDbInstance' => __DIR__ . '/..' . '/zetacomponents/database/src/instance.php',
        'ezcDbMissingParameterException' => __DIR__ . '/..' . '/zetacomponents/database/src/exceptions/missing_parameter.php',
        'ezcDbMssqlOptions' => __DIR__ . '/..' . '/zetacomponents/database/src/options/identifiers.php',
        'ezcDbTransactionException' => __DIR__ . '/..' . '/zetacomponents/database/src/exceptions/transaction.php',
        'ezcDbUtilities' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/utilities.php',
        'ezcDbUtilitiesMysql' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/utilities_mysql.php',
        'ezcDbUtilitiesOracle' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/utilities_oracle.php',
        'ezcDbUtilitiesPgsql' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/utilities_pgsql.php',
        'ezcDbUtilitiesSqlite' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/utilities_sqlite.php',
        'ezcQuery' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/query.php',
        'ezcQueryDelete' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/query_delete.php',
        'ezcQueryException' => __DIR__ . '/..' . '/zetacomponents/database/src/exceptions/query_exception.php',
        'ezcQueryExpression' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/expression.php',
        'ezcQueryExpressionMssql' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/expression_mssql.php',
        'ezcQueryExpressionOracle' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/expression_oracle.php',
        'ezcQueryExpressionPgsql' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/expression_pgsql.php',
        'ezcQueryExpressionSqlite' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/expression_sqlite.php',
        'ezcQueryInsert' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/query_insert.php',
        'ezcQueryInvalidException' => __DIR__ . '/..' . '/zetacomponents/database/src/exceptions/query/invalid.php',
        'ezcQueryInvalidParameterException' => __DIR__ . '/..' . '/zetacomponents/database/src/exceptions/query/invalid_parameter.php',
        'ezcQuerySelect' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/query_select.php',
        'ezcQuerySelectMssql' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/query_select_mssql.php',
        'ezcQuerySelectOracle' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/query_select_oracle.php',
        'ezcQuerySelectSqlite' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/query_select_sqlite.php',
        'ezcQuerySqliteFunctions' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/implementations/query_sqlite_function_implementations.php',
        'ezcQuerySubSelect' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/query_subselect.php',
        'ezcQueryUpdate' => __DIR__ . '/..' . '/zetacomponents/database/src/sqlabstraction/query_update.php',
        'ezcQueryVariableParameterException' => __DIR__ . '/..' . '/zetacomponents/database/src/exceptions/query/variable_parameter.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixesPsr0 = ComposerStaticInitdcd641cbf181421d6835f83525c03200::$prefixesPsr0;
            $loader->classMap = ComposerStaticInitdcd641cbf181421d6835f83525c03200::$classMap;

        }, null, ClassLoader::class);
    }
}
