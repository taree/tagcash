

<tr style="background-color:#eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top: 5px;"><i>Reseller Request!</i></h1>
	</td>
</tr>
<tr>
	<td>
		<p><?php echo Yii::t("labels", "Hi!"); ?></p>

		<p><?php echo Yii::t("labels", $name." (".$user.") requested you to make him a reseller."); ?></p>

		<p><?php echo Yii::t("labels", "If you have no idea who this person is or have no idea why you received this email, please ignore it."); ?></p>

		<p>
			<?php echo Yii::t("labels", "Thanks,"); ?><br /><br />
			<b><?php echo Yii::t("labels", "Tagbond") ?></b>
		</p>
	</td>
</tr>