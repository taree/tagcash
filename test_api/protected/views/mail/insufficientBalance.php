<tr style="background-color: #eb3f3c;">
    <td>
        <h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i><b style='text-transform:uppercase'>Insufficient Balance</b></i></h1>
    </td>
</tr>
<tr>
    <td>		
        <p>
            You do not have sufficient balance in your <?php echo $walletName; ?> wallet for <?php echo $name ?> to redeem his/her gift.
            Please top-up you wallet at the earliest.
            <br/>
            <small><?php echo $user_email; ?></small>
        </p>
        <p>
            <?php echo Yii::t("labels", "Thank you!"); ?><br><br>
            <b><?php echo Yii::t("labels", "Tagwild"); ?></b>
        </p>
    </td>
</tr>

