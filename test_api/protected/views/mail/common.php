	<tr style="background-color: #eb3f3c">
		<td>
			<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i><?php echo $head; ?></i></h1>
		</td>
	</tr>
	<tr>
		<td>
			<p>Hi <b><?php echo $toName; ?></b></p>

			<p>
				<?php echo $message; ?>
			</p>
			
			<p>
				<?php echo Yii::t('labels', 'Thank you for using our service!'); ?><br><br>
				<b><?php if($fromName) {
					echo $fromName;
				} else {
					echo Yii::t('labels', 'Tagcash');
				} ?></b>
			</p>
		</td>
	</tr>