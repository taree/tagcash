<tr style="background-color:#eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top: 50px;"><i>Forgot Password</i></h1>
	</td>
</tr>
<tr>
	<td>
		<p><?php echo Yii::t("labels","Hi ".$name."!"); ?></p>

		<p><?php echo Yii::t("labels","We received your request for a password change. Please click the link below to reset your password."); ?></p>

		<p><a href="<?php echo $resetlink; ?>">Reset your password</a></p>

		<p><?php echo Yii::t("labels", "After clicking the link, you will receive an email which contains your new password."); ?></p>

		<p>
			<?php echo Yii::t("labels", "Thank you!"); ?><br /><br />
			<b><?php echo Yii::t("labels", "Tagcash") ?></b>
		</p>
	</td>
</tr>