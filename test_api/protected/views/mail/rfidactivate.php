<tr style="background-color:#eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top: 50px;"><i>Bind RFID</i></h1>
	</td>
</tr>
<tr>
	<td>
		<p><?php echo Yii::t("labels", "Hi!"); ?></p>

		<p><?php echo Yii::t("labels", "Someone, requested to bind an RFID tag to your account."); ?></p>

		<p><?php echo Yii::t("labels", "Please click on the following link to accept it"); ?></p>

		<a href="<?php echo Yii::app()->params['site_url']."/site/bindrfid/".$userId."/?code=".$code ?>"><?php echo Yii::t("labels", "Bind RFID Tag"); ?></a>

		<p><?php echo Yii::t("labels", "If you have no idea why you received it, please ignore it."); ?></p>

		<p>
			<?php echo Yii::t("labels", "Thanks,"); ?><br /><br />
			<b><?php echo Yii::t("labels", "Tagbond") ?></b>
		</p>
	</td>
</tr>