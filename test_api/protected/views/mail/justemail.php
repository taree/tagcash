<tr style="background-color: #eb3f3c;">
    <td>
        <h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i><b style='text-transform:uppercase'>Message From <?php echo $name; ?></b></i></h1>
    </td>
</tr>
<tr>
    <td>		
        <p>
            <i><?php echo $message; ?></i>
        </p>
        <p>
            <?php echo Yii::t("labels", "Thank you!"); ?><br><br>
            <b><?php echo Yii::t("labels", "Tagwild"); ?></b>
        </p>
    </td>
</tr>