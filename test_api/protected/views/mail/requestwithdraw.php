<tr style="background-color: #eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i><b style='text-transform:uppercase'>Withdrawal Request</b></i></h1>
	</td>
</tr>
<tr>
	<td>		
		<p>
			You have requested to cash out <b><?php echo number_format($amount,2)." ".$code; ?></b> on <b><?php echo $date_request; ?></b>.<br/>
			Please click this <a href="<?php echo $this->createAbsoluteUrl('credit/withdrawStatus').'?transactionId='.$tranx_id.'&status=0'; ?>" class="btn btn-danger">link</a> to confirm your request.
			<br/>
			<br/>
			<b>Do not click the link if you did not make this request.</b>
		</p>
		<p>
			<?php echo Yii::t("labels", "Thank you!"); ?><br><br>
			<b><?php echo Yii::t("labels", "Tagcash"); ?></b>
		</p>
	</td>
</tr>