
<h3><?php echo $data['email']; ?> is requesting a payment.</h3>
<p><b>Payment Details</b></p>
<p><?php
    if ($data['payment_type'] == 'bank') {
        echo "Bank: " . $data['bank_name'];
    } else {
        echo "Remittance center: " . $data['name'];
    }
    ?>
</p>
<?php
if ($data['payment_type'] == 'bank') {
    ?>
    <p>Account No.: <?php echo $data['account_number']; ?></p>
    <p>Account Name.: <?php echo $data['account_name']; ?></p>
<?php } ?>
<p>Amount: <?php echo $data['currency'] . ' ' . number_format($data['amount'], 2); ?></p>
<br>
<p>Check request in <b><i><a href="http://admin.tagbond.com">http://admin.tagbond.com</a></i></b></p>