<tr style="background-color: #eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i>Tagcash Email Activation Link</i></h1>
	</td>
</tr>
<tr>
	<td>
		<p><?php echo Yii::t("labels", "Hi " . $name . "!"); ?></p>

		<p>You have changed your email on Tagcash and it needs to be verfied again. Please click the activation link to verify your Email</p>

		<p><a href="<?php echo $activationkey; ?>"><?php echo $activationkey; ?></a></p>

		<p>If you didn't request this email or have no idea why you received it, please ignore it.</p>

		<p>Thank you!<br><br>
			<b><?php if($from){
				echo $form;
			} else {
				echo "Tagcash";
			} ?></b>
		</p>
	</td>
</tr>