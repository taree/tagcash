<tr style="background-color:#eb3f3c;">
    <td>
        <h1 style="color:#FFF; margin-bottom:0px; margin-top: 50px;"><i>You have received a promo code from Tagwild!</i></h1>
    </td>
</tr>
<tr>
    <td>
        <p><?php echo Yii::t("labels", "Hi!"); ?></p>
        <p><?php echo "You have received your promo code via the gift " . $dealTitle . " from Tagwild."; ?></p>
        <p>
            <?php echo $description; ?>
        </p>
        <p><?php echo "If you didn't request this email or have no idea why you received it, please ignore it."; ?></p>
        <p>
            <?php echo "Thank you,"; ?><br /><br />
            <b><?php echo "Tagwild"; ?></b>
        </p>
    </td>
</tr>