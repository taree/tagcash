<tr style="background-color: #eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i><?php echo "<b style='text-transform:uppercase'>".$dealTitle."</b>"; ?></i></h1>
	</td>
</tr>
<tr>
	<td>
		<p><?php echo Yii::t("labels", "Hi there!"); ?></p>
		<p>
			<?php echo "<b>".$name."</b> would like to give ".$amount." ".(($wallet_code) ? $wallet_code : 'Points')." voucher to you!"; ?>
		</p>
		<?php echo "Claim your voucher <a href='".$siteurl.'/v/'.CHtml::encode($voucher_id)."'>here</a> or you can enter this code <b>".CHtml::encode($voucher_id); ?></b><br>
		Claim your voucher before time runs out! Enjoy!
		<p>

		<p><?php echo "If you didn't request this email or have no idea why you received it, please ignore it."; ?></p>

			<?php echo Yii::t("labels", "Thanks,"); ?><br><br>
			<b><?php echo Yii::t("labels", "Tagcash"); ?></b>
		</p>
	</td>
</tr>

