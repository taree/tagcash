<div id="login-page">
	<div class="navbar navbar-fixed-top redbg" style="height:.2em;">
	</div>


	<div class="mycontainer logincontainer clearfix">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'loginPage-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
			'htmlOptions' => array(
				'class' => 'form'
			)
		));?>

		<div class="row-fluid">
			<div class="navbar-inner redbg" style="height:3.6em;">
				<div class="span4">
					<a href="#" class="logo clearfix">
						<img src="<?php echo Yii::app()->createUrl("images/logo.png"); ?>" alt="Tagcash Logo"/>
					</a>
				</div>
				<div class="span8">
					<h4>Login to Tagcash</h4>
				</div>
			</div>
		</div>

		<?php
		if(Yii::app()->user->hasFlash("error")){
			echo "<div class='alert alert-error'>";
			echo Yii::app()->user->getFlash("error");
			echo "</div>";
		}
		?>

		<div class="row-fluid tBreak">
			<div class="span4">
				<label class="control-label"><?php echo $form->label($modelLogin,'username'); ?></label>
			</div>
			<div class="span8">
				<?php echo $form->textField($modelLogin,'username', array('class'=>'textfield input-xlarge')); ?>
				<?php echo $form->error($modelLogin,'username'); ?>
			</div>
		</div>

		<div class="row-fluid tBreak">
			<div class="span4">
				<label class="control-label"><?php echo $form->label($modelLogin,'user_password'); ?></label>
			</div>
			<div class="span8">
				<?php echo $form->passwordField($modelLogin,'user_password', array('class'=>'textfield input-xlarge', 'value' => '')); ?>
				<?php echo $form->error($modelLogin,'user_password'); ?>
			</div>
		</div>

		<div class="row-fluid submitfooter tBreak">
			<div class="span12 pagination-centered">
					<input type="submit" class="submit-btn" value="Login to Tagcash"/>
					<a href="<?php echo $this->createUrl('oauth/signup'); ?>" class="width220 navbar" style="color: white;"><input type="button" class="submit-btn" value="Sign up for Tagcash"/></a>
			</div>
			
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>