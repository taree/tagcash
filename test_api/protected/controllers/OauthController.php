<?php

class OauthController extends Controller {

    public function __construct($id, $module = null) {
        parent::__construct($id, $module);

        $validator = new CEmailValidator;
        if (!$_POST['username'] && $_POST['user_email']) {
            if (!$validator->validateValue($_POST['user_email'])) {
                if (intval($_POST['user_email']) < 1) {
                    $_POST['user_email'] = '';
                } else
                    $_POST['user_email'] = intval($_POST['user_email']);
            }
            $_POST['username'] = $_POST['user_email'];
        }
        if ($_POST['username']) {
            if (!$validator->validateValue($_POST['username'])) {
                if (intval($_POST['username']) < 1) {
                    $_POST['username'] = '';
                } else
                    $_POST['username'] = intval($_POST['username']);
            }
        }
        if (!$_POST['password'] && $_POST['user_password']) {
            $_POST['password'] = $_POST['user_password'];
        }

        // Create the auth server, the three parameters passed are references
        //  to the storage models
        $this->_OAUTH->authserver = new League\OAuth2\Server\Authorization(
                new League\OAuth2\Server\Storage\PDO\Client, new League\OAuth2\Server\Storage\PDO\Session, new League\OAuth2\Server\Storage\PDO\Scope
        );

        //expires_in
        if(!Yii::app()->request->getPost('fb_id')){
        $expires_in = ($_POST['expires_in']) ? $_POST['expires_in'] : 60 * 60 * 24;
        $expires_in_long = ($_POST['expires_in']) ? $_POST['expires_in'] : 60 * 60 * 24 * 7;
        }else{
         $expires_in = 31536000; // one year
         $expires_in_long = 31536000;   // one year
        }
        
        // Enable the grant types
        //refresh token
        $RefreshToken = new League\OAuth2\Server\Grant\RefreshToken($this->_OAUTH->authserver);
        $RefreshToken->setAccessTokenTTL($expires_in_long);
        $this->_OAUTH->authserver->addGrantType($RefreshToken);

        //authourization code
        $AuthCode = new League\OAuth2\Server\Grant\AuthCode($this->_OAUTH->authserver);
        $AuthCode->setAccessTokenTTL($expires_in);
        $this->_OAUTH->authserver->addGrantType($AuthCode);

        $this->_OAUTH->authserver->getGrantType('authorization_code')->setAuthTokenTTL(300); //Set the auth code expiry to 5 mins for testing
        //client credentials
        $ClientCredentials = new League\OAuth2\Server\Grant\ClientCredentials($this->_OAUTH->authserver);
        $ClientCredentials->setAccessTokenTTL($expires_in);
        $this->_OAUTH->authserver->addGrantType($ClientCredentials);

        //implicit
        $this->_OAUTH->authserver->addGrantType(new League\OAuth2\Server\Grant\Implicit($this->_OAUTH->authserver));

        //resource owner - password
        $thisObj = $this;
        if(!Yii::app()->request->getPost('fb_id')){
        $authoriseUserId = function($username, $password) use ($thisObj) {
            if ($username && $password) {                
                $password = hash('sha512', Yii::app()->params['salt1'] . $password . Yii::app()->params['salt2']);                
                if ($_POST['login_type'] == 'admin') {
                    $admin = Administrators::model()->findByAttributes(array('admin_email' => $username, 'admin_password' => $password));
                } else {
                    if (strpos($username, '@')) {
                        $yiiQuery = array('user_email' => $username, 'user_password' => $password);
                    } else if (is_numeric($username)) {
                        $yiiQuery = array('id' => $username, 'user_password' => $password);
                    }

                    if ($_POST['restrictions']) {
                        $restrictions = $_POST['restrictions'];
                        $restrictions = json_decode($_POST['restrictions']);
                        foreach ($restrictions as $restrict) {
                            if ($restrict == "reseller") {
                                $yiiQuery['user_reseller'] = 1;
                            }
                        }
                    }

                    $user = Users::model()->findByAttributes($yiiQuery);
                }
            } else {
                $response = array(
                    'error' => 'invalid_username_andor_password',
                    'status' => 'failed'
                );
                Rest::sendResponse(403, CJSON::encode($response));
            }

            if ($admin) {
                $thisObj->_ADMINOBJ = $admin;
                return $admin->id;
            } else if ($user) {
                $thisObj->_USEROBJ = $user;
                return $user->id;
            } else {
                return false;
            }
        };
        }else{
        $authoriseUserId = function($username, $password) use ($thisObj) {
            $fb_id=Yii::app()->request->getPost('fb_id');
            if ($username && $password && $fb_id) {
                $password = hash('sha512', Yii::app()->params['salt1'] . $password . Yii::app()->params['salt2']);
               
                    if (strpos($username, '@')) {
                        $yiiQuery = array('user_email' => $username, 'user_password' => $password ,'fb_id'=>$fb_id);
                        $yiiQuery2 = array('user_email' => $username, 'user_password' => $password );
                    } else if (is_numeric($username)) {
                        $yiiQuery = array('id' => $username, 'user_password' => $password,'fb_id'=>$fb_id);
                        $yiiQuery2 = array('id' => $username, 'user_password' => $password);
                    }

                    if ($_POST['restrictions']) {
                        $restrictions = $_POST['restrictions'];
                        $restrictions = json_decode($_POST['restrictions']);
                        foreach ($restrictions as $restrict) {
                            if ($restrict == "reseller") {
                                $yiiQuery['user_reseller'] = 1;
                                $yiiQuery2['user_reseller'] = 1;
                            }
                        }
                    }

                    $user = Users::model()->findByAttributes($yiiQuery);
                    if(!$user){
                       $user = Users::model()->findByAttributes($yiiQuery2);
                       if($user){
                        $fb_id_linked=Users::model()->findByAttributes(array('fb_id'=>$fb_id));
                        if(!$fb_id_linked){ // new fb id , not linked to any account
                          $user->fb_id=$fb_id;
                          $user->save(false);
                        }else{
                            //already used fb id
                            return false;
                        }
                       }
                    }
                    
            } else {
                
                 $res=  array(
                        "messages" => array(
                            array(
                                "text" => "Login Failed - Invalid username or password",
                                "quick_replies" => array(
                                    array(
                                        "title" => "Try Again",
                                        "block_names" => array("login")
                                    ),
                                    array(
                                        "title" => "Back",
                                        "block_names" => array("menu")
                                    )
                                )
                            )
                        )
                    );
                Rest::sendResponse(403, CJSON::encode($res));
            }

            if ($admin) {
                $thisObj->_ADMINOBJ = $admin;
                return $admin->id;
            } else if ($user) {
                if($user->user_verified & 1){ // email verification check
                $thisObj->_USEROBJ = $user;
                return $user->id;
                }else{
                    $res=  array(
                        "messages" => array(
                            array(
                                "text" => "Login Failed - Please verify email address",
                                "quick_replies" => array(
                                    array(
                                        "title" => "Try Again",
                                        "block_names" => array("login")
                                    ),
                                    array(
                                        "title" => "Back",
                                        "block_names" => array("menu")
                                    )
                                )
                            )
                        )
                    );
                Rest::sendResponse(403, CJSON::encode($res));
                }
            } else {
                return false;
            }
        };
        
        }


        $grantPassword = new League\OAuth2\Server\Grant\Password($this->_OAUTH->authserver);
        $grantPassword->setVerifyCredentialsCallback($authoriseUserId);
        $grantPassword->setAccessTokenTTL($expires_in_long);
        $this->_OAUTH->authserver->addGrantType($grantPassword);
    }

    

   
    public function actionAuthorise() {
        // Retrieve the auth params from the user's session
        $params['client_id'] = Yii::app()->user->getState('client_id');
        $params['client_details'] = Yii::app()->user->getState('client_details');
        $params['redirect_uri'] = Yii::app()->user->getState('redirect_uri');
        $params['response_type'] = Yii::app()->user->getState('response_type');
        $params['scopes'] = Yii::app()->user->getState('scopes');
        $params['state'] = Yii::app()->user->getState('state');
        $userScopes = array();
        $communityScopes = array();

        // Get the user ID
        $params['user_id'] = Yii::app()->user->id;

        $clientDetails = OauthclientDetails::model()->findByAttributes(array("client_id" => $params['client_id']));
        $communityId = $clientDetails->community_id;

        //check whether the user is already have membership in community
        $existingMember = CommunityUsers::model()->with('role')->findByAttributes(array(
            "user_id" => $params['user_id'],
            "community_id" => $communityId
        ));

        if (!$existingMember) { //non community member
            $communityObj = new Communities;
            $community = $communityObj->getUserJoinSettings($params['user_id'], $communityId);
            if ($community[0]['community_type'] != 0) { //paid/private  community.
                $status = "private";
                if ($community[0]['community_type'] == 2)
                    $status = "paid";
                $modelLogin = new Users('login');
                Yii::app()->user->setFlash('error', 'This is a ' . $status . ' community!<br/> Login to tagbond and take membership!');
                $this->render("login", array("modelLogin" => $modelLogin));
                exit();
            }
        }

        if ($params['scopes']) {
            foreach ($params['scopes'] as $scope) {
                if (preg_match("/^user/", $scope['scope'])) {
                    $userScopes[] = $scope;
                } elseif (preg_match("/^community/", $scope['scope'])) {
                    $communityScopes[] = $scope;
                }
            }
        }

        // Check that the auth params are all present
        foreach ($params as $key => $value) {
            if ($value === null) {
                // Throw an error because an auth param is missing - don't
                // continue any further
            }
        }

        // User is not signed in so redirect them to the sign-in route (/oauth/signin)
        if ($params['user_id'] === null) {
            return $this->redirect(array('signin'));
        }

        $clientDetails = OAuthClients::getClientDetails($params['client_id']);

        // Check if the client should be automatically approved
        $autoApprove = OAuthClients::approvedBefore($params['client_id'], $params['user_id'], "user", $params['scopes']);

        // Process the authorise request if the user's has clicked 'approve' or the client
        if ($_POST['approve'] !== null || $autoApprove === true) {
            if ($params['response_type'] == "token") {
                //If implicit request. Generate an access token immediatly
                $params['grant_type'] = "implicit";

                if (!OAuthClients::checkIfAllowedGrant($params['client_id'], $params['grant_type'])) {
                    //This client doesnt have the permission to use this grant
                    throw new CHttpException(403, "grant_not_permitted");
                }

                $response = $this->_OAUTH->authserver->issueAccessToken($params);

                //Just save this access token now to the token details table. For future use
                $model = new OAuthtokenDetails;
                $model->oauth_token = $response['access_token'];
                $model->save();

                header("Location:" . $params['redirect_uri'] . "#" . $response['access_token']);
                exit;
            } else {
                $code = $this->_OAUTH->authserver->getGrantType('authorization_code')->newAuthoriseRequest('user', $params['user_id'], $params);

                //join community if a public community.
                if (!$existingMember) {

                    $defaultRole = CommunityRoles::getDefaultRole($communityId);

                    if (!$defaultRole) {
                        throw new CHttpException(403, "internal_error");
                    }

                    $communityUsersModel = new CommunityUsers();
                    $communityUsersModel->user_id = $params['user_id'];
                    $communityUsersModel->community_id = $communityId;
                    $communityUsersModel->role_id = $defaultRole->id;

                    if ($community->community_autojoin) {
                        $communityUsersModel->communityuser_status = 1;
                    } else {
                        $communityUsersModel->communityuser_status = 0;
                    }

                    $communityUsersModel->communityuser_relation = 1;
                    if (!$communityUsersModel->save()) {
                        throw new CHttpException(403, "internal_error");
                    }
                }

                header("Location:" . $params['redirect_uri'] . "?code=" . $code . "&state=" . $params['state']);
                exit;
            }
        }

        // If the user has denied the client so redirect them back without an authorization code
        if ($_POST['deny'] !== null) {
            $this->redirect($params['redirect_uri'], array(
                'error' => 'access_denied',
                'error_message' => $this->_OAUTH->authserver->getExceptionMessage('access_denied'),
                'state' => isset($params['state']) ? $params['state'] : ''
                    )
            );
        }

        // The client shouldn't automatically be approved and the user hasn't yet
        //  approved it so show them a form
        $this->render("authorise", array('clientDetails' => $clientDetails, 'userScopes' => $userScopes, 'communityScopes' => $communityScopes));
    }

    public function actionAccessToken() {
        //Check if a particular client can use the grant type
        $fb_id=Yii::app()->request->getPost('fb_id');
        if (!OAuthClients::checkIfAllowedGrant($_POST['client_id'], $_POST['grant_type']) && !$fb_id) {
            //This client doesnt have the permission to use this grant
            throw new CHttpException(403, 'grant_not_permitted');
        }
        if(!OAuthClients::checkIfAllowedGrant($_POST['client_id'], $_POST['grant_type']) && $fb_id) {
            
            $res=  array(
                        "messages" => array(
                            array(
                                "text" => "Login Failed - we are faceing some technical problem with bot, please contact tagcash support. error Code GNP379",
                                "quick_replies" => array(
                                    array(
                                        "title" => "Try Again",
                                        "block_names" => array("login")
                                    ),
                                    array(
                                        "title" => "Back",
                                        "block_names" => array("menu")
                                    )
                                )
                            )
                        )
                    );
                Rest::sendResponse(403, CJSON::encode($res));
        }

        $clientDetails = OauthclientDetails::model()->findByAttributes(array('client_id' => $_POST['client_id']));

        try {
            // Tell the auth server to issue an access token
            $response = $this->_OAUTH->authserver->issueAccessToken();
            
            if ($_POST['grant_type'] == 'password') {
                //checking admin
                if ($clientDetails->login_id != $this->_ADMINOBJ->id && !$fb_id) {
                    throw new CHttpException(403, 'client_id_not_permitted');
                }
                if ($clientDetails->login_id != $this->_ADMINOBJ->id && $fb_id) {
                    $res=  array(
                        "messages" => array(
                            array(
                                "text" => "Login Failed - we are faceing some technical problem with bot, please contact tagcash support. error Code CNP407",
                                "quick_replies" => array(
                                    
                                    array(
                                        "title" => "Back",
                                        "block_names" => array("menu")
                                    )
                                )
                            )
                        )
                    );
                Rest::sendResponse(403, CJSON::encode($res));
                }
                
                

                if ($this->_ADMINOBJ) {
                    $response = array_merge($response, $this->_ADMINOBJ->getAdminDetails());
                } else {
                    $response = array_merge($response, Users::getUserDetails($this->_USEROBJ));
                }
            }

            //Just save this access token now to the token details table. For future use
            $model = new OAuthtokenDetails;
            $model->oauth_token = $response['access_token'];

            if ($_POST['login_from'])
                $model->login_from = $_POST['login_from'];
            if ($_POST['device_id'])
                $model->device_id = $_POST['device_id'];
            $model->save();
            if(Yii::app()->request->getPost('grant_type') == 'authorization_code'){
            $res=array('status'=>'success','result'=>array('access_token'=>$response['access_token']));
            echo json_encode($res); exit;
            }

            //update last login, matchmaker and user location on login if location is given
            $this->_USEROBJ->last_login = gmdate("Y-m-d H:i:s");
            if (Common::isValidLatitude($_POST['latitude']) && Common::isValidLongitude($_POST['longitude'])) {
                $this->_USEROBJ->latitude = $_POST['latitude'];
                $this->_USEROBJ->longitude = $_POST['longitude'];
            }
            // check already in matchmker 
            $result = Matchmaker::getMatchmakerDetails($this->_USEROBJ->id);
            $status = null;
            //if new to matchmaker : enable
            if (!$result) {
                $this->_USEROBJ->matchmaker = 1;
                $status = 1;
            }if ($this->_USEROBJ->userkyc_date_verified == '0000-00-00') {
                $this->_USEROBJ->userkyc_date_verified = null;
            }
            $this->_USEROBJ->save();
           $response['modules']=  ModuleListViaCountryOrWallet::getModuleList($userId=$this->_USEROBJ->id, $userType=1, $countryId=$this->_USEROBJ->country_id);           
            //update or add matchmaker
            Matchmaker::updateMatchmakerDetails($this->_USEROBJ, $status);
        } catch (League\OAuth2\Server\Exception\ClientException $e) {
            // Throw an exception because there was a problem with the client's request
            $authserver = $this->_OAUTH->authserver;
            if(!$fb_id){
            $response = array(
                'error' => $authserver::getExceptionType($e->getCode()),
                'error_description' => $e->getMessage(),
                'status' => "failed"
            );
            
            }else{
                
                 $response=  array(
                        "messages" => array(
                            array(
                                "text" => "Login Failed - ".$e->getMessage()." error Code LEAG477",
                                "quick_replies" => array(
                                    array(
                                        "title" => "Try Again",
                                        "block_names" => array("login")
                                    ),
                                    array(
                                        "title" => "Back",
                                        "block_names" => array("menu")
                                    )
                                )
                            )
                        )
                    );
            }
            
            Rest::sendResponse(403, CJSON::encode($response));
        } catch (Exception $e) {
            // Throw an error when a non-library specific exception has been thrown
            if(!$fb_id){
            $response = array(
                'error' => 'undefined_error',
                'error_description' => $e->getMessage(),
                'status' => "failed"
            );
            }else{
              $response=  array(
                        "messages" => array(
                            array(
                                "text" => "Login Failed - we are faceing some technical problem with bot, please contact tagcash support. error Code ERR503",
                                "quick_replies" => array(
                                    array(
                                        "title" => "Try Again",
                                        "block_names" => array("login")
                                    ),
                                    array(
                                        "title" => "Back",
                                        "block_names" => array("menu")
                                    )
                                )
                            )
                        )
                    );
            }

            Rest::sendResponse(403, CJSON::encode($response));
        }
        //update services last login
        Services::updateLastLogin($response['id']);

        //get jabber details
        if(Yii::app()->name !=='Tagcash-demo'){
        $jabberAccount = EJabberdUsers::getJabberAccount($response['id']);
        $response['jabber_account'] = $jabberAccount;
        }
//        $walletBalance = WalletBalances::model()->findByAttributes(array(
//            'balance_id' => $response['id'],
//            'balance_type' => 1,
//            'wallet_type_id' => 525
//        ));
//        if ((!$walletBalance->id) && (intval($response['country']->id) != 174)) {
//            $TagcashPlayPoints = new TagcashPlayPoints();
//            $transfer = $TagcashPlayPoints->walletTransfer($response['id'], 1, 10, 'debit', $this, 'Free points of 1000 PLAY');
//        }
//        if (intval($response['country']->wallet_id)) {
//            $walletBalance = WalletBalances::model()->findByAttributes(array(
//                'balance_id' => $response['id'],
//                'balance_type' => 1,
//                'wallet_type_id' => intval($response['country']->wallet_id)
//            ));
//            if (!$walletBalance->id) {
//                //create wallet for this user
//                $walletBalance = new WalletBalances();
//                $walletBalance->balance_id = $response['id'];
//                $walletBalance->balance_type = 1;
//                $walletBalance->wallet_type_id = intval($response['country']->wallet_id);
//                $walletBalance->save();
//            }
//
//            if (intval($response['country']->wallet_id)) {
//                $default_wallet = intval($response['country']->wallet_id);
//            } else {
//                if ($response['country'] == 174) {
//                    $default_wallet = 1;
//                } else {
//                    $default_wallet = 525;
//                }
//            }
//
//            $userPreferences = UserPreferences::model()->findByAttributes(array(
//                'user_id' => $response['id']
//            ));
//            if (($userPreferences) && (!$userPreferences->pref_wallet_id)) {
//                $userPreferences->pref_wallet_id = $default_wallet;
//            } elseif (!$userPreferences) {
//                $userPreferences = new UserPreferences();
//                $userPreferences->user_id = $response['id'];
//                $userPreferences->pref_wallet_id = $default_wallet;
//            }
//            $userPreferences->save();
//        }
//
//        $walletBalance = WalletBalances::model()->findByAttributes(array(
//            'balance_id' => $response['id'],
//            'balance_type' => 1,
//            'wallet_type_id' => 525
//        ));
//        if ((!$walletBalance->id) && ($response['country'] != 174)) {
//            $TagPlayPoints = new TagcashPlayPoints();
//            $transfer = $TagPlayPoints->walletTransfer($response['id'], 1, 10, 'credit', $this, 'Free energy of 10 Play');
//        }
        if(!$fb_id){
        $this->response['result'] = $response;
        $this->response['status'] = "success";
        }else{
            /*$this->response = array(
                    'messages' => array(
                         array('text'=>'Keep your access_token private  =>  '.$response['access_token'])
                    ),
                ); */ 
         $this->response = array(
                "set_attributes" => array(
                    "access_token" => $response['access_token']
                ),
                "block_names" => array("user_pay", "merchant_pay", "pay_bills", "buy_sell_load"),
                "type" => "show_block",
                "title" => "go",
                "messages" => array(
                    array(
                        "text" => "Login Successful",
                         "quick_replies" => array(
                                    array(
                                        "title" => "Proceed",
                                        "block_names" => array("welcome")
                                    )
                                )
                    )
                )
            );
        }
        Rest::sendResponse(200, json_encode($this->response, JSON_NUMERIC_CHECK));
    }

   

    public function actionLogout() {
        $fb_id=  Common::getFbId();
        $accessToken=Yii::app()->request->getPost('access_token');
        if($accessToken){
        $token=  OauthSessionAccessTokens::model()->findByAttributes(array('access_token'=>$accessToken));
        if($token){
           $token->access_token_expires=1;
           $token->save(false);
        }
        }
        if($fb_id){
        $userObj=  Users::model()->findByAttributes(array('fb_id'=>$fb_id));
        if($userObj){
          $userObj->fb_id=NULL;
          $userObj->save(false);  
            $fbIdFail=false;
        }else{
          $fbIdFail=true;  
        }        
            if(!$fbIdFail){
             $this->response = array(
                "set_attributes" => array(
                    "access_token" => 'ABCDEFGHIJKLMNOPQ'
                ),
                "block_names" => array("user_pay", "merchant_pay", "pay_bills", "buy_sell_load", "menu"),
                "type" => "show_block",
                "messages" => array(
                    array(
                        "text" => "Logout Successful",
                         "quick_replies" => array(
                                    array(
                                        "title" => "Proceed",
                                        "block_names" => array("register_login_menu")
                                    )
                                )
                    )
                )
            );
            Rest::sendResponse(200, json_encode($this->response, JSON_NUMERIC_CHECK));    
                
            }else{
             $this->response = array(
                "messages" => array(
                    array(
                        "text" => "Logout Unsuccessful",
                        
                    )
                )
            );
            Rest::sendResponse(200, json_encode($this->response, JSON_NUMERIC_CHECK)); 
            }
        }else{
            if (Yii::app()->user)
            Yii::app()->user->logout();
        }
    }

    
}

?>