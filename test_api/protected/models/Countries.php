<?php

/**
 * This is the model class for table "countries".
 *
 * The followings are the available columns in table 'countries':
 * @property integer $id
 * @property string $country_code
 * @property string $country_name
 * @property string $country_longname
 * @property string $country_callingcode
 * @property integer $timezone_id
 * @property string $currency_code
 *
 * The followings are the available model relations:
 * @property Communities[] $communities
 * @property Currency $currencyCode
 * @property Ip2country[] $ip2countries
 * @property Users[] $users
 */
class Countries extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'countries';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('timezone_id, country_block_status', 'numerical', 'integerOnly'=>true),
			array('country_name, country_longname', 'length', 'max'=>80),
			array('country_code', 'length', 'max'=>2),
			array('country_callingcode', 'length', 'max'=>8),
			array('currency_code', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, country_code, country_name, country_longname, country_callingcode, timezone_id, currency_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'communities' => array(self::HAS_MANY, 'Communities', 'country_id'),
			'currencyCode' => array(self::BELONGS_TO, 'Currency', 'currency_code'),
			'ip2countries' => array(self::HAS_MANY, 'Ip2country', 'country_id'),
			'users' => array(self::HAS_MANY, 'Users', 'country_id'),
			'states' => array(self::HAS_MANY, 'States', 'country_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'country_code' => 'Country Code',
			'country_name' => 'Country Name',
			'country_longname' => 'Country Longname',
			'country_callingcode' => 'Country Callingcode',
			'country_block_status' => 'Blocked Status',
			'timezone_id' => 'Timezone',
			'currency_code' => 'Currency Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('country_code',$this->country_code,true);
		$criteria->compare('country_name',$this->country_name,true);
		$criteria->compare('country_longname',$this->country_longname,true);
		$criteria->compare('country_callingcode',$this->country_callingcode,true);
		$criteria->compare('timezone_id',$this->timezone_id);
		$criteria->compare('currency_code',$this->currency_code,true);
		$criteria->compare('country_block_status',$this->country_block_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Countries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getBlockStatus(){
		$response = new stdClass;
		$response->wallet_blocked = false;
		if($this->country_block_status == 1){
			$response->wallet_blocked = true;
		}

		return $response;
	}

	public static function getCountryName($id,$code = false){
		if($code)
			return $result = SELF::model()->findByPK(intval($id))->country_code;
		else	
			return $result = SELF::model()->findByPK(intval($id))->country_name;
	}

	public static function getCallingCode($country_id){
		return $result = SELF::model()->findByPK(intval($country_id))->country_callingcode;
	}

	public static function getCountryCode($country_name){
		return $result = SELF::model()->findByAttributes(array('country_name' => $country_name))->id;
	}
}
