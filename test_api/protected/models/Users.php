<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $user_firstname
 * @property string $user_lastname
 * @property integer $user_gender
 * @property string $user_password
 * @property string $user_email
 * @property integer $user_verified
 * @property string $user_activationkey
 * @property string $user_created_at
 * @property integer $user_status
 * @property string $user_dob
 * @property string $user_mobile
 * @property string $user_address1
 * @property string $user_address2
 * @property string $user_city
 * @property string $user_state
 * @property integer $country_id
 * @property integer $timezone_id
 * @property integer $user_zipcode
 * @property string $user_registeredip
 * @property integer $user_loginstatus
 * @property integer $user_autoadd
 * @property string $user_pin
 * @property integer $user_referrer
 * @property integer $pre_approved
 *
 * The followings are the available model relations:
 * @property HaLogins[] $haLogins
 * @property UserContacts[] $userContacts
 * @property UserContacts[] $userContacts1
 * @property UserLogins[] $userLogins
 * @property Countries $country
 * @property Timezones $timezone
 * @property Users $userReferrer
 * @property Users[] $users
 */
class Users extends CActiveRecord {

    const WEAK = 0;
    const STRONG = 1;

    public $displayErrors = true;
    //pagination
    public $pageCount;
    public $pageOffset;
    //settings
    private $settings;
    //identity
    public $user;
    public $userId;
    public $community;
    public $communityId;
    public $thisObj;
    public $agreement;
    public $email;
    public $username;
    public $password;
    public $rememberMe;
    private $_identity;
    // public $verifyCode; 
    public $password_current; // holds the password confirmation word
    public $password_repeat; //will hold the encrypted password for update actions.
    public $role_type;
    //location
    public $longitude;
    public $latitude;
    public $distance;
    //community category
    public $category_id;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('country_id, timezone_id, state_id', 'numerical', 'integerOnly' => true, 'min' => 1),
            array('user_email, user_password', 'required', 'on' => 'register', 'message' => 'invalid_{attribute}'),
            array('user_email', 'required', 'on' => 'facebook', 'message' => 'invalid_{attribute}'),
            array('user_email', 'email', 'except' => 'changepassword', 'message' => 'invalid_{attribute}'),
            array('user_email', 'unique', 'caseSensitive' => false, 'except' => 'login', 'message' => 'email_exists'),
            array('fb_id', 'unique', 'caseSensitive' => false, 'except' => 'login', 'message' => 'invaid_fb_id'),
            array('user_smscode', 'length', 'min' => 5),
            array('user_forgotcode', 'length', 'max' => 200),
            array('user_mobile', 'unique', 'except' => 'login', 'message' => 'mobile_exists'),
            array('user_gender, user_verified, user_status, user_zipcode, user_loginstatus, user_autoadd,  user_referrer, pre_approved', 'numerical', 'integerOnly' => true),
            array('user_firstname, user_lastname, user_password, user_email, user_pin', 'length', 'max' => 255, 'message' => '{attribute}_too_long'),
            array('user_activationkey', 'length', 'max' => 128),
            array('user_mobile', 'length', 'max' => 30),
            array('user_address1, user_address2, keyword', 'length', 'max' => 250),
            array('user_city, user_state', 'length', 'max' => 70),
            array('user_registeredip', 'length', 'max' => 20),
            array('user_autoadd,matchmaker', 'in', 'range' => array(0, 1)),
            array('user_dob', 'type', 'type' => 'date', 'message' => '{attribute} is not a valid date', 'dateFormat' => 'yyyy-MM-dd', 'allowEmpty' => true, 'except' => 'changepassword'),
            array('user_firstname, user_lastname', 'required', 'on' => 'updateprofile'),
            array('user_firstname, user_lastname, user_gender, user_dob, user_mobile, user_address1, user_address2, user_city, user_state, user_pin, country_id, timezone_id, user_zipcode, user_autoadd', 'safe', 'on' => 'updateprofile'),
            array('id, user_password, user_email, user_verified, user_activationkey, user_smscode, user_created_at, user_status,  user_registeredip, user_loginstatus, user_referrer, user_referrertype', 'unsafe', 'on' => 'updateprofile'),
            array('user_pin', 'length', 'min' => 4, 'max' => 4, 'on' => 'updateprofile'),
            array('user_pin', 'numerical', 'on' => 'updateprofile'),
            array('user_password', 'required', 'on' => 'changepassword'),
            array('user_firstname, user_lastname', 'required', 'except' => 'login,signup', 'message' => 'invalid_{attribute}'),
            array('user_password', 'length', 'min' => 6, 'on' => 'changepassword, register', 'tooShort' => 'invalid_{attribute}'),
            //Login with tagbond
            array('username, user_password', 'required', 'on' => 'login,signup'),
            array('user_email, user_firstname, user_lastname', 'required', 'on' => 'signup'),
            array('agreement', 'required', 'on' => 'signup', 'message' => 'You should agree the terms'),
            array('agreement', 'numerical', 'integerOnly' => true),
            // array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, user_firstname, user_lastname, user_gender, user_password, user_email, user_verified, user_activationkey, user_created_at, user_status, user_dob, user_mobile, user_address1, user_address2, user_city, user_state, country_id, timezone_id, user_zipcode, user_registeredip, user_loginstatus, user_autoadd, user_pin, user_referrer, user_referrertype, player_points, matchmaker, latitude, longitude, load_discount, last_game_slot_played, keyword', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'country' => array(self::BELONGS_TO, 'Countries', 'country_id'),
            'timezone' => array(self::BELONGS_TO, 'Timezones', 'timezone_id'),
            'state2' => array(self::BELONGS_TO, 'States', 'state_id'),
            'userLogins' => array(self::HAS_MANY, 'UserLogins', 'user_id'),
            'userRestrictions' => array(self::HAS_ONE, 'UserRestrictions', 'user_id'),
            'userPreferences' => array(self::HAS_ONE, 'UserPreferences', 'user_id'),
            'userContacts' => array(self::HAS_MANY, 'UserContacts', 'contact_user2_id'),
            'userContacts1' => array(self::HAS_MANY, 'UserContacts', 'contact_user1_id'),
            'userImages' => array(self::HAS_MANY, 'UserImages', 'user_id'),
            'userProfile' => array(self::HAS_MANY, 'UserProfiles', 'user_id'),
            'userDefaultcommunity' => array(self::BELONGS_TO, 'Communities', 'user_defaultcommunity_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'country_id' => 'Country',
            'timezone_id' => 'Timezone',
            'state_id' => 'State',
            'user_firstname' => 'Firstname',
            'user_lastname' => 'Lastname',
            'user_gender' => 'Gender',
            'user_password' => 'Password',
            'user_email' => 'Email',
            'user_verified' => 'Verified',
            'user_activationkey' => 'Activation Key',
            'user_created_at' => 'Created At',
            'user_status' => 'Status',
            'user_dob' => 'Date of birth',
            'user_mobile' => 'Mobile',
            'user_address1' => 'Address Line 1',
            'user_address2' => 'Address Line 2',
            'user_city' => 'City',
            'user_state' => 'State',
            'user_zipcode' => 'Zipcode',
            'user_registeredip' => 'Registeredip',
            'user_loginstatus' => 'Loginstatus',
            'user_autoadd' => 'Security',
            'user_pin' => 'Pin',
            'user_referrer' => 'Referrer',
            'user_referrertype' => 'Referrer Type',
            'pre_approved' => 'Pre Approved',
            'profile_id' => 'Default Profile',
            'username' => 'Email',
            'rememberMe' => 'Remember me next time',
            'password_current' => 'Current password',
            'password_repeat' => 'Repeat password',
            'player_points' => 'Player Points',
            'matchmaker' => 'Matchmaker',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'load_discount' => 'Load Discount',
            'last_game_slot_played' => 'UTC time in millisecond',
            'keyword' => 'Keyword',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_firstname', $this->user_firstname, true);
        $criteria->compare('user_lastname', $this->user_lastname, true);
        $criteria->compare('user_gender', $this->user_gender);
        $criteria->compare('user_password', $this->user_password, true);
        $criteria->compare('user_email', $this->user_email, true);
        $criteria->compare('user_verified', $this->user_verified);
        $criteria->compare('user_activationkey', $this->user_activationkey, true);
        $criteria->compare('user_created_at', $this->user_created_at, true);
        $criteria->compare('user_status', $this->user_status);
        $criteria->compare('user_dob', $this->user_dob, true);
        $criteria->compare('user_mobile', $this->user_mobile, true);
        $criteria->compare('user_address1', $this->user_address1, true);
        $criteria->compare('user_address2', $this->user_address2, true);
        $criteria->compare('user_city', $this->user_city, true);
        $criteria->compare('user_state', $this->user_state, true);
        $criteria->compare('country_id', $this->country_id);
        $criteria->compare('timezone_id', $this->timezone_id);
        $criteria->compare('user_zipcode', $this->user_zipcode);
        $criteria->compare('user_registeredip', $this->user_registeredip, true);
        $criteria->compare('user_loginstatus', $this->user_loginstatus);
        $criteria->compare('user_autoadd', $this->user_autoadd);
        $criteria->compare('user_pin', $this->user_pin, true);
        $criteria->compare('user_referrer', $this->user_referrer);
        $criteria->compare('user_referrertype', $this->user_referrertype);
        $criteria->compare('user_referrer_rewardstatus', $this->user_referrer_rewardstatus);
        $criteria->compare('pre_approved', $this->pre_approved);
        $criteria->compare('user_defaultcommunity_id', $this->user_defaultcommunity_id);
        $criteria->compare('user_reseller', $this->user_reseller);
        $criteria->compare('profile_id', $this->profile_id);
        $criteria->compare('user_allow_charging', $this->user_allow_charging);
        $criteria->compare('user_allowphoto', $this->user_allowphoto);
        $criteria->compare('membership_plan', $this->membership_plan);
        $criteria->compare('wallet_enabled', $this->wallet_enabled);
        $criteria->compare('user_autoapprove_rfid', $this->user_autoapprove_rfid);
        $criteria->compare('user_smsverification_status', $this->user_smsverification_status);
        $criteria->compare('user_smscode_date_last', $this->user_smscode_date_last, true);
        $criteria->compare('state_id', $this->state_id);
        $criteria->compare('access_status', $this->access_status);
        $criteria->compare('user_rating', $this->user_rating, true);
        $criteria->compare('chef', $this->chef);
        $criteria->compare('kyc_reward', $this->kyc_reward);
        $criteria->compare('registration_method', $this->registration_method);
        $criteria->compare('userkyc_date_verified', $this->userkyc_date_verified, true);
        $criteria->compare('player_points', $this->player_points);
        $criteria->compare('matchmaker', $this->matchmaker, true);
        $criteria->compare('latitude', $this->latitude);
        $criteria->compare('longitude', $this->longitude);
        $criteria->compare('load_discount', $this->load_discount);
        $criteria->compare('last_game_slot_played', $this->last_game_slot_played, true);
        $criteria->compare('keyword', $this->keyword, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    //convertion of user type = user/community
    public static function convertType($type) {
        if ($type == 1) {
            return 'user';
        } else if ($type == 2) {
            return 'community';
        }

        return $type;
    }

    public function beforeSave() {
        if ($this->scenario == "changepassword" || $this->scenario == "register" || $this->scenario =='facebook') {
            $security = new TagSecurity;
            $this->user_password = $security->hash($this->user_password);
        }
        return parent::beforeSave();
    }

    public function seeErrors() {
        $result = array();
        $errors = $this->getErrors();
        foreach ($errors as $key => $value) {
            $result = array_merge($result, $value);
        }

        return $result;
    }

    public function isPermitted($user, $pin) {
        if ($this->id == $user->id) {
            return true;
        } else if (!$this->user_verified && !$this->userRestrictions) {
            return true;
        } else if ($this->userRestrictions && $this->userRestrictions->checkPin($pin)) {
            return true;
        }

        return false;
    }

    public function setIdentity($thisObj) {
        $this->thisObj = $thisObj;
        $this->userId = $thisObj->_USERID;
        $this->communityId = $thisObj->_COMMUNITYID;
        $this->user = $thisObj->_USEROBJ;

        $this->setSettings($thisObj->_SETTINGS);

        return true;
    }

    public function setSettings($settings) {
        $this->settings = $settings;
    }

    public function getSettings() {
        if (!$this->settings) {
            $this->settings = SiteSettings::getSettings();
        }

        return $this->settings;
    }

    public function getUser($params, $thisObj) {
        if ($thisObj) {
            if (
                    ($params['id'] && $params['id'] == $thisObj->_USEROBJ->id) ||
                    ($params['email'] && $params['email'] == $thisObj->_USEROBJ->user_email) ||
                    ($params['mobile'] && $params['mobile'] == $thisObj->_USEROBJ->user_mobile)
            ) {
                return $thisObj->_USEROBJ;
            }
        }

        if ($params['id']) {
            $user = Users::model()->findByAttributes(array('id' => $params['id']));
            if (!$user) {
                if ($this->displayErrors)
                    throw new CHttpException(403, 'invalid_id');
            }
        }
        else if ($params['email']) {
            $user = Users::model()->findByAttributes(array('user_email' => $params['email']));
            if (!$user) { // || !$user->getVerificationDetails()->email_verified){
                if ($this->displayErrors)
                    throw new CHttpException(403, 'invalid_email');
            }
        }
        else if ($params['mobile']) {
            //search with 0 and country code appended			
            /* if(isset($params['isdcode'])){
              $user = Users::model()->findAll('user_mobile=:mobile', array(':mobile'=>intval($params['isdcode'].$params['mobile'])));
              }
              else{
              $user = Users::model()->findAll('user_mobile=:mobile1 OR user_mobile=:mobile2', array(':mobile1'=>$params['mobile'],':mobile2'=>intval('0'.$params['mobile'])));
              } */
            $user = Users::model()->findAll('user_mobile=:mobile1 OR user_mobile=:mobile2 OR user_mobile=:mobile3', array(':mobile1' => $params['mobile'], ':mobile2' => intval('0' . $params['mobile']), ':mobile3' => intval($params['isdcode'] . $params['mobile'])));
            //$user = Users::model()->findByAttributes(array('user_mobile'=>$params['mobile']));
            $user = $user[0];
            if (!$user) {// || !$user->getVerificationDetails()->sms_verified){
                if ($this->displayErrors)
                    throw new CHttpException(403, 'invalid_mobile');
            }
        }

        if (!$user) {
            return false;
        }

        return $user;
    }

    public function getVerificationDetails() {
        if (isset($this->user_verified)) {
            $verification = new stdClass();
            $email = 1; //00001
            $sms = 2; //00010
            //$passport = 4; //00100
            $data = 4; //00100 
            //$bill = 8; //01000
            $id = 8; //01000
            //$kyc = 16; //10000
            $image = 16; //10000
            $signature = 32;
            $video = 64;
            $kyc = 128;

            //email verification
        $verification->email_verified = false;
        if ($this->user_verified & $email) {
            $verification->email_verified = true;
        }

        //sms verification
        $verification->sms_verified = false;
        if ($this->user_verified & $sms) {
            $verification->sms_verified = true;
        }

        
        $verification->passport_verified = false;
         //data verification
        $verification->data_verified = false;
        if ($this->user_verified & $data) {
            $verification->data_verified = true;
            //passport verification depricated , keeping for compactability
            $verification->passport_verified = true;
        }

        $verification->bill_verified = false;
        $verification->id_verified = false;
        if ($this->user_verified & $id) {
            $verification->id_verified = true;
            //bill verification depricated , keeping for compactability
             $verification->bill_verified = true;
        }
        //image verification
        $verification->image_verified = false;
        if ($this->user_verified & $image) {
            $verification->image_verified = true;
        }
        //signature verification
        $verification->signature_verified = false;
        if ($this->user_verified & $signature) {
            $verification->signature_verified = true;
        }
        //video verification
        $verification->video_verified = false;
        if ($this->user_verified & $video) {
            $verification->video_verified = true;
        }
        
        //kyc verification
        $verification->kyc_verified = false;
        if ($this->user_verified & $kyc) {
            $verification->kyc_verified = true;
        }

            return $verification;
        }
    }

    public function checkVerification($verificationtype) {
        if ($verificationtype != "email_verified" && $verificationtype != "sms_verified" && $verificationtype != "passport_verified") {
            //If invalid verification type
            return false;
        } else {
            $verificationdetails = $this->getVerificationDetails();
            return $verificationdetails->$verificationtype;
        }
    }

    public function getCommunitiesUserIsMemberOf() {
        if (!is_numeric($this->pageCount) || $this->pageCount > 100) {
            $this->pageCount = 100;
        }

        if (!is_numeric($this->pageOffset)) {
            $this->pageOffset = 0;
        }
        $select = '
					t1.role_id, 
					t1.community_id,
					t2.longitude, 
					t2.latitude,
					t2.category_id,
					t4.name as category_name,
					t1.user_id, t2.id,
					t2.community_city, 
					t2.community_name, 
					t2.community_verified, 
					t3.role_name, 
					t3.role_type';
        $query = Yii::app()->db->createCommand()
                ->from('community_users t1')
                ->leftJoin('communities t2', 't1.community_id = t2.id')
                ->leftJoin('community_roles t3', 't1.role_id = t3.id')
                ->leftJoin('categories t4', 't4.id = t2.category_id')
                ->where('t1.user_id = :userId AND t3.role_type = 3 AND t1.communityuser_status = 1 AND t2.community_type != 3', array(
                    ':userId' => $this->id))
                ->group('t1.community_id')
                ->limit($this->pageCount, $this->pageOffset);
        if ($this->category_id) {
            $query->andWhere('category_id = :category_id', array('category_id' => $this->category_id));
        }
        if ($this->latitude && $this->longitude && $this->distance) {
            $select.=' , SQRT( POW( 69.1 * ( latitude - ' . $this->latitude . ' ) , 2 ) + POW( 69.1 * ( ' . $this->longitude . ' - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance';
            $query->having('distance <= ' . intval($this->distance));
            $query->order(array('distance'));
        }

        $query->select($select);
        $result = $query->queryAll();

        return $result;
    }

    public function getCommunitiesUserIsStaffOf($params = array()) {
        if (!is_numeric($params['pageCount']) || $params['pageCount'] > 100) {
            $params['pageCount'] = 100;
        }

        if (!is_numeric($params['pageOffset'])) {
            $params['pageOffset'] = 0;
        }
        $search = "";
        if ($params['community_name']) {
            $search = ' AND ( t2.community_name LIKE "%' . $params['community_name'] . '%")';
        }

        $query = Yii::app()->db->createCommand()
                ->select('
					t1.role_id,
					t1.community_id,
					t1.user_id, t2.id,
					t2.community_name,
					t2.community_verified,
					t3.role_name,
					t3.role_type'
                )
                ->from('community_users t1')
                ->leftJoin('communities t2', 't1.community_id = t2.id')
                ->leftJoin('community_roles t3', 't1.role_id = t3.id');

        $query = $query->where('
			t1.user_id = :userId 
			AND (t3.role_type = 1 OR t3.role_type = 2) 
			AND t1.communityuser_status = 1 AND t2.community_type !=3' . $search, array(':userId' => $this->id));

        if ($params['verified']) {
            if ($params['verified'] == 'merchant') {
                $query->andWhere('t2.community_verified & 2'); //00010
            } else if ($params['verified'] == 'cico') {
                $query->andWhere('t2.community_verified & 4'); //00100
            } else if ($params['verified'] == 'merchant_cico') {
                $query->andWhere('t2.community_verified & 6'); // 00110
            } else {
                $query->andWhere('t2.community_verified > 0'); //just normal verified
            }
        }

        $query = $query->limit($params['pageCount'], $params['pageOffset']);
        $communities = $query->queryAll();

        if ($communities) {
            foreach ($communities as $community) {
                //verification
                if ($community['community_verified'] & 2) {
                    $community['community_verified'] = 'merchant';
                } else if ($community['community_verified'] > 0) {
                    $community['community_verified'] = true;
                } else {
                    $community['community_verified'] = false;
                }

                //role
                if ($community['role_type'] == 1) {
                    $community['role_type'] = "owner";
                } elseif ($community['role_type'] == 2) {
                    $community['role_type'] = "staff";
                }

                $result[] = $community;
            }
        }

        return $result;
    }

    public function getPendingCommunities($relationType) {
        if (!is_numeric($this->pageCount) || $this->pageCount > 100) {
            $this->pageCount = 100;
        }

        if (!is_numeric($this->pageOffset)) {
            $this->pageOffset = 0;
        }
        $select = 't1.role_id, t1.community_id, t2.category_id, t4.name as category_name,t2.longitude, t2.latitude,t1.user_id, t2.id, t2.community_name, t2.community_city, t2.community_verified, t3.role_name, t3.role_type';
        $query = Yii::app()->db->createCommand()
                ->from('community_users t1')
                ->leftJoin("communities t2", "t1.community_id = t2.id")
                ->leftJoin("community_roles t3", "t1.role_id = t3.id")
                ->leftJoin('categories t4', 't4.id = t2.category_id')
                ->where("t1.user_id = :userId AND t1.communityuser_status = 0 AND t1.communityuser_relation = :relationType AND t2.community_type !=3", array(
                    ':userId' => $this->id,
                    ":relationType" => $relationType))
                ->group('t1.community_id')
                ->limit($this->pageCount, $this->pageOffset);
        if ($this->category_id) {
            $query->andWhere('category_id = :category_id', array('category_id' => $this->category_id));
        }

        if ($this->latitude && $this->longitude && $this->distance) {
            $select.=' , SQRT( POW( 69.1 * ( latitude - ' . $this->latitude . ' ) , 2 ) + POW( 69.1 * ( ' . $this->longitude . ' - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance';
            $query->having('distance <= ' . intval($this->distance));
            $query->order(array('distance'));
        }
        $query->select($select);
        $result = $query->queryAll();

        return $result;
    }

    public function getCommunityInvites($userId = NULL) {
        if (!$userId) {
            $userId = $this->id;
        }

        $communities = Yii::app()->db->createCommand()
                ->select('t1.community_id, t2.community_name, t1.community_verified, t1.communityusers_date')
                ->from('community_users t1')
                ->join('communities t2', "t2.id = t1.community_id")
                ->where("t1.user_id = :userId AND t1.communityuser_status = 0 AND t1.communityuser_relation = 2 AND t2.community_type !=3", array(':userId' => $userId))
                ->queryAll();

        $parsedCommunities = array();
        if ($communities) {
            foreach ($communities as $community) {
                $object = new stdClass();
                $object->community_id = $community['community_id'];
                $object->community_name = $community['community_name'];
                $object->request_date = $community['communityusers_date'];

                $parsedCommunities[] = $object;
            }
        }

        return $parsedCommunities;
    }

    public function countCommunityInvites($userId = NULL) {
        if (!$userId) {
            $userId = $this->id;
        }

        $communityCount = Yii::app()->db->createCommand()
                ->select('COUNT(*)')
                ->from('community_users t1')
                ->where("t1.user_id = :userId AND t1.communityuser_status = 0 AND t1.communityuser_relation = 2", array(':userId' => $userId))
                ->queryScalar();

        return $communityCount;
    }

    public function countContactRequests($userId = NULL) {
        if (!$userId) {
            $userId = $this->id;
        }

        return Contacts::countRequests($userId);
    }

    public static function countRegistrationsLastDay() {
        $lastDay = date("Y-m-d H:i:s", time() - (24 * 3600));

        $count = Yii::app()->db->createCommand()
                ->select('COUNT(*)')
                ->from('users t1')
                ->where("t1.user_created_at > :lastDay", array(':lastDay' => $lastDay))
                ->queryScalar();

        return $count;
    }

    public static function countRegistrationsLastWeek() {
        $lastWeek = date("Y-m-d H:i:s", time() - (24 * 3600 * 7));

        $count = Yii::app()->db->createCommand()
                ->select('COUNT(*)')
                ->from('users t1')
                ->where("t1.user_created_at > :lastWeek", array(':lastWeek' => $lastWeek))
                ->queryScalar();

        return $count;
    }

    public function login() {
        $this->setAttribute('user_password', hash('sha512', Yii::app()->params['salt1'] . $this->user_password . Yii::app()->params['salt2']));

        $this->rememberMe = '0';
        if (isset($_POST['Users']['rememberMe'])) {
            $this->rememberMe = $_POST['Users']['rememberMe'];
        }
        if ($this->_identity === null) {
            /* username = $this->email */
            $this->_identity = new UserIdentity($this->username, $this->user_password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        } else {
            Yii::app()->user->setFlash('error', 'Invalid email/password');
            return false;
        }
    }

    public function loginWithPin($pin) {
        if ($this->userRestrictions->checkPin($pin)) {
            return true;
        }

        return false;
    }

    public function getDefaultProfile() {
        if ($this->profile_id) {
            $result = UserProfiles::model()->findByPk($this->profile_id);

            if ($result) {
                $parsedResult = new stdClass;
                $parsedResult->id = $result->id;
                $parsedResult->profile_name = $result->profile_name;
                $parsedResult->profile_email = $result->profile_email;
                $parsedResult->profile_bio = $result->profile_bio;
                $parsedResult->profile_mobile = $result->profile_mobile;
                $parsedResult->profile_twitter = $result->profile_twitter;
                $parsedResult->profile_linkedin = $result->profile_linkedin;
                $parsedResult->profile_facebook = $result->profile_facebook;
                $parsedResult->profile_skype = $result->profile_skype;
                return $parsedResult;
            }
        } else {
            return false;
        }
    }

    public function getOwnProfile($superApp) {
        $country = new stdClass();
        $country->country_id = $this->country->id;
        $country->country_code = $this->country->country_code;
        $country->country_name = $this->country->country_name;
        $country->country_callingcode = $this->country->country_callingcode;
        //This is function is for the user/profile api call and the user is seeing his own details
        if ($this->id) {
            if ($superApp) {
                $result = new stdClass;
                $result->id = $this->id;
                $result->user_firstname = $this->user_firstname;
                $result->user_lastname = $this->user_lastname;
                $result->user_nickname = $this->user_nickname;
                $result->user_verificationdetails = $this->getVerificationDetails();
                $result->rating = Ratings::getRatings($this->id, 1);
                //gender
                if ($this->user_gender == 1) {
                    $result->user_gender = "male";
                } else if ($this->user_gender == 2) {
                    $result->user_gender = "female";
                } else if ($this->user_gender == 3) {
                    $result->user_gender = "ts";
                } else {
                    $result->user_gender = null;
                }

                //avatar status
                if ($avatar = UserImages::model()->findByAttributes(array("user_id" => $userModel->id, "image_active" => 1))) {
                    $result->user_avatar = true;
                } else {
                    $result->user_avatar = false;
                }

                //signature status
                $user_image = new UserImages;
                if ($signature = $user_image->s3UploadStatus($this->id, "signature")) {
                    $result->user_signature = true;
                } else {
                    $result->user_signature = false;
                }

                $result->user_email = $this->user_email;
                $result->user_verificationdetails = $this->getVerificationDetails();
                $result->user_created_at = $this->user_created_at;
                $result->user_nickname = $this->user_nickname;
                $result->user_dob = $this->user_dob;
                $result->user_mobile = $this->user_mobile;
                $result->user_address1 = $this->user_address1;
                $result->user_address2 = $this->user_address2;
                $result->user_city = $this->user_city;
                $result->user_state = $this->user_state;
                $defaultProfile = $this->getDefaultProfile();
                $result->profile_bio = $defaultProfile->profile_bio;
                $result->user_country = $country;


                $timezone = new stdClass();
                $timezone->timezone_id = $this->timezone->id;
                $timezone->timezone_label = $this->timezone->timezone_label;

                $result->user_timezone = $timezone;

                $result->user_zipcode = $this->user_zipcode;
                $result->user_autoadd = Common::convertToBoolean($this->user_autoadd);
                $result->contact_status = "self";
                if ($this->membership_plan == 0)
                    $result->user_type = 'Bronze';

                else if ($this->membership_plan == 1)
                    $result->user_type = 'Silver';

                else if ($this->membership_plan == 2)
                    $result->user_type = 'Gold';
                $result->keyword = $this->keyword ? $this->keyword : '';
                return $result;
            }
            else {
                $result = new stdClass;
                $result->id = $this->id;
                $result->user_firstname = $this->user_firstname;
                $result->user_lastname = $this->user_lastname;
                $result->user_nickname = $this->user_nickname;
                $result->user_email = $this->user_email;
                $result->user_country = $country;
                $result->rating = Ratings::getRatings($this->id, 1);
                //gender
                if ($this->user_gender == 1) {
                    $result->user_gender = "male";
                } else if ($this->user_gender == 2) {
                    $result->user_gender = "female";
                } else if ($this->user_gender == 3) {
                    $result->user_gender = "ts";
                } else {
                    $result->user_gender = null;
                }

                $result->user_nickname = $this->user_nickname;
                $result->user_dob = $this->user_dob;
                $result->user_address1 = $this->user_address1;
                $result->user_address2 = $this->user_address2;
                //default profile details
                $defaultProfile = $this->getDefaultProfile();
                $result->profile_email = $defaultProfile->profile_email;
                $result->profile_bio = $defaultProfile->profile_bio;
                $result->profile_mobile = $defaultProfile->profile_mobile;
                $result->profile_twitter = $defaultProfile->profile_twitter;
                $result->profile_linkedin = $defaultProfile->profile_linkedin;
                $result->profile_facebook = $defaultProfile->profile_facebook;
                $result->profile_skype = $defaultProfile->profile_skype;
                $result->keyword = $this->keyword ? $this->keyword : '';
                return $result;
            }
        } else {
            throw new CHttpException(403, "invalid_user");
        }
    }

    public function getProfileFromCommunityPerspective($communityId) {
        $result = new stdClass;
        $result->id = $this->id;
        $result->user_firstname = $this->user_firstname;
        $result->user_lastname = $this->user_lastname;
        $result->user_nickname = $this->user_nickname;

        $member = CommunityUsers::isPartOf($this->id, $communityId);
        $result->user_verificationdetails = $this->getVerificationDetails();
        $result->rating = Ratings::getRatings($this->id, 1);
        $result->keyword = $this->keyword ? $this->keyword : '';
        // profile bio and country  details made public - for mehul's requirement-confirmed by noman
        $country = Countries::model()->findByPk($this->country_id);
        $result->country = array('country_id' => $country->id, 'country_name' => $country->country_name, 'country_code' => $country->country_code);
        $result->user_country = array('country_id' => $country->id, 'country_name' => $country->country_name, 'country_code' => $country->country_code);
        $defaultProfile = $this->getDefaultProfile();
        if ($defaultProfile) {
         $result->profile_bio = $defaultProfile->profile_bio;    
         }
        if ($member) {
            if (($member->communityuser_relation == 1 AND $member->communityuser_status == 1) ||
                    $member->communityuser_relation == 2) {
                //Means whether the user is an approved member or the community has received a request from the user to join
                //the community, then the community can view the user's details
                //$defaultProfile = $this->getDefaultProfile();
                if ($defaultProfile) {
                    $result->profile_email = $defaultProfile->profile_email;
                //$result->profile_bio = $defaultProfile->profile_bio;
                    $result->profile_mobile = $defaultProfile->profile_mobile;
                    $result->profile_twitter = $defaultProfile->profile_twitter;
                    $result->profile_linkedin = $defaultProfile->profile_linkedin;
                    $result->profile_facebook = $defaultProfile->profile_facebook;
                    $result->profile_skype = $defaultProfile->profile_skype;
                }

                $role = new stdClass();
                $role->role_name = $member->role->role_name;

                if ($member->role->role_type == 1) {
                    $role->role_type = "owner";
                } elseif ($member->role->role_type == 2) {
                    $role->role_type = "staff";
                } else {
                    $role->role_type = "member";
                }

                if ($member->communityuser_relation == 1) {
                    if ($member->communityuser_status == 1) {
                        $role->role_status = "approved";
                    } else {
                        $role->role_status = "request_sent";
                    }
                } else {
                    if ($member->communityuser_status == 1) {
                        $role->role_status = "approved";
                    } else {
                        $role->role_status = "request_received";
                    }
                }

                $result->role = $role;

                if ($role->role_status == "approved") {
                    $result->can_charge = $member->communityuser_can_charge;

                    $result->reward = new stdClass;
                    $result->reward = $member->getRewardSettings();

                    $result->wallet = new stdClass;
                    $result->wallet = $member->getWalletSettings();
                }
            } else {
                $role = new stdClass();
                $role->role_status = "non_member";

                $result->role = $role;
            }
        } else {
            $role = new stdClass();
            $role->role_status = "non_member";

            $result->role = $role;
        }

        return $result;
    }

    public function getProfileFromUserPerspective($userId) {
        //This is when a contact or some other user views this users profile
        $country = new stdClass;
        $country->country_id = $this->country_id;
        $country->country_name = Countries::getCountryName($this->country_id);

        $result = new stdClass;
        $result->id = $this->id;
        $result->user_firstname = $this->user_firstname;
        $result->user_lastname = $this->user_lastname;
        $result->user_nickname = Nicknames::getNickname($this->id, $userId);
        $result->user_country = $country;
        $result->user_verificationdetails = $this->getVerificationDetails();
        if ($avatar = UserImages::model()->findByAttributes(array("user_id" => $this->id, "image_active" => 1))) {
            $result->user_avatar = true;
        } else {
            $result->user_avatar = false;
        }
        $result->rating = Ratings::getRatings($this->id, 1);
        $result->keyword = $this->keyword ? $this->keyword : '';
        //signature status
        $user_image = new UserImages;
        if ($signature = $user_image->s3UploadStatus($this->id, "signature")) {
            $result->user_signature = true;
        } else {
            $result->user_signature = false;
        }
        $contactDetails = Contacts::getContactDetails($userId, $this->id);

        if ($contactDetails) {
            //gender
            if ($this->user_gender == 1) {
                $result->user_gender = "male";
            } else if ($this->user_gender == 2) {
                $result->user_gender = "female";
            } else if ($this->user_gender == 3) {
                $result->user_gender = "ts";
            } else {
                $result->user_gender = null;
            }

            $result->user_dob = $this->user_dob;
            $result->user_address1 = $this->user_address1;
            $result->user_address2 = $this->user_address2;
            if ($contactDetails->contact_status == "contact" or $contactDetails->contact_status == "request_received") {
                if ($this->membership_plan == 0)
                    $result->user_type = 'Bronze';

                else if ($this->membership_plan == 1)
                    $result->user_type = 'Silver';

                else if ($this->membership_plan == 2)
                    $result->user_type = 'Gold';

                //Means he is a contact or has received a request
                if ($contactDetails->contact_incomingprofileid) {
                    $profile = UserProfiles::model()->findByPk($contactDetails->contact_incomingprofileid);
                } else {
                    $profile = UserProfiles::model()->findByAttributes(array('user_id' => $this->id));
                }
                if ($profile) {
                    //$result->profile_email = $profile->profile_email;
                    $result->profile_bio = $profile->profile_bio;
                    //$result->profile_mobile = $profile->profile_mobile;
                    //$result->profile_twitter = $profile->profile_twitter;
                    //$result->profile_linkedin = $profile->profile_linkedin;
                    //$result->profile_facebook = $profile->profile_facebook;
                    //$result->profile_skype = $profile->profile_skype;
                }
                $user_settings = UserSettings::getSettings($this->id);
                if ($user_settings) {
                    if ($user_settings->user_email_access_status)
                        $result->profile_email = $this->user_email;
                    if ($user_settings->user_contact_access_status)
                        $result->profile_mobile = $this->user_mobile;
                }
                $result->contact_status = $contactDetails->contact_status;
                $result->contact_notes = $contactDetails->contact_notes;

                if ($contactDetails->contact_outgoingprofileid) {
                    $result->contact_profileid = $contactDetails->contact_outgoingprofileid;
                } else {
                    $result->contact_profileid = $this->profile_id;
                }
            } 
            else if ($contactDetails->contact_status == "request_sent") {
               $result->contact_status = $contactDetails->contact_status;
            }
            else {
                $result->contact_status = "non_contact";
            }
        } else {
            $result->contact_status = "non_contact";
        }

        return $result;
    }

    public function logLogin() {
        $userLogins = new UserLogins;
        $userLogins->user_id = $this->id;
        $userLogins->login_ip = $_SERVER['REMOTE_ADDR'];
        $userLogins->save();
    }

    public static function getUserDetails($user) {
        //if the grant type is password assume that this is our official app and add additional data
        $response = array();
        $response['id'] = $user->id;
        $response['user_email'] = $user->user_email;
        $response['user_firstname'] = $user->user_firstname;
        $response['user_lastname'] = $user->user_lastname;
        $response['user_mobile'] = $user->user_mobile;
        $response['user_verificationdetails'] = $user->getVerificationDetails();
        $response['matchmaker'] = $user->matchmaker == 1 ? true : false;
        $country = new stdClass;
        if ($user->country_id) {
            $country->id = intval($user->country_id);
            $country->name = $user->country->country_name;
            $country->wallet_id = $user->country->wallet_id;
        }

        $response['country'] = $country;
        $response['user_defaultcommunity_id'] = $user->user_defaultcommunity_id;
        return $response;
    }

    public function registerEmail($to, $activation_link, $password = null, $pin = null, $custom_message = null, $normal_register = null) {
        $settings = array(
            'view' => 'register',
            'subject' => 'TAG System Registration'
        );
        $data = array(
            'name' => $to['firstname'] . ' ' . $to['lastname'],
            'email' => $to['email'],
            'normal_register' => $normal_register,
            'activationkey' => $activation_link,
            'password' => $password,
            'pin' => $pin,
            'custom_message' => $custom_message
        );
        $mail = new TagbondMail();
        $mail_stat = $mail->sendTagbondMail($settings, $data, $response);
        if ($mail_stat == true) {
            return true;
        } else {
            return false;
        }
    }

    public function resetPassword($user) {
        $user->user_forgotcode = md5(time() . rand() . $user->id);
        if ($user->save(false)) {
            // generate reset link
            $reset_link = $user->getSettings()->site_url . '/site/resetpassword?' .
                    Yii::app()->urlManager->createPathInfo(
                            array('forgotcode' => $user->user_forgotcode, 'userid' => $user->id), $equal = '=', $ampersand = '&', $key = NULL);
            $settings = array(
                'view' => 'forgotpassword',
                'subject' => 'Tagcash Reset Password'
            );
            $data = array(
                'name' => $user->user_firstname . ' ' . $user->user_lastname,
                'email' => $user->user_email,
                'resetlink' => $reset_link,
            );
            $mail = new TagbondMail();
            $mail_stat = $mail->sendTagbondMail($settings, $data, $response);
            if ($mail_stat == true) {
                return true;
            } else {
                return false;
            }
        }else{
            print_r($user->getErrors());
        }
    }

    public function getMobileVerificationCode() {
        $user = $this;
        $sms = 2; //00010
        //generate code
        $code = rand(1000, 9999);

        $collection = Yii::app()->edmsMongoCollection('u_' . $user->id);
        $mobileDoc = $collection->findOne(array(
            'mobile' => (string) $user->user_mobile
        ));

        $nowDate = date('Y-m-d H:i:s');
        if (!$mobileDoc) {
            $mobileDoc = new stdClass();
            $mobileDoc->mobile = (string) $user->user_mobile;
            $mobileDoc->verification_status = false;
            $mobileDoc->verification_code = intval($code);
            $collection->insert($mobileDoc);
        } else {
            $collection->update(array(
                '_id' => $mobileDoc['_id']
                    ), array('$set' => array(
                    'verification_status' => false,
                    'verification_code' => intval($code)
            )));

            //update verification
            $user->user_verified = $user->user_verified & (~$sms);
            $user->save();
        }

        return $code;
    }

    public function processMobileVerificationCode($code) {
        $user = $this;
        $sms = 2; //00010

        $collection = Yii::app()->edmsMongoCollection('u_' . $user->id);
        $mobileDoc = $collection->findOne(array(
            'mobile' => (string) $user->user_mobile
        ));

        if ($mobileDoc) {
            if ($mobileDoc['verification_code'] != (string) $code) {
                $collection->remove(array(
                    '_id' => $mobileDoc['_id']
                ));
            } else {
                $collection->update(array(
                    '_id' => $mobileDoc['_id']
                        ), array('$set' => array(
                        'verification_status' => true,
                )));

                //update verification
                if (!$user->getVerificationDetails()->sms_verified) {
                    $user->payReferrer();
                    $user->user_verified = $user->user_verified | $sms;
                    $user->save();
                }

                return true;
            }
        }

        return false;
    }

    private function payReferrer() {
        $user = $this;
        if ($user->user_referrer &&
                ($user->getSettings()->referral_amount && $user->getSettings()->wallet_id_default)) {
            $wallet_id = $user->getSettings()->wallet_id_default;
            $amount = $user->getSettings()->referral_amount;
            $toType = ($user->user_referrertype) ? $user->user_referrertype : 'user';
            $toId = $user->user_referrer;

            $transfer = new WalletTransfers;
            $transfer->setType('commission');
            $transfer->setTo($toType, $toId);
            $transfer->setWallets($wallet_id);
            $transfer->transfer_narration = 'Reference Commission';
            $transfer->transfer_from_amount = $amount;

            if ($transfer->transferInternal()) {
                return true;
            }

            //dbug::p($transfer->getErrors());
        }

        return false;
    }

    //Get contact requests and community invitations of a user.
    public function getRequests($relationType) {
        if (!is_numeric($this->pageCount) || $this->pageCount > 100) {
            $this->pageCount = 100;
        }

        if (!is_numeric($this->pageOffset)) {
            $this->pageOffset = 0;
        }

        $relation = 2;

        if ($this->username) {
            $search = ' AND ( t2.user_firstname LIKE :search OR t2.user_lastname LIKE :search) ';
            $search_c = ' AND (t2.community_name LIKE :search) ';
        }

        $query = 'SELECT t1.community_id AS contact_id, t2.community_name AS contact_name, "community" AS "type", t3.role_type ' .
                'FROM community_users t1 ' .
                'LEFT JOIN communities t2 ON t1.community_id = t2.id ' .
                'LEFT JOIN community_roles t3 ON t1.role_id = t3.id ' .
                'WHERE t1.user_id = :user_id AND t1.communityuser_status = 0 AND t1.communityuser_relation = :relation ' . $search_c .
                'UNION select t2.id AS contact_id, CONCAT(t2.user_firstname," ",t2.user_lastname) AS contact_name,"user" AS "type", "" AS "role_type"' .
                'from user_contacts t1 LEFT JOIN users t2 ON t1.contact_user1_id = t2.id ' .
                'where t2.community_type !=3 AND t1.contact_user2_id = :user_id AND contact_status = 0' . $search . ' LIMIT ' . $this->pageOffset . ' , ' . $this->pageCount;

        $rawData = Yii::app()->db->createCommand($query);
        $rawData->bindParam(":user_id", $this->id, PDO::PARAM_STR);
        $rawData->bindParam(":relation", $relation, PDO::PARAM_INT);

        $search_like = "%" . $this->username . "%";
        if ($this->username) {
            $rawData->bindParam(":search", $search_like, PDO::PARAM_STR);
        }

        $result = $rawData->queryAll();

        return $result;
    }

    public function getUserCommunities() {
        if (!is_numeric($this->pageCount) || $this->pageCount > 100) {
            $this->pageCount = 100;
        }

        if (!is_numeric($this->pageOffset)) {
            $this->pageOffset = 0;
        }
        $select = '
					t1.role_id, 
					t1.community_id,
					t2.longitude, 
					t2.latitude, 
					t2.category_id,
					t4.name as category_name,
					t1.user_id, t2.id, 
					t2.community_name,
					t2.community_city,
					t2.community_verified, 
					t3.role_name,
					t3.role_type';
        $query = Yii::app()->db->createCommand()
                ->from('community_users t1')
                ->leftJoin('communities t2', 't1.community_id = t2.id')
                ->leftJoin('community_roles t3', 't1.role_id = t3.id')
                ->leftJoin('categories t4', 't4.id = t2.category_id')
                ->where('t1.user_id = :userId AND t1.communityuser_status = 1 AND t2.community_type !=3', array(
            ':userId' => $this->id));

        if ($this->role_type) {
            if ($this->role_type == 'staff') {
                $query->andWhere('t3.role_type = 1 OR t3.role_type = 2');
            } else if ($this->role_type == 'member') {
                $query->andWhere('t3.role_type = 3');
            }
        }

        if ($this->community) {
            $query->andWhere('t2.community_name LIKE :community_name', array(':community_name' => "%" . $this->community . "%"));
        }
        $query->group('t1.community_id');

        if ($this->category_id) {
            $query->andWhere('category_id = :category_id', array('category_id' => $this->category_id));
        }

        if ($this->latitude && $this->longitude && $this->distance) {
            $select.=' , SQRT( POW( 69.1 * ( latitude - ' . $this->latitude . ' ) , 2 ) + POW( 69.1 * ( ' . $this->longitude . ' - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance';
            $query->having('distance <= ' . intval($this->distance));
            $query->order(array('distance'));
        }
        $query->select($select);
        $query->limit($this->pageCount, $this->pageOffset);

        $result = $query->queryAll();

        return $result;
    }

    public function checkUser() {
        if ($this->email) {
            $criteria = new CDbCriteria;
            $criteria->select = 'id';
            $criteria->condition = "user_email = '" . $this->email . "'";
            $user = Users::model()->findAll($criteria);
            if ($user) {
                return $user[0]['id'];
            } else {
                return false;
            }
        }
    }

    //update the user as KYC verified
    public function updateKycVerification($user_id, $verification_status) {
        $user = Users::model()->findByPk($user_id);
        if ($user) {
            //$verification_status = true : user is kyc verified.
            if ($verification_status == true) {
                if ($user->user_verified <= 128) {
                    $user->user_verified = $user->user_verified + 128;
                }
            } else {
                if ($user->user_verified >= 128) {
                    $user->user_verified = $user->user_verified - 128;
                }
            }
            if ($user->save())
                return true;
            else
                return false;
        } else
            return false;
    }

    //user search using id, email, mobile or name
    public function searchUser($params) {
        if (!is_numeric($params['pageCount']) || $params['pageCount'] > 100) {
            $params['pageCount'] = 100;
        }

        if (!is_numeric($params['pageOffset'])) {
            $params['pageOffset'] = 0;
        }

        $criteria = Yii::app()->db->createCommand()
                ->from('users u');

        $select = 'u.id,u.user_email,u.user_firstname,u.user_lastname,u.user_verified,country_id';
        if ($params['longitude'] && $params['latitude'] && $params['distance']) {
            $select.=' ,latitude, longitude, SQRT( POW( 69.1 * ( latitude - ' . $params['latitude'] . ' ) , 2 ) + POW( 69.1 * ( ' . $params['longitude'] . ' - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance';
            
            $criteria->having('distance <= ' . intval($params['distance']));
            $criteria->order(array('distance'));
        }

        $criteria->select($select);
        if ($params['online'])
                $criteria->andWhere('online=1');

        if ($params['id']) {
            $criteria->andWhere('u.id=:id', array(':id' => $params['id']));
        }
        if ($params['email']) {
            $criteria->andWhere('user_email=:email', array(':email' => $params['email']));
        }
        if ($params['mobile']) {
            $criteria->andWhere('user_mobile=:mobile1 OR user_mobile=:mobile2 OR user_mobile=:mobile3', array(':mobile1' => $params['mobile'], ':mobile2' => intval('0' . $params['mobile']), ':mobile3' => intval($params['isdcode'] . $params['mobile'])));
        }
        if ($params['name'] && $params['country_id']) {
            $criteria->andWhere("(CONCAT_WS(' ',user_firstname,user_lastname) LIKE :param) AND country_id = :country", array(
                ':param' => '%' . $params['name'] . '%',
                ':country' => $params['country_id']
            ));
        }

        if ($params['name'] && !$params['country_id']) {
            $criteria->andWhere("(CONCAT_WS(' ',user_firstname,user_lastname) LIKE :param)", array(
                ':param' => '%' . $params['name'] . '%',
            ));
        }

        if (!$params['name'] && $params['country_id']) {
            $criteria->andWhere('country_id  = :param', array(
                ':param' => $params['country_id']
            ));
        }

        if ($params['nickname']) {
            $criteria->andWhere('user_nickname LIKE :param', array(
                ':param' => $params['nickname'] . '%'
            ));
        }

        if ($params['keyword ']) {
            $criteria->andWhere('keyword LIKE :param', array(
                ':param' => $params['keyword '] . '%'
            ));
        }

        if ($params['gender'] || $params['age']) {
            if ($params['gender']) {
                if ($params['gender'] == 'male')
                    $gender = 1;

                if ($params['gender'] == 'female')
                    $gender = 2;

                if ($params['gender'] == 'ts')
                    $gender = 3;

                if ($params['gender'] == 'other') {
                    $criteria->andWhere('user_gender IS NULL');
                } else {
                    $criteria->andWhere('user_gender = :param', array(
                        ':param' => $gender
                    ));
                }
            }

            if ($params['age']) {
                if (Common::isJson($params['age'])) {
                    $age = json_decode($params['age'], true);
                    $criteria->andWhere('TIMESTAMPDIFF( YEAR, user_dob, CURDATE( ) ) BETWEEN ' . intval($age[0]) . ' AND ' . intval($age[1]));
                } else {
                    if (substr($params['age'], 0, 2) == 'gt') {
                        $criteria->andWhere('TIMESTAMPDIFF( YEAR, user_dob, CURDATE( ) ) > ' . intval(substr($params['age'], 2)));
                    }

                    if (substr($params['age'], 0, 2) == 'lt') {
                        $criteria->andWhere('TIMESTAMPDIFF( YEAR, user_dob, CURDATE( ) ) < ' . intval(substr($params['age'], 2)));
                    }

                    if (substr($params['age'], 0, 2) == 'eq') {
                        $criteria->andWhere('TIMESTAMPDIFF( YEAR, user_dob, CURDATE( ) ) = ' . intval(substr($params['age'], 2)));
                    }
                }
            }
        }

        if ($params['friends'] == true) {
            $criteria->join = " INNER JOIN `user_contacts` uc ON ((u.id = uc.contact_user1_id AND uc.contact_user2_id = $this->id)  OR (u.id = uc.contact_user2_id AND uc.contact_user1_id = $this->id)) AND contact_status = 1";
        }

        if ($params['city']) {
            $criteria->andWhere('user_city  = :param', array(
                ':param' => $params['city']
            ));
        }

        $criteria->limit($params['pageCount'], $params['pageOffset']);
        $user = $criteria->queryAll();

        /* if(!$user && !$params['online']){
          throw new CHttpException(403, "no_users");
          } */
        return $user;
    }

    public static function formatVerification($user_verification) {
        $verification = new stdClass;
        $email = 1; //00001
        $sms = 2; //00010
        //$passport = 4; //00100
        $data = 4;//00100 
        //$bill = 8; //01000
        $id = 8; //01000
        //$kyc = 16; //10000
        $image = 16; //10000
        $signature = 32; 
        $video=64;
        $kyc= 128;
        

        //email verification
       $verification->email_verified = false;
        if ($user_verification & $email) {
            $verification->email_verified = true;
        }

        //sms verification
        $verification->sms_verified = false;
        if ($user_verification & $sms) {
            $verification->sms_verified = true;
        }

        
        $verification->passport_verified = false;
         //data verification
        $verification->data_verified = false;
        if ($user_verification & $data) {
            $verification->data_verified = true;
            //passport verification depricated , keeping for compactability
            $verification->passport_verified = true;
        }

        $verification->bill_verified = false;
        $verification->id_verified = false;
        if ($user_verification & $id) {
            $verification->id_verified = true;
            //bill verification depricated , keeping for compactability
             $verification->bill_verified = true;
        }
        //image verification
        $verification->image_verified = false;
        if ($user_verification & $image) {
            $verification->image_verified = true;
        }
        //signature verification
        $verification->signature_verified = false;
        if ($user_verification & $signature) {
            $verification->signature_verified = true;
        }
        //video verification
        $verification->video_verified = false;
        if ($user_verification & $video) {
            $verification->video_verified = true;
        }
        
        //kyc verification
        $verification->kyc_verified = false;
        if ($user_verification & $kyc) {
            $verification->kyc_verified = true;
        }
        return $verification;
    }

    public static function getCountDown($time) {
        $date = strtotime($time) + (24 * 3600);
        $remaining = $date - time();

        $hours_remaining = floor(($remaining % 86400) / 3600);
        return $hours_remaining;
    }

    public function updateOnlineLocation($locationData) {
        foreach ($locationData as $location) {
            if ($location['user_id'] > 0) {
                $user = Users::model()->findByPk($location['user_id']);
                if ($user) {

                    $users[] = $location['user_id'];
                    $user->latitude = $location['lat'];
                    $user->longitude = $location['long'];
                    $user->online = 1;
                    $user->save();
                }
            }
        }
        return $users;
    }

    public function setUsersOffline($users) {
        foreach ($users as $user) {
            $user = Users::model()->findByPk($user);
            if ($user) {
                $user->online = 0;
                $user->save();
            }
        }
        return true;
    }

    public function testing()
    {
        return Users::model()->all();
    }
}
