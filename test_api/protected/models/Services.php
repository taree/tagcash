<?php
class Services extends CFormModel{

    public $response;

    public $pageCount;
    public $pageOffset;
    public $listType;

    public $serviceId;
    public $service;
    public $userId;
    public $user;
    public $communityId;

    public $owner;
    public $type;
    public $visibility;
    public $country;
    public $price;
    public $location;
    public $availability;
    public $country_id;

    public $serviceinfo;

    //massage
    public $duration;
    public $gender;
    public $massage_type;

    //common services
    public $services;
    public $rate;
    public $listing_type; //offered or wanted
    public $hours;

    public $mode;

    public $book_time;
    public $schedule;
    public $service_id;

    public function rules(){
        return array(
            array('owner, type, visibility, country, availability,location','required','message' => '{attribute}_required'),
            array('type','in','range'=>array('massage','handyman','plumber','driver','maid','cleaner','yaya'),'allowEmpty'=>false, 'message'=>'invalid_{attribute}'),
        );
    }

    public function setIdentity($thisObj){
        $this->userId = $thisObj->_USERID;
        $this->communityId = $thisObj->_COMMUNITYID;
        $this->user = $thisObj->_USEROBJ;
        return true;
    }

    //create service
    public function createService($data, $id, $type,$communityId=0){

        $this->owner = new stdClass;
        $this->serviceinfo = new stdClass;
        $collection = Yii::app()->edmsMongoCollection('services');
        $checkUser = $collection->findOne(array(
                                            'owner.id' => intval($id),
                                            //'owner.type' => 'user',
                                            'type' => $data['type'],
                                        ));
        if($checkUser){
            throw new CHttpException(403, "user_has_existing_service");
        }
        if ($type == "user" || $type == "community") {
            //for future
            $userModel = Users::model()->findByPk($id);
            $this->owner->id = intval($id);
            if ($communityId > 0) {
                $communityModal = Communities::model()->findByPk($communityId);
                if ($communityModal) {
                    $this->owner->name = $communityModal->community_name;
                    $this->owner->type = "community";
                    $this->owner->community_id = (int) $communityModal->id;
                } else {
                    throw new CHttpException(403, "community_info_not_found");
                }
            } else {
                $this->owner->type = "user";
                $this->owner->name = $userModel->user_firstname . " " . $userModel->user_lastname;
            }
        }

        //country
        if($data['country']){
            $this->country = $data['country'];
        }

        //visibility
        if($data['visibility']){
            $this->visibility = $data['visibility'];
        }

        //location
        $this->location = $data['location'];
        $this->availability = $data['availability'];
        $this->gender   = $data['gender'];
        $service_details = new stdClass;

        if($data['type'] == "massage"){
            $service_details->services = $data['massage_info']['massage_types'];
            $service_details->duration = $data['massage_info']['massage_duration'];
        }
        else{
            $this->mode = "service";
            $service_details->services  = $data['services'];
            $service_details->rate      = $data['rate'];
            $this->listing_type = $data['listing_type'];
        }

        $this->serviceinfo = $service_details;

        $this->type = $data['type'];

        $price = new stdClass();
        /*if($data['price']['amount'] > 0  && $data['price']['wallet_id']){
            $price->amount = $data['price']['amount'];
            if($data['price']['wallet_id']){
                $price->wallet_id = $data['price']['wallet_id'];
            }
        }*/
        $this->price = $price;

        if($this->validate()){
            $parsedData = new stdClass;
            $parsedData->owner          = $this->owner;
            $parsedData->price          = $this->price;
            $parsedData->country 		= intval($this->country);
            $parsedData->location 		= $this->location;
            $parsedData->availability   = $this->availability;
            $parsedData->gender         = $this->gender;
            $parsedData->visibility     = intval($this->visibility);
            $parsedData->type 			= $this->type;
            $parsedData->price          = $this->price;
            $parsedData->serviceinfo    = $this->serviceinfo;

            if($this->type != 'massage'){
                $parsedData->mode = $this->mode;
                $parsedData->listing_type =$this->listing_type;
            }
            $parsedData->status         = intval(1);
            $parsedData->last_login         = round(microtime(true) * 1000); //last login will be updated while user login

            

            $collection->insert($parsedData);

            if($data['image']){
                $imageData = base64_decode($data['image']);
                if(UserImages::checkAws() && UserImages::getImageType($imageData)){
                    $imageData = UserImages::convertImage($imageData);

                    $imageName = 'license_'.$parsedData->_id.".jpg";
                    $imagePath = Yii::app()->params['uploadPath'].'/licenses/';
                    $imageS3Name = 'license_'.$parsedData->_id.'.png';
                    $imageS3Path = 'uploads/licenses';

                    $resultS3 = Yii::app()->tamazon->uploadObject($imageS3Path, $imageS3Name, $imageData);
                    if(!$resultS3){
                        file_put_contents($imagePath.$imageName, $imageData);
                    }
                }
            }
            return (string)$parsedData->_id;
        }else{
            return false;
        }
    }

    public function editService($id,$data){
        if(!TagMongo::isValidId($id)){
            throw new CHttpException(403, "invalid_service_id");
        }
        $update = new stdClass;
        $collection = Yii::app()->edmsMongoCollection('services');
        $orderCollection = Yii::app()->edmsMongoCollection('services_orders');
        if(Yii::app()->request->getParam('type') == 'massage')
            $service = $collection->findOne(array(
            "_id" => new MongoID($id),
            "owner.id" => intval($this->userId),
            //"owner.type" => "user"
        ));
        else
        $service = $collection->findOne(array(
            "_id" => new MongoID($id),
            "owner.id" => intval($this->userId),
            "owner.type" => "user"
        ));
        if(!$service){
            throw new CHttpException(403, "invalid_service");
        }
        if($data['country'])
            $update->country = intval($data['country']);

        //location
        if($data['location'])
        {
            $this->location = $data['location'];
            $update->location = $data['location'];
        
        }

        //visibility
        if($data['visibility']){
            $update->visibility = intval($data['visibility']);
        }
        
        if($data['availability']){
            $update->availability = $data['availability'];
        }
        if($data['type'] == "massage"){
            if(isset($data['massage_info'])){
                $service_details = new stdClass;
                $service_details->services = $data['massage_info']['massage_types'];
                $service_details->duration = $data['massage_info']['massage_duration'];
            }
        }
        else{
            if($data['listing_type'])
                $update->listing_type = $data['listing_type'];
            if(isset($data['services']) || isset($data['rate'])){
                $service_details = new stdClass;
                $service_details->services  = $data['services']?$data['services']:$services['serviceinfo']['services'];
                $service_details->rate = $data['rate']?$data['rate']:$services['serviceinfo']['rate'];
            }
        }

        if(isset($data['price'])){
            $price = new stdClass();
            $price = $data['price'];
            /*
            if($data['price'] && ($data['price']['type'] != "none")){
                $price->amount = $data['price']['amount'];
                if($data['price']['type'] == "wallet"){
                    $price->type = 'wallet';
                    $price->wallet_id = $data['price']['wallet_id'];
                }
            }else{
                $price->type = "none";
            }*/
            $update->price = $price;
        }
        if($service_details)
            $update->serviceinfo = $service_details;
        if(Yii::app()->request->getParam('type') == 'massage'){
        $updateService = $collection->update(array(
            "_id" => new MongoID($id),
            "owner.id" => intval($this->userId),
            //"owner.type" => "user"
        ), array('$set' => $update));

        $updateServiceOrder = $orderCollection->update(array(
            "service_id" => new MongoID($id),
            "owner.id" => intval($this->userId),
            //"owner.type" => "user"
        ), array('$set' => $update));
        }
        else{
            $updateService = $collection->update(array(
            "_id" => new MongoID($id),
            "owner.id" => intval($this->userId),
            "owner.type" => "user"
        ), array('$set' => $update));

        $updateServiceOrder = $orderCollection->update(array(
            "service_id" => new MongoID($id),
            "owner.id" => intval($this->userId),
            "owner.type" => "user"
        ), array('$set' => $update));
        }
        if($updateService && $updateServiceOrder){
            if($data['image']){
                $imageData = base64_decode($data['image']);
                if(UserImages::checkAws() && UserImages::getImageType($imageData)){
                    $imageData = UserImages::convertImage($imageData);

                    $imageName = 'license_'.$parsedData->_id.".jpg";
                    $imagePath = Yii::app()->params['uploadPath'].'/licenses/';
                    $imageS3Name = 'license_'.$parsedData->_id.'.png';
                    $imageS3Path = 'uploads/licenses';

                    $resultS3 = Yii::app()->tamazon->uploadObject($imageS3Path, $imageS3Name, $imageData);
                    if(!$resultS3){
                        file_put_contents($imagePath.$imageName, $imageData);
                    }
                }
            }
            return true;
        }else{
            return false;
        }
    }

    //get service details
    public function getServiceInfo($id){
        $collection = Yii::app()->edmsMongoCollection('services');
        $service = $collection->findOne(array(
            "_id" => new MongoID($id),
        ));

        if($service){
            $object = self::formatService($service);
            return $object;
        }else{
            throw new CHttpException(403, "invalid_service_id");
        }
    }

    //get service order details
    public function getServiceOrderInfo($id, $userId = null,$ostatus = false){
        $collection = Yii::app()->edmsMongoCollection('services_orders');
        $service = $collection->findOne(array(
            "_id" => new MongoID($id),
        ));

        if($service){
            if($userId){
                $order_status = true;
            }
            else{
                $order_status = false;
                $id = null;
            }
            if($ostatus){
                $order_status = true;    
            }
            $object = self::formatService($service,$userId, $order_status,$id);
            return $object;
        }else{
            throw new CHttpException(403, "invalid_service_id");
        }
    }

    //list available services
    public function getServicesList($userId){
        SELF::checkAllRequestStatus();
        // pagination
        if(!is_numeric($this->pageCount) || $this->pageCount > 100){
            $this->pageCount = 100;
        }
        if(!is_numeric($this->pageOffset)){
            $this->pageOffset = 0;
        }
        if($this->schedule){
            $bookStatus = array(1);
            //$bookStatus = array(4);
            if($this->book_time == 0)
                $bookStatus = array(1);
            $schedule_timestamp = strtotime($this->schedule);
            $scheduleDayF = date('l', $schedule_timestamp);
            $scheduleTimeF = date('Hi', $schedule_timestamp);
            if($this->type == "massage"){
                $scheduleTimeT = $scheduleTimeF;
                $scheduleDayT = $scheduleDayF;
            }
            else{
                if($this->hours > 0)
                    $hour = $this->hours;
                else
                    $hour = 1;
                $schedule_timestampT = strtotime($this->schedule.' +'.($hour*60).' minutes');
                $scheduleDayT = date('l', $schedule_timestampT);
                if($scheduleDayT != $scheduleDayF)
                    return false;
                $scheduleTimeT = date('Hi', $schedule_timestampT);
            }

            $schedule = array('availability.'.strtolower($scheduleDayF).'.from' => array('$lte'=>intval($scheduleTimeF)),
                'availability.'.strtolower($scheduleDayT).'.to' => array('$gte'=>intval($scheduleTimeT)));
        }
        //search parameters
        if($this->type == "massage" && !$this->service_id){
            
            if (Yii::app()->request->getPost('location')) {
            $locationDataArr = json_decode(Yii::app()->request->getPost('location'), true);
            $postLocatonArr = array($locationDataArr['lng'],$locationDataArr['lat']);
        }
        if (Yii::app()->request->getPost('massageTherapistMaxRange')) {
            $massageTherapistRange = Yii::app()->request->getPost('massageTherapistMaxRange');
        }else{
            $massageTherapistRange = 5; // therapist deafault range 5
        }
        $massageTherapistRange=  floatval($massageTherapistRange/111.12);   //KM to radian convertion
        
        if($postLocatonArr){
            $params = array(
                '$and' => array(
                    array(
                        'type' => $this->type,
                        'status' => array('$in' => $bookStatus),
                        'visibility' => intval(1),
                        'gender' => $this->gender,
                    ),
                    array(
                        'owner.id' => array('$ne' => intval($userId)),
                    ),
//                    array(
//                        'serviceinfo.duration' => array('$in' => array(intval($this->duration))),
//                    ),
//                    array(
//                        'serviceinfo.services' => array('$in' => array(intval($this->massage_type))),
//                    ),
                    array(
                        'country' => intval($this->country_id),
                    )
                   
                ),
                 
                'location'=> array('$near'=>$postLocatonArr,'$maxDistance'=> $massageTherapistRange)
                 
            );
        }else{
            $params = array(
                '$and' => array(
                    array(
                        'type' => $this->type,
                        'status' => array('$in' => $bookStatus),
                        'visibility' => intval(1),
                        'gender' => $this->gender,
                    ),
                    array(
                        'owner.id' => array('$ne' => intval($userId)),
                    ),
//                    array(
//                        'serviceinfo.duration' => array('$in' => array(intval($this->duration))),
//                    ),
//                    array(
//                        'serviceinfo.services' => array('$in' => array(intval($this->massage_type))),
//                    ),
                    array(
                        'country' => intval($this->country_id),
                    )
                )
            );
        }
            //Common::pre(json_encode($params));exit;
        }else{
            $params = array(
                '$and' => array(
                    array(
                        'type' => $this->type,
                        'status' => intval(1),
                        'visibility' => intval(1),
                    ),
                    array(
                        'country' => intval($this->country_id),
                    ),
                    array(
                        'owner.id' => array('$ne' => intval($userId)),
                    )));
            if($this->type){
                $params['$and'][] = array('type' => $this->type);
            }
            if($this->service){
                $params['$and'][] = array(
                        'serviceinfo.services' => array('$in' => array($this->service)),
                    );
                    /*array(
                        'serviceinfo.handy.rate' => $this->handy_rate,
                    )*/
            }
        }
        if(is_array($schedule)){
            $params['$and'][] = $schedule;
        }
        if ($this->listing_type) {
            $params['listing_type'] = $this->listing_type;
        }
        $requests = array();
        //exclude those services who has not logged in 24 hrs 
        $time_before_24hr = round(microtime(true) * 1000) - 3600000*24;
        $params['last_login'] = array('$gte'=>$time_before_24hr);
        
        //for service request call
        if($this->service_id){
            $params['_id'] = new MongoID($this->service_id);
        }
        $collection = Yii::app()->edmsMongoCollection('services');
        //print_r(json_encode($params)); exit;
        $services = $collection->find($params)
            ->skip($this->pageOffset)->limit($this->pageCount);
        $parsedServices = array();
        if($services){
            //book ahead
            //check for available services at the given time
            if($this->book_time != 0){
                foreach($services as $service){
                    //for massages need to check the availibility with in the duration given.
                    $collectionLive = Yii::app()->edmsMongoCollection('services_orders');
                    $scheduleDate = new DateTime($this->schedule);
                    if($this->type == "massage"){
                        $paramLive = array('request_order.schedule' => array('$gte'=> $scheduleDate->format('Y-m-d 00:00:00')));
                    }
                    else{
                        $paramLive = array('request_order.schedule' => array('$gte'=> $scheduleDate->format('Y-m-d 00:00:00'), '$lte' => $scheduleDate->format('Y-m-d 23:59:59')));
                    }

                    $serviceId = $service['_id']->__toString();
                    $paramLive['service_id'] = new MongoID($serviceId);
                    $paramLive['type'] = $this->type;
                    $liveServices = $collectionLive->find($paramLive);
                    $toArrayLive = iterator_to_array($liveServices);
                    if(!empty($toArrayLive)){
                        if($this->type == "massage"){ //check for specific time and slot
                            foreach($liveServices as $liveService){
                                //check with duration too
                                $bookedTime = $liveService['request_order']['schedule'];
                                $duration = $liveService['request_order']['duration'];

                                switch($duration){
                                    case '0':
                                        $bookedTime = strtotime($bookedTime);
                                        $bookedTimeEnd = $bookedTime+(60*60);
                                        $scheduleTime = strtotime($this->schedule);
                                        if($this->duration == 0)
                                            $scheduleTimeEnd = $scheduleTime+(60*60);
                                        if($this->duration == 1)
                                            $scheduleTimeEnd = $scheduleTime+(60*90);
                                        if($this->duration == 2)
                                            $scheduleTimeEnd = $scheduleTime+(60*120);

                                        if($bookedTimeEnd<$scheduleTime){
                                            $skip = false;
                                            break;
                                        }
                                        else if($scheduleTimeEnd<$bookedTime){
                                            $skip = false;   
                                            break;
                                        }
                                        if($liveService['user_id'] == $userId){
                                            $requests[] = SELF::formatService($liveService,$userId,true,$liveService['_id']->__toString());
                                        }
                                        $skip = true;
                                        break;

                                    case '1':
                                        $bookedTime = strtotime($bookedTime);
                                        $bookedTimeEnd = $bookedTime+(60*90);
                                        $scheduleTime = strtotime($this->schedule);
                                        if($this->duration == 0)
                                            $scheduleTimeEnd = $scheduleTime+(60*60);
                                        if($this->duration == 1)
                                            $scheduleTimeEnd = $scheduleTime+(60*90);
                                        if($this->duration == 2)
                                            $scheduleTimeEnd = $scheduleTime+(60*120);
                                        if($bookedTimeEnd<$scheduleTime){
                                            $skip = false;
                                            break;
                                        }
                                        else if($scheduleTimeEnd<$bookedTime){
                                            $skip = false;   
                                            break;
                                        }
                                        if($liveService['user_id'] == $userId){
                                            $requests[] = SELF::formatService($liveService,$userId,true,$liveService['_id']->__toString());
                                        }
                                        
                                        $skip = true;
                                        break;
                                    case '2':
                                        $bookedTime = strtotime($bookedTime);
                                        $bookedTimeEnd = $bookedTime+(60*120);
                                        $scheduleTime = strtotime($this->schedule);
                                        if($this->duration == 0)
                                            $scheduleTimeEnd = $scheduleTime+(60*60);
                                        if($this->duration == 1)
                                            $scheduleTimeEnd = $scheduleTime+(60*90);
                                        if($this->duration == 2)
                                            $scheduleTimeEnd = $scheduleTime+(60*120);
                                        if($bookedTimeEnd<$scheduleTime){
                                            $skip = false;
                                            break;
                                        }
                                        else if($scheduleTimeEnd<$bookedTime){
                                            $skip = false;   
                                            break;
                                        }
                                        if($liveService['user_id'] == $userId)
                                            $requests[] = SELF::formatService($liveService,$userId,true,$liveService['_id']->__toString());
                                        
                                        $skip = true;
                                        break;
                                }

                            }
                            if(!$skip){
                                $object = self::formatService($service,null,false);
                                $parsedServices[] = $object;
                            }
                        }
                        else{//check for specific date. so if there is service in service_orders, not available for the day
                            foreach($liveServices as $liveService){
                                if($liveService['user_id'] == $userId)
                                    $requests[] = SELF::formatService($liveService,$userId,true,$liveService['_id']->__toString());
                            }
                            continue;
                        }
                    }
                    else{
                        $object = self::formatService($service,null,false);
                        $parsedServices[] = $object;
                    }
                }
            }
            else{
                foreach($services as $service){
                    $object = self::formatService($service);
                    $parsedServices[] = $object;
                }
            }
        }
        //get this users requests --for showing in app

        //$this->listType = 3;
        //$requests = $this->listRequests($userId);
        if(sizeOf($requests)>0)
            $parsedServices = array_merge($parsedServices, $requests);
        /*$this->listType = 4;
        $bookigs = $this->listRequests($userId);
        if($bookigs)
            $parsedServices[] = $bookigs;*/
        return $parsedServices;
    }

    public function myServices($userId,$ownerType='all'){
        // pagination
        if(!is_numeric($this->pageCount) || $this->pageCount > 100){
            $this->pageCount = 100;
        }
        if(!is_numeric($this->pageOffset)){
            $this->pageOffset = 0;
        }
        if($ownerType == 'all')
             $params = array(
            'owner.id' => intval($userId),
            //'owner.type' => 'user'
        );
        else if($ownerType == 'user')
        $params = array(
            'owner.id' => intval($userId),
            'owner.type' => 'user'
        );
        else if($ownerType == 'community')
        $params = array(
            'owner.id' => intval($userId),
            'owner.type' => 'community'
        );
        if($this->type)
            $params['type'] = $this->type;

        $collection = Yii::app()->edmsMongoCollection('services');
        $services = $collection->find($params)
            ->skip($this->pageOffset)->limit($this->pageCount);

        $parsedServices = array();

        if($services){
            $count = 0;
            foreach($services as $service){
                $object = self::formatService($service,null,false);
                $parsedServices[] = $object;
            }
        }

        return $parsedServices;
    }

    public function listBookings($userId,$ownerType='all'){
        // pagination
        if(!is_numeric($this->pageCount) || $this->pageCount > 100){
            $this->pageCount = 100;
        }
        if(!is_numeric($this->pageOffset)){
            $this->pageOffset = 0;
        }
        if($ownerType == 'all')
        $params = array(
                        'type' => $this->type,
                        'owner.id' => intval($userId),
                        //'owner.type' => 'user'  
                    );
        else if($ownerType == 'community')
             $params = array(
                        'type' => $this->type,
                        'owner.id' => intval($userId),
                        'owner.type' => 'community' 
                    );
        else if($ownerType == 'user')
             $params = array(
                        'type' => $this->type,
                        'owner.id' => intval($userId),
                        'owner.type' => 'user'  
                    );
        //Common::pre($params);exit;
        $collection = Yii::app()->edmsMongoCollection('services_orders');
        $services = $collection->find($params)
            ->skip($this->pageOffset)->limit($this->pageCount);

        $parsedServices = array();

        if($services){
            $count = 0;
            foreach($services as $service){
                $count++;
                $object = self::formatService($service,null,true,$service['_id']->__toString());
                $parsedServices[] = $object;
            }
        }

        return $parsedServices;
    }

    public function listRequests($userId){
        // pagination
        if(!is_numeric($this->pageCount) || $this->pageCount > 100){
            $this->pageCount = 100;
        }
        if(!is_numeric($this->pageOffset)){
            $this->pageOffset = 0;
        }
        $params = array(
            'type' => $this->type,
            'user_id' => intval($userId),
            'status' => intval($this->listType)
        );
        if($this->listType == 3 || $this->listType == 4 || $this->listType == 6){ //service requests, booked, or started services
            $collection = Yii::app()->edmsMongoCollection('services_orders');
        }else if($this->listType == 5 || $this->listType == 7){ //history
            $collection = Yii::app()->edmsMongoCollection('services_history');
        }else{
            throw new CHttpException(403, "invalid_list_type");
        }

        $services = $collection->find($params)
            ->skip($this->pageOffset)->limit($this->pageCount);

        $parsedServices = array();

        if($services){
            $count = 0;
            foreach($services as $service){
                $count++;
                $object = self::formatService($service);
                $parsedServices[] = $object;
            }
        }

        return $parsedServices;

    }

    public static function formatService($service, $userId = null, $oder_status = true, $order_id = null){
        $object = new stdClass();
        $object->id = $service['_id']->__toString();
        $serviceId = $object->id;
        if($service['service_id']){
            $object->service_id = $service['service_id']->__toString();
            $serviceId = $object->service_id;
        }
        $object->type = $service['type'];
        $object->owner = self::formatOwner($service['owner']);
        $object->status = $service['status'];
        $object->visibility = $service['visibility'];
        $object->country = $service['country'];
        $object->availability = $service['availability'];
        $object->location = $service['location'];
        $object->price = $service['price'];
        if($service['serviceinfo'])
            $object->serviceinfo = $service['serviceinfo'];
        else
            $object->serviceinfo = "";

        if($service['listing_type']){
            $object->listing_type = $service['listing_type'];    
        }        
        $object->image = '';
        $resultS3 = Yii::app()->tamazon->isObjectExist('uploads/licenses', 'license_'.$object->id.'.png');
        if($resultS3){
            $object->image = sprintf('http://%s.s3.amazonaws.com/uploads/licenses/%s', Yii::app()->tamazon->bucketRoot,'license_'.$object->id.'.png');
        }

        if($oder_status){
            $object->order_status = self::checkOrderStatus($serviceId,$userId,$order_id);
        }
        else
            $object->order_status = 0;

        if($service['mode']){
            $object->mode = $service['mode'];
        }
        if($service['user_id']){
            $object->user_id = $service['user_id'];
            $object->userinfo = self::formatUserInfo($service['userinfo']);
        }

        return $object;

    }

    public static function formatOwner($owner){
        $object = new stdClass;
        $object->id = intval($owner['id']);
        $object->name = $owner['name'];
        $object->type = $owner['type'];
        if($owner['community_id'])
        $object->community_id = $owner['community_id'];
        return $object;
    }

    public static function formatUserInfo($data){
        $object = new stdClass;
        $object->id = intval($data['id']);
        $object->user_firstname = $data['user_firstname'];
        $object->user_lastname = $data['user_lastname'];

        return $object;
    }

    public static function formatMassageOrder($id, $data){
        $object = new stdClass;
        $object->order_id = new MongoID($id);
        $object->book_time = $data['book_time'];
        $object->address = $data['address'];
        $object->schedule = $data['schedule'];
        $object->type = intval($data['type']);
        $object->duration = intval($data['duration']);
        $object->request_expiry = $data['request_expiry'];
        $object->time_left = (strtotime($data['request_expiry'])*1000)-(strtotime(date("Y-m-d H:i:s"))*1000);

        return $object;
    }

    public static function deleteServiceOrder($id){
        $collection = Yii::app()->edmsMongoCollection('services_orders');
        $collection->remove(array(
            "_id" => new MongoID($id)
        ));

        return true;
    }

    public static function checkOrderStatus($id,$userId = null,$order_id = null){
        $query = array(
            "service_id" => new MongoID($id));
        if($userId>0){
            $query['user_id'] = intval($userId);
        }
        if($order_id){
            $query['_id'] =  new MongoID($order_id);   
        }
        $collection = Yii::app()->edmsMongoCollection('services_orders');
        $service = $collection->findOne($query);
        //Common::pre($service);exit;
        if($service){
            if($service['status'] == intval(3)){
                if(date("Y-m-d H:i:s") >= $service['request_order']['request_expiry']){
                    self::deleteServiceOrder($service['_id']);
                    $orderInfo = 0;
                }else{
                    $orderInfo = new stdClass;
                    $orderInfo->status = intval($service['status']);
                    $orderInfo->order_id = new MongoID($service['_id']);
                    $orderInfo->book_time = $service['request_order']['book_time'];
                    $orderInfo->address = $service['request_order']['address'];
                    $orderInfo->schedule = $service['request_order']['schedule'];
                    $orderInfo->type = intval($service['request_order']['type']);
                    $orderInfo->duration = intval($service['request_order']['duration']);
                    $orderInfo->request_expiry = $service['request_order']['request_expiry'];
                    $orderInfo->time_left = (strtotime($service['request_order']['request_expiry'])*1000)-(strtotime(date("Y-m-d H:i:s"))*1000);
                }
            }else{
                $orderInfo = new stdClass;
                $orderInfo->order_id = new MongoID($service['_id']);
                $orderInfo->status = intval($service['status']);
                $orderInfo->address = $service['request_order']['address'];
                $orderInfo->book_time = $service['request_order']['book_time'];
                $orderInfo->type = $service['request_order']['type'];
                $orderInfo->duration = $service['request_order']['duration'];
                if($service['status'] == intval(4)){
                    if($service['request_order']['schedule'] == "now"){
                        $orderInfo->start_date = null;
                        $orderInfo->end_date = null;
                        $orderInfo->time_left = null;
                    }else {
                        $startDate = strtotime($service['request_order']['schedule']);
                        $orderInfo->start_date = $service['request_order']['schedule'];
                        if($service['type'] == 'massage'){
                            $durations = array(60, 90, 120);
                            $milli = $durations[$service['request_order']['duration']] * 60000;
                            $endDate = $startDate + (60 * $durations[$service['request_order']['duration']]);
                        }
                        else{
                            $milli = $durations[$service['request_order']['duration']] * 60000*60;
                            $endDate = $startDate + (60*60*intval($durations[$service['request_order']['duration']]));
                        }
                        
                        $formattedEndDate = date("Y-m-d H:i:s", $endDate);
                        $orderInfo->end_date = $formattedEndDate;
                        //$orderInfo->time_left = $milli;
                    }
                }else if($service['status'] == intval(6)){
                    $orderInfo->start_date = $service['request_order']['start_date'];
                    $orderInfo->end_date = $service['request_order']['end_date'];
                    if($service['type'] == 'massage')
                    $orderInfo->time_left = (strtotime($service['request_order']['end_date'])*1000)-(strtotime(date("Y-m-d H:i:s"))*1000);
                }
            }
            return $orderInfo;
        }else{
            return 0;
        }
    }

    public function requestService($id, $userId, $request){
        $service = self::getServiceInfo($id);
        if($service){
            $user = Users::model()->findByPk($userId);
            $this->service_id = $id;
            $this->schedule = $request->schedule;
            $this->book_time = $request->book_time;
            $this->type = $service->type;
            $this->duration = $request->duration;
            $this->country_id = $user->country_id;
            $result = SELF::getServicesList($userId);
            if(sizeOf($result)>0){
                if($result[0]->user_id == $userId){
                    throw new CHttpException(403, "already_requested");
                }
            }
            else
                throw new CHttpException(403, "service_not_available");
            
            if($userId == $service->owner->id){
                throw new CHttpException(403, "cant_request_own_service");
            }
            $parsedData = new stdClass;
            $parsedData->service_id     = new MongoID($id);
            $parsedData->status         = intval(3);
            $parsedData->owner          = $service->owner;
            $parsedData->price          = $service->price;
            $parsedData->country 		= $service->country;
            $parsedData->location 		= $service->location;
            $parsedData->visibility     = $service->visibility;
            $parsedData->type 			= $service->type;
            $parsedData->price          = $service->price;
            if($service->mode){
                $parsedData->mode = $service->mode;
            }
            $parsedData->serviceinfo    = $service->serviceinfo;
            $requestInfo = new stdClass;
            $requestInfo->address       = $request->address;
            $requestInfo->book_time     = $request->book_time;
            $requestInfo->schedule      = ($request->schedule)?$request->schedule:"now";
            $requestInfo->type          = intval($request->type);
            $requestInfo->duration      = intval($request->duration);
            if($request->number_of_therapist)
            $requestInfo->number_of_therapist      = intval($request->number_of_therapist);
            if($request->payment_method)
            $requestInfo->payment_method      = $request->payment_method;
            $date = date("Y-m-d H:i:s");
            $currentDate = strtotime($date);
            $afterFiveMinutes = $currentDate+(60*5);
            $expiryDate = date("Y-m-d H:i:s", $afterFiveMinutes);
            $requestInfo->request_expiry= $expiryDate;
            $requestInfo->time_left = intval(300000);
            $parsedData->request_order  = $requestInfo;
            $parsedData->listing_type  = $service->listing_type;
            $parsedData->user_id        = intval($userId);
            
            $userinfo = new stdClass;
            $userinfo->id               = intval($user->id);
            $userinfo->user_firstname   = $user->user_firstname;
            $userinfo->user_lastname    = $user->user_lastname;
            $parsedData->userinfo       = $userinfo;
            $orderCollection = Yii::app()->edmsMongoCollection('services_orders');
            $orderCollection->insert($parsedData);
            
            //send SMS to owner
             if ($service->owner->type == 'user' || $service->owner->type == 'community') {
                $owner = $service->owner->id;
                if ($service->owner->type == 'user') {
                    $owneruser = Users::model()->findByPk($owner);
                    $user_mobile = $owneruser['user_mobile'];
                } else if ($service->owner->type == 'community') {
                    $owneruser = Communities::model()->findByPk($service->owner->community_id);
                    $user_mobile = $owneruser['community_mobile'];
                }

                if ($user_mobile && $owneruser['country_id']) {
                    $calling_code = Countries::getCallingCode($owneruser['country_id']);
                    if ($calling_code) {
                        try {
                            $calling_code = (is_numeric($calling_code)) ? '+' . $calling_code : $calling_code;
                            $msg = "Hello,
                            You have received a service request from user: " . ucwords($userinfo->user_firstname) . ' ' . ucwords($userinfo->user_lastname);
                            $sid = "AC4811d35956e67b20096fe797cd6ffdfc";
                            $token = "de327aee73d15e5dfb4b831d1464d2e7";
                            require_once(Yii::app()->getBasePath() . '/extensions/twilio/Services/Twilio.php');
                            $client = new Services_Twilio($sid, $token);
                            $message = $client->account->sms_messages->create(
                                    '+15409310049', $calling_code . $user_mobile . '', '' . $msg . ''
                            );
                        } catch (Exception $e) {
                            
                            $message = array(
                                'message' => 'You have received a service request from user: ' . ucwords($userinfo->user_firstname) . ' ' . ucwords($userinfo->user_lastname),
                                'user_id' => $service->owner->id,
                                'user_type' => 1,
                                'notification_type' => 12,
                                'count' => 1
                                    //'vibrate' => 1,
                                    //'sound'       => 1
                            );
                            //sendMessage : to userid, totype, message, notification type (12 - new service request)
                            Gcm::sendMessage($message);

                            $response['result'] = "request_sent_notification_sms_failed";
                            $response['status'] = "success";
                            Rest::sendResponse(200, json_encode($response, JSON_NUMERIC_CHECK));
                        }
                    } else {
                        $message = array(
                            'message' => 'You have received a service request from user: ' . ucwords($userinfo->user_firstname) . ' ' . ucwords($userinfo->user_lastname),
                            'user_id' => $service->owner->id,
                            'user_type' => 1,
                            'notification_type' => 12,
                            'count' => 1
                                //'vibrate' => 1,
                                //'sound'       => 1
                        );
                        //sendMessage : to userid, totype, message, notification type (12 - new service request)
                        Gcm::sendMessage($message);
                        $response['result'] = "request_sent_notification_sms_failed";
                        $response['status'] = "success";
                        Rest::sendResponse(200, json_encode($response, JSON_NUMERIC_CHECK));
                    }
                } else {
                     $message = array(
                         'message' => 'You have received a service request from user: ' . ucwords($userinfo->user_firstname) . ' ' . ucwords($userinfo->user_lastname),
                        'user_id' => $service->owner->id,
                        'user_type' => 1,
                        'notification_type' => 12,
                        'count' => 1
                            //'vibrate' => 1,
                            //'sound'       => 1
                    );
                    //sendMessage : to userid, totype, message, notification type (12 - new service request)
                    Gcm::sendMessage($message);

                    $response['result'] = "request_sent_notification_sms_failed";
                    $response['status'] = "success";
                    Rest::sendResponse(200, json_encode($response, JSON_NUMERIC_CHECK));
                }
            }

            /*send notification to owner*/
            //send notifiction
            $message = array(
                'message'   => 'You have received a service request from user: '.ucwords($userinfo->user_firstname).' '.ucwords($userinfo->user_lastname),
                'user_id'   =>$service->owner->id,
                'user_type' =>1,
                'notification_type' => 12,
                'count' => 1
                //'vibrate' => 1,
                //'sound'       => 1
            );
            //sendMessage : to userid, totype, message, notification type (12 - new service request)
            Gcm::sendMessage($message);
            return true;
        }
        else{
            throw new CHttpException(403, "invalid_service_id");
        }
    }

    public function checkAllRequestStatus(){
        $collection = Yii::app()->edmsMongoCollection('services_orders');
        $services = $collection->find();
        if($services){
            foreach($services as $service){
                if($service['status'] == 3){
                    if(date("Y-m-d H:i:s") >= $service['request_order']['request_expiry']){
                        self::deleteServiceOrder($service['_id']);
                    }
                }
            }
        }
        return true;
    }

    public function acceptRequest($id){
        $serviceCollection = Yii::app()->edmsMongoCollection('services');
        $collection = Yii::app()->edmsMongoCollection('services_orders');
        $data = $collection->findOne(array("_id" => new MongoID($id)));
        if($data['status'] == 4){
            throw new CHttpException(403, "request_already_approved");
        }else{
            $query = array();
            $query['type'] = $data['type'];
            $query['owner.id'] = intval($this->userId);
            $query['request_order.schedule'] = $data['request_order']['schedule'];
            $query['status'] = array('$in' => array(4, 6));
            $query['user_id'] = array('$ne' => $data['user_id']);

            $hasSchedule = $collection->find($query);
            $parsedServices = array();
            foreach($hasSchedule as $sched){
                $object = new stdClass();
                $object->id = $sched['_id']->__toString();

                $parsedServices[] = $object;
            }

            if(sizeOf($parsedServices) > 0){
                throw new CHttpException(403, "schedule_already_taken");
            }else{
                $service = self::getServiceOrderInfo($id,$data['user_id']);
                //Common::pre($service->order_status);exit;
                $orderInfo = new stdClass;
                $orderInfo->service_order_id = new MongoID($service->order_status->order_id);
                $orderInfo->address = $service->order_status->address;
                $orderInfo->schedule = $service->order_status->schedule;
                $orderInfo->book_time = intval($service->order_status->book_time);
                $orderInfo->type = $service->order_status->type;
                $orderInfo->duration = $service->order_status->duration;
                $updateServiceOrder = $collection->update(array(
                    "_id" => new MongoID($id),
                ), array('$set' => array("status" => intval(4), "request_order" => $orderInfo)));
                $serviceCollection->update(array(
                    "_id" => new MongoID($data['service_id']),
                ), array('$set' => array("status" => intval(1))));
                if ($updateServiceOrder) {
                    $collection->remove(array(
                        "service_id" => new MongoID($data['service_id']),
                        "status" => intval(3)
                    ));

                    //send notification to user
                    $message = array(
                        'message'   => 'Your '.$query['type'].' service request accepted',
                        'user_id'   =>$service->userinfo->id,
                        'user_type' =>1,
                        'notification_type' => 13,
                        'count' => 1
                        //'vibrate' => 1,
                        //'sound'       => 1
                    );
                    //sendMessage : to userid, totype, message, notification type (13 - service request accepted)
                    Gcm::sendMessage($message);
                    return "request_accepted";
                }
                return false;
            }
        }
    }

    public function declineRequest($id){
        $collection = Yii::app()->edmsMongoCollection('services_orders');
        $service = $collection->findOne(array(
            "_id" => new MongoID($id),
        ));
        if($service){
            $collection->update(array(
                "_id" => new MongoID($id),
            ), array('$set' => array("status" => intval(5))));
            self::moveToHistory($id);
            return "request_declined";
        }
        return false;
    }

    public function unRegister($id, $userId){
        $query = array(
            "_id" => new MongoID($id));
        if($userId>0){
            $query['owner.id'] = intval($userId);
        }
        $collection = Yii::app()->edmsMongoCollection('services');
        $service = $collection->findOne($query);
        //Common::pre($service);exit;
        if($service){
            $collectionOrder = Yii::app()->edmsMongoCollection('services_orders');
            $service = $collectionOrder->findOne(array(
                "service_id" => new MongoID($id),
            ));
            if($service)
                throw new CHttpException(403, 'service_arealdy_in_use');
            else{
                $collection->remove(array(
                    "_id" => new MongoID($id)
                ));
                return true;
            }
        }
        return false;
    }

    public static function moveToHistory($id){
        $collection = Yii::app()->edmsMongoCollection('services_orders');
        $service = $collection->findOne(array(
            "_id" => new MongoID($id),
        ));
        $history = new stdClass();
        $history->service_id           = $service['service_id'];
        $history->status            = $service['status'];
        $history->owner             = $service['owner'];
        $history->price             = $service['price'];
        $history->country 		    = $service['country'];
        $history->location 		    = $service['location'];
        $history->visibility        = $service['visibility'];
        $history->type 			    = $service['type'];
        $history->price             = $service['price'];
        $history->serviceinfo       = $service['serviceinfo'];
        if($service['mode']){
            $history->mode = $service['mode'];
        }
        $history->request_order     = $service['request_order'];
        $history->user_id           = $service['user_id'];
        $history->userinfo          = $service['userinfo'];
        $historyCollection = Yii::app()->edmsMongoCollection('services_history');
        $historyCollection->insert($history);

        $collection->remove(array(
            "_id" => new MongoID($id)
        ));

        return true;
    }

    public function activateService($id){
        $collection = Yii::app()->edmsMongoCollection('services');
        $data = $collection->findOne(
            array(
                "_id" => new MongoID($id),
                "owner.id" => intval($this->userId),
            )
        );
        if(!$data){
            throw new CHttpException(403, "action_not_allowed_for_non_owner");
        }

        $activate = $collection->update(array(
            "_id" => new MongoID($id),
        ), array('$set' => array("status" => intval(1))));

        if($activate){
            return true;
        }
        return false;
    }

    public function deactivateService($id){
        $collection = Yii::app()->edmsMongoCollection('services');
        $orderCollection = Yii::app()->edmsMongoCollection('services_orders');
        $data = $collection->findOne(
            array(
                "_id" => new MongoID($id),
                "owner.id" => intval($this->userId),
            )
        );
        if(!$data){
            throw new CHttpException(403, "action_not_allowed_for_non_owner");
        }

        $params['status'] = array('$in' => array(4,6));
        $params['service_id'] = new MongoID($id);
        $orders = $orderCollection->find($params);
        foreach($orders as $order){
            $object = new stdClass();
            $object->id = $order['_id']->__toString();

            $parsedServices[] = $object;
        }

        if(sizeOf($parsedServices) > 0){
            throw new CHttpException(403, 'service_has_active_orders');
        }else{
            $deactivate = $collection->update(array(
                "_id" => new MongoID($id),
            ), array('$set' => array("status" => intval(2))));

            if($deactivate){
                return true;
            }
            return false;
        }

    }

    public function startService($id){
        $service = self::getServiceOrderInfo($id,null,true);
        //Common::pre($service->order_status);exit;
        $orderInfo = new stdClass;
        $orderInfo->order_id = new MongoID($service->order_status->order_id);
        $orderInfo->address = $service->order_status->address;
        $orderInfo->book_time = intval($service->order_status->book_time);
        $orderInfo->type = $service->order_status->type;
        $orderInfo->duration = $service->order_status->duration;
        $orderInfo->start_date = date("Y-m-d H:i:s");
        $durations = array(60,90,120);
        $orderInfo->time_left = $durations[$service->order_status->duration]*60000;
        $currentDate = strtotime(date("Y-m-d H:i:s"));
        $endDate = $currentDate+(60*$durations[$service->order_status->duration]);
        $orderInfo->end_date = date("Y-m-d H:i:s", $endDate);
        $collection = Yii::app()->edmsMongoCollection('services_orders');
        $serviceCollection = Yii::app()->edmsMongoCollection('services');
        $start = $collection->update(array(
            "_id" => new MongoID($id),
        ), array('$set' => array("status" => intval(6), "request_order" => $orderInfo)));
        //if($service->type == 'massage')
            if($start){
                $serviceCollection->update(array(
                    "_id" => new MongoID($service->service_id),
                ), array('$set' => array("status" => intval(6))));
                return true;
            }
        return false;
    }

    public function endService($id){
        $collection = Yii::app()->edmsMongoCollection('services_orders');
        $serviceCollection = Yii::app()->edmsMongoCollection('services');
        $end = $collection->update(array(
            "_id" => new MongoID($id),
        ), array('$set' => array("status" => intval(7))));
        $service = self::getServiceOrderInfo($id);
        $serviceCollection->update(array(
            "_id" => new MongoID($service->service_id),
        ), array('$set' => array("status" => intval(1))));
        if($end){
            self::moveToHistory($id);
            return true;
        }
        return false;
    }

    public function offeredServiceInfo($type){
        $collection = Yii::app()->edmsMongoCollection('services');
        $service = $collection->findOne(array(
            "owner.id" => intval($this->userId),
            "type" => $type,
        ));

        if($service){
            $object = self::formatService($service,null,false);
            return $object;
        }else{
            throw new CHttpException(403, "no_offered_service");
        }
    }

    //create service
    public function createServiceLook($data, $id, $type){

        $this->owner = new stdClass;
        $this->serviceinfo = new stdClass;

        if($type == "user"){
            //for future
            $userModel = Users::model()->findByPk($id);
            $this->owner->id = intval($id);
            $this->owner->type = "user";
            $this->owner->name = $userModel->user_firstname." ".$userModel->user_lastname;
        }

        //country
        if($data['country']){
            $this->country = $data['country'];
        }

        //visibility
        if($data['visibility']){
            $this->visibility = $data['visibility'];
        }

        //location
        $this->location = $data['location'];

        $service_details = new stdClass;


        $this->mode = "wanted";
        $service_details->handy['type'] = $data['handy_info']['handy_type'];
        $service_details->handy['rate'] = $data['handy_info']['handy_rate'];
        $service_details->handy['schedule'] = $data['handy_info']['schedule'];
        $service_details->handy['address'] = $data['handy_info']['address'];

        $this->serviceinfo = $service_details;

        $this->type = "handyman";

        $price = new stdClass();
        $price->type = "none";

        $this->price = $price;

        if($this->validate()){
            $parsedData = new stdClass;
            $parsedData->owner          = $this->owner;
            $parsedData->price          = $this->price;
            $parsedData->country 		= $this->country;
            $parsedData->location 		= $this->location;
            $parsedData->visibility     = intval($this->visibility);
            $parsedData->type 			= $this->type;
            $parsedData->price          = $this->price;
            $parsedData->serviceinfo    = $this->serviceinfo;
            $parsedData->mode           = $this->mode;
            $parsedData->status         = intval(1);

            $collection = Yii::app()->edmsMongoCollection('services');

            $collection->insert($parsedData);

            if($data['image']){
                $imageData = base64_decode($data['image']);
                if(UserImages::checkAws() && UserImages::getImageType($imageData)){
                    $imageData = UserImages::convertImage($imageData);

                    $imageName = 'license_'.$parsedData->_id.".jpg";
                    $imagePath = Yii::app()->params['uploadPath'].'/licenses/';
                    $imageS3Name = 'license_'.$parsedData->_id.'.png';
                    $imageS3Path = 'uploads/licenses';

                    $resultS3 = Yii::app()->tamazon->uploadObject($imageS3Path, $imageS3Name, $imageData);
                    if(!$resultS3){
                        file_put_contents($imagePath.$imageName, $imageData);
                    }
                }
            }

            return (string)$parsedData->_id;
        }else{
            return false;
        }
    }

    //update login time: Include in service search only those who have logged in 24hrs
    public static function updateLastLogin($userId,$type = 'user'){
        if($userId>0){
            $update = new stdClass;
            $collection = Yii::app()->edmsMongoCollection('services');
            $services = $collection->findOne(array(
                "owner.id" => intval($userId),
                //"owner.type" => $type,  // commented as type may be community or user so checking only owner id
            )); 
            if($services){
                $update->last_login = round(microtime(true) * 1000);
                $collection = Yii::app()->edmsMongoCollection('services');
                $stat = $collection->update(array(
                                "owner.id" => intval($userId),
                                //"owner.type" => $type,   // commented as type may be community or user so checking only owner id
                        ), array('$set' => $update), array('multiple' => true));
            }
        }
        return true;
    }
}