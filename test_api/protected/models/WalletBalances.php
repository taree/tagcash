<?php

/**
 * This is the model class for table "wallet_balances".
 *
 * The followings are the available columns in table 'wallet_balances':
 * @property integer $balance_id
 * @property integer $balance_type
 * @property string $balance_amount
 * @property string $balance_promised
 * @property integer $wallet_type_id
 *
 * The followings are the available model relations:
 * @property WalletTypes $walletType
 */
class WalletBalances extends CActiveRecord {

    public $userId;
    public $communityId;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wallet_balances';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('balance_id, balance_type, wallet_type_id', 'required'),
            array('balance_id, balance_type, wallet_type_id', 'numerical', 'integerOnly' => true),
            array('wallet_type_id', 'validateWalletType'),
            array('balance_amount, balance_promised', 'length', 'max' => 19),
            array('balance_amount', 'numerical', 'min' => 0),
            array('balance_amount', 'validateBalance'),
            array('balance_promised', 'numerical', 'min' => 0),
            array('balance_promised', 'validatePromised'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('balance_id, balance_type, balance_amount, balance_promised, wallet_type_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'walletType' => array(self::BELONGS_TO, 'WalletTypes', 'wallet_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'balance_id' => 'Balance',
            'balance_type' => 'Balance Type',
            'balance_amount' => 'Balance Amount',
            'balance_promised' => 'Balance Promised',
            'wallet_type_id' => 'Wallet Type',
        );
    }

    public function validateWalletType($attribute, $params) {
        if ($this->isNewRecord) {
            $type = WalletTypes::model()->findByPk($this->wallet_type_id);
            if (!$type) {
                $this->addError($attribute, 'invalid_wallet_type');
            }

            $wallet = WalletBalances::model()->findByPk(array(
                'balance_id' => $this->balance_id,
                'balance_type' => $this->balance_type,
                'wallet_type_id' => $this->wallet_type_id));
            if ($wallet) {
                $this->addError($attribute, 'duplicate_wallet');
            }
        }
    }

    public function validateBalance($attribute, $params) {
        if ($this->balance_amount) {
            if ($this->balance_type != 3 && $this->balance_amount < 0) {
                $this->addError($attribute, 'wallet_error');
            }
        }
    }

    public function validatePromised($attribute, $params) {
        //validate amount going to add
        if (is_array($params) && $params['amount']) {
            if (($this->balance_promised + $params['amount']) <= $this->balance_amount) {
                return true;
            } else {
                $this->addError($attribute, 'wallet_promised');
                return false;
            }
        }

        //validate current
        if ($this->balance_promised > $this->balance_amount) {
            $this->addError($attribute, 'insufficient_wallet_balance');
        }
    }

    public function checkWalletBalance($amount) {
        if ($this && !$this->validate()) {
            return false;
        } else if ($this && $this->balance_type != 3) {
            $balance = ($this->balance_amount - $this->balance_promised) - $amount;

            if ($balance < 0) {
                return false;
            }
        }

        return true;
    }

    public static function checkIfCanPay($balanceId, $balanceType, $walletId, $amount) {
        $walletBalance = self::getWalletBalance($balanceId, $balanceType, $walletId);

        if ($amount && $walletBalance['amount'] >= $amount) {
            return true;
        }

        return false;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('balance_id', $this->balance_id);
        $criteria->compare('balance_type', $this->balance_type);
        $criteria->compare('balance_amount', $this->balance_amount, true);
        $criteria->compare('balance_promised', $this->balance_promised, true);
        $criteria->compare('wallet_type_id', $this->wallet_type_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WalletBalances the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function seeErrors() {
        $errors = $this->getErrors();
        foreach ($errors as $key => $value) {
            if ($key != "transfer_date") {
                return $value[0];
            }
        }
    }

    public static function formatWalletType($walletType) {
        switch ($walletType) {
            case 1:
                return 'crypto';
            case 2:
                return 'tag_points';
            case 3:
                return 'community_points';
            default:
                return 'normal';
        }
    }

    public function setIdentity($thisObj) {
        $this->communityId = $thisObj->_COMMUNITYID;
        $this->userId = $thisObj->_USERID;
        return true;
    }

    public function listAll($filter = array(), $filterWallet = array()) {
        if ($this->communityId) {
            $filter['balance_id'] = $this->communityId;
            $filter['balance_type'] = 2;
            $country = Communities::model()->findByPk($this->communityId);
            $hasADV = self::model()->findByAttributes(array('balance_id' => $filter['balance_id'], 'balance_type' => $filter['balance_type'], 'wallet_type_id' => 523));
            if (!$hasADV) {
                WalletBalances::addWallet($this->communityId, 2, 523, 1000);
            }
        } else if ($this->userId) {
            $filter['balance_id'] = $this->userId;
            $filter['balance_type'] = 1;
            $country = Users::model()->findByPk($this->userId);
            /* PLAY points are given at the time of mobile verification : UserController::actionVerifySMSCode
              $hasPLAY = self::model()->findByAttributes(array('balance_id'=>$filter['balance_id'],'balance_type'=>$filter['balance_type'],'wallet_type_id'=>525));
              if(!$hasPLAY){
              WalletBalances::addWallet($this->userId, 1, 525, 1000);
              } */
        } else {
            throw new CHttpException(403, "internal_error");
        }
        if ($country->country_id == 174) {
            WalletBalances::addWallet($filter['balance_id'], $filter['balance_type'], 1, 0);
            WalletBalances::addWallet($filter['balance_id'], $filter['balance_type'], 7, 0);
        }
        //wallet type's filter
        //$conditionWallet = '1';
        $paramsWallet = array();

        if ($filterWallet['currency_code']) {
            $conditionWallet.= ' walletType.currency_code=:currencyCode AND ';
            $paramsWallet[':currencyCode'] = $filterWallet['currency_code'];
        }

        if ($filterWallet['community_id']) {
            $conditionWallet.= ' walletType.community_point_id=:communityId AND ';
            $paramsWallet[':communityId'] = $filterWallet['community_id'];
        }
        if ($filterWallet['wallet_type']) {
            $wallet_type = explode(',', $filterWallet['wallet_type']);
            $i = 1;
            $conditionWallet_type = '';
            foreach ($wallet_type as $wallet) {

                if ($wallet == 'crypto') {
                    if (count($walletType) > 0) {
                        $conditionWallet_type.= ' OR walletType.wallet_type=:walletType' . $i . '';
                    } else
                        $conditionWallet_type.= ' walletType.wallet_type=:walletType' . $i . '';
                    $walletType[] = 1; //crypto
                    $i++;
                }
                else if ($wallet == 'cash') {
                    if (count($walletType) > 0)
                        $conditionWallet_type.= ' OR walletType.wallet_type=:walletType' . $i . '';
                    else
                        $conditionWallet_type.= ' walletType.wallet_type=:walletType' . $i . '';
                    $walletType[] = 0; //cash
                    $i++;
                }
                else if ($wallet == 'tag') {
                    if (count($walletType) > 0)
                        $conditionWallet_type.= ' OR walletType.wallet_type=:walletType' . $i . '';
                    else
                        $conditionWallet_type.= ' walletType.wallet_type=:walletType' . $i . '';
                    $walletType[] = 2; //tag point
                    $i++;
                }
                else if ($wallet == 'point') {
                    if (count($walletType) > 0)
                        $conditionWallet_type.= ' OR walletType.wallet_type=:walletType' . $i . '';
                    else
                        $conditionWallet_type.= ' walletType.wallet_type=:walletType' . $i . '';
                    $walletType[] = 3; //community points
                    $i++;
                }
            }

            if (count($walletType) > 0) {
                $conditionWallet.= '(' . $conditionWallet_type . ') AND ';
            }
            $i = 1;
            if ($walletType) {
                foreach ($walletType as $type) {
                    $paramsWallet[':walletType' . $i . ''] = $type;
                    $i++;
                }
            }
        }
        $conditionWallet .= " 1";
        if ($conditionWallet) {
            $walletType = array(
                'walletType' => array(
                    'condition' => $conditionWallet,
                    'params' => $paramsWallet,
                )
            );
        }


        $list = WalletBalances::model()->with($walletType)->findAllByAttributes($filter);
        return $list;
    }

    public function create() {
        if ($this->balance_type == 3) {
            $exist = Administrators::model()->findByPk($this->balance_id);
        } else if ($this->balance_type == 2) {
            $exist = Communities::model()->findByPk($this->balance_id);
        } else {
            $exist = Users::model()->findByPk($this->balance_id);
        }

        if (!$exist) {
            $this->addError('balance_id', 'user/community_does_not_exists');
            return false;
        }

        if (!$this->save()) {
            return false;
        }

        return true;
    }

    public function deleteWallet() {
        if ($this->balance_amount == 0) {
            if ($this->delete()) {
                return true;
            }

            throw new CHttpException(403, "internal_error");
        }

        throw new CHttpException(403, "balance_not_zero");
    }

    public function updatePromised($amount, $minus) {
        if ($amount === 0) {
            return true;
        } else if ($minus) {
            if (($this->balance_promised - $amount) >= 0) {
                $this->balance_promised -= $amount;
                if ($this->save()) {
                    return true;
                }
            }

            $this->balance_promised = 0;
            $this->save();
            return true;
        } else {
            if (($this->balance_promised + $amount) <= $this->balance_amount) {
                $this->balance_promised += $amount;
                $this->save();
                return true;
            }
        }

        return false;
    }

    public static function total($filter = array(), $count = 100, $offset = 0) {
        $query = Yii::app()->db->createCommand()
                ->select('wb.wallet_type_id, wt.wallet_name, wt.currency_code, SUM(wb.balance_amount) AS wallet_total')
                ->from('wallet_balances wb')
                ->group('wb.wallet_type_id');

        $query = $query->join("wallet_types wt", "wt.id = wb.wallet_type_id");

        if ($filter['type']) {
            if ($filter['type'] == 'user') {
                $filter['type'] = 1;
            } else if ($filter['type'] == 'community') {
                $filter['type'] = 2;
            } else if ($filter['type'] == 'admin') {
                $filter['type'] = 3;
            }

            if ($filter['type'] == 'user-community') {
                $query->andWhere('wb.balance_type = 1 OR wb.balance_type = 2');
            } else {
                $query->andWhere('wb.balance_type = :balance_type', array(
                    ':balance_type' => $filter['type'],
                ));
            }
        }

        if ($filter['wallet_type_id']) {
            $query->andWhere('wb.wallet_type_id = :wallet_type_id', array(
                ':wallet_type_id' => $filter['wallet_type_id'],
            ));
        }

        if ($filter['currency_code']) {
            $query->andWhere('wt.currency_code = :currency_code', array(
                ':currency_code' => $filter['currency_code'],
            ));
        }

        $result = $query->queryAll();
        return $result;
    }

    public static function addWallet($userId, $type, $walletId, $amount = NULL) {

        $model = WalletBalances::model()->findByAttributes(array('balance_id' => $userId, 'balance_type' => $type, 'wallet_type_id' => $walletId));
        if ($model) {
            $command = Yii::app()->db->createCommand();
            $command->update(
                    'wallet_balances', array('balance_amount' => ($model->balance_amount + $amount)), 'balance_id= ' . $userId . ' and balance_type =' . $type . ' and wallet_type_id = ' . $walletId);

            $command->execute();
        } else {
            $model = new WalletBalances;

            $model->balance_id = $userId;
            $model->balance_type = $type;
            $model->balance_amount = $amount;
            $model->balance_promised = 0;
            $model->wallet_type_id = $walletId;

            $model->save();
        }
    }

    public static function getWalletBalance($balanceId, $balanceType, $walletId) {
        $model = self::model()->findByAttributes(array('balance_id' => $balanceId, 'balance_type' => $balanceType, 'wallet_type_id' => $walletId));

        $walletBalance['amount'] = ($model->balance_amount - $model->balance_promised);
        $walletBalance['walletId'] = $model->wallet_type_id;


        return $walletBalance;
    }

    public static function getUserWalletBalances($community_id, $user_id = null) {
        //show balances of community members
        $wallet_type = WalletTypes::model()->findAllByAttributes(array(
            'community_point_id' => $community_id));
        if (sizeof($wallet_type) > 0) {
            $query = Yii::app()->db->createCommand()
                    ->select('wb.balance_id,
									wb.balance_amount,
									wb.balance_promised,
									wb.wallet_type_id,
									wt.wallet_name,
									wt.currency_code')
                    ->from('wallet_balances wb')
                    ->leftJoin('wallet_types wt', 'wt.id = wb.wallet_type_id')
                    ->where('wb.balance_type = 1');
            if ($user_id) {
                foreach ($wallet_type as $wallet) {
                    $query->andWhere('wb.wallet_type_id = :wallet_id AND wb.balance_id = :user_id', array(':wallet_id' => $wallet->id,
                        ':user_id' => $user_id));
                    $rslt = $query->queryAll();
                    if ($rslt)
                        $result[] = $rslt;
                }
            }
            else {
                foreach ($wallet_type as $wallet) {
                    $query->andWhere('wb.wallet_type_id = :wallet_id', array(':wallet_id' => $wallet->id));
                    $rslt = $query->queryAll();
                    if ($rslt)
                        $result[] = $rslt;
                }
            }
            return $result;
        } else
            return false;
    }

    public static function getWalletAddress($id, $type, $wallet, $callback = null) {
        $walletAddress = WalletBalances::model()->findByAttributes(array(
            'balance_id' => $id,
            'balance_type' => $type,
            'wallet_type_id' => $wallet
        ));
        if (!$walletAddress->receiving_address) {
            $settings = SiteSettings::getSettings();

            if (!$callback) {
                $callback = urlencode(yii::app()->params['site_url']);
            }

            $url = "https://api.blockchain.info/v2/receive?xpub=" . $settings->blockchain_xpub . "&callback=" . $callback . "&key=" . $settings->blockchain_key;
            //$url = "https://blockchain.info/merchant/".$settings->blockchain_guid."/new_address?password=".$settings->blockchain_main_password;
            //die($url);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = json_decode(curl_exec($ch), TRUE);

            if ($response['address']) {
                if (!$walletAddress) {
                    $walletAddress = new WalletBalances();

                    $walletAddress->balance_id = $id;
                    $walletAddress->balance_type = $type;
                    $walletAddress->wallet_type_id = $wallet;
                }

                $walletAddress->receiving_address = $response['address'];
                $walletAddress->save();
            } else {
                return false;
            }
        }

        return WalletBalances::model()->findByAttributes(array(
                    'balance_id' => $id,
                    'balance_type' => $type,
                    'wallet_type_id' => $wallet
                ))->receiving_address;
    }

    public static function getNewWalletAddress($callback) {

        $settings = SiteSettings::getSettings();
        $url = "https://api.blockchain.info/v2/receive?xpub=" . $settings->blockchain_xpub . "&callback=" . $callback . "&key=" . $settings->blockchain_key."&gap_limit=1000";
        //die($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($ch), TRUE);
        if ($response['address']) {
            return $response['address'];
        } else {
            return false;
        }
    }

    public static function getTagValue($id, $amount) {
        $query = Yii::app()->db->createCommand()
                ->select("tag_value")
                ->from("reward_tag_value")
                ->where('wallet_type=:id', array(':id' => $id))
                ->queryRow();

        if ($query) {
            return $query['tag_value'] * $amount;
        }
        return $query;
    }

    /**
     * This function is for getting all wallets of a user or community
     * User types:
     * 1 = user
     * 2 = community
     */
    public static function getAllWalletsOfUser($id, $type, $walletType = false) {
        $params = array(':id' => $id, ':balanceType' => $type, ':type' => $walletType);

        $query = 't2.wallet_type=:type and t1.balance_id=:id and t1.balance_type = :balanceType and t1.wallet_type_id = t2.id';
        if ($type == 2) {
            $params = array(':id' => $id, ':balanceType' => $type);
            $query = 't1.balance_id=:id and t1.balance_type = :balanceType and t1.wallet_type_id = t2.id';
        } else if ($type == 1) {
            $params = array(':id' => $id, ':balanceType' => $type);
            $query = 't1.balance_id=:id and t1.balance_type = :balanceType and t1.wallet_type_id = t2.id';
        }

        if ($walletType) { //jeremy
            $params = array(':id' => $id, ':balanceType' => $type, ':walletType' => $walletType);
            $query = $query . " AND wallet_type_id = :walletType";
        }

        $model = Yii::app()->db->createCommand()
                ->select('*,(t1.balance_amount - t1.balance_promised) as deduct_amount, t2.currency_code')
                ->from('wallet_balances t1, wallet_types t2')
                ->where($query, $params)
                ->queryAll();

        return $model;
    }

    public static function getTagTypeWallet($id, $type) {
        $query = Yii::app()->db->createCommand()
                ->select('*,(t1.balance_amount - t1.balance_promised) as deduct_amount, t2.currency_code')
                ->from('wallet_balances t1')
                ->leftjoin('wallet_types t2', 't2.id = t1.wallet_type_id')
                ->where('t1.balance_id = :id AND t1.balance_type = :type AND t1.wallet_type_id = :tagType', array(
                    ':id' => $id,
                    ':type' => $type,
                    ':tagType' => Yii::app()->params['tagID'],
                ))
                ->queryAll();

        return $query;
    }

    public static function verifyBalance($id, $type, $wallet, $amount) {
        $balance = self::model()->findByAttributes(array(
            'balance_id' => $id,
            'balance_type' => $type,
            'wallet_type_id' => $wallet
        ));

        $totalBalance = floatval($balance->balance_amount - $balance->balance_promised);

        if ($totalBalance < $amount) {
            return false;
        }

        return true;
    }

}
