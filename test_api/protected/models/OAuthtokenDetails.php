<?php

/**
 * This is the model class for table "oauthtoken_details".
 *
 * The followings are the available columns in table 'oauthtoken_details':
 * @property integer $id
 * @property string $oauth_token
 * @property integer $oauth_perspective_id
 *
 * The followings are the available model relations:
 * @property Communities $oauthPerspective
 * @property OauthSessionAccessTokens $oauthToken
 */
class OAuthtokenDetails extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OAuthtokenDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oauthtoken_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('oauth_token', 'required'),
			array('oauth_perspective_id', 'numerical', 'integerOnly'=>true),
			array('oauth_token', 'length', 'max'=>40),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, oauth_token, oauth_perspective_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oauthPerspective' => array(self::BELONGS_TO, 'Communities', 'oauth_perspective_id'),
			'oauthToken' => array(self::BELONGS_TO, 'OauthSessionAccessTokens', 'oauth_token'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'oauth_token' => 'Oauth Token',
			'oauth_perspective_id' => 'Oauth Perspective',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('oauth_token',$this->oauth_token,true);
		$criteria->compare('oauth_perspective_id',$this->oauth_perspective_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function listToken($type, $id){
		$query = Yii::app()->db->createCommand()
				->select('*')
				->from('oauth_sessions os')
				->join('oauth_session_access_tokens osat', 'osat.session_id = os.id')
				->join('oauthtoken_details od', 'od.oauth_token = osat.access_token')
				->where("osat.access_token_expires > UNIX_TIMESTAMP()");
		if($type == 2){
		}
		else{
			$query->andWhere("
					os.owner_type = 'user' AND os.owner_id = :userId", array(
						':userId' => $id,
						));
		}

		$result = $query->queryAll();

		$parsedResult = array();
		foreach($result as $row){
			$parsedResult[] = array(
				'id'=>$row['id'],
				'client_id'=>$row['client_id'],
				'access_token'=>$row['access_token'],
				'expires'=>$row['access_token_expires']
				);
		}

		return $parsedResult;
	}
}