<?php
class WalletCharging extends CDbTestCase
{
	public $fixtures=array(
		//'transfers'=>'WalletTransfers',
		//'balances'=>'WalletBalances',
	);

	public function testParams(){
		echo "\n User".json_encode(Yii::app()->params['user']);
	}

	public function testCharging(){
		echo "\n\nCase: Charging";
		$transfer = new WalletTransfers;
		$transfer->transfer_narration = 'Amount: 2, To user: 14';
		$transfer->transfer_pin = Yii::app()->params['user']['pin'];
		$transfer->transfer_from_amount = 3;
		$transfer->transfer_from_type = 1;
		$transfer->transfer_from_id = Yii::app()->params['user']['id'];
		$transfer->transfer_to_type = 2;
		$transfer->transfer_to_id = Yii::app()->params['user']['community']['id'];
		$transfer->setWallets(Yii::app()->params['user']['wallet']['id'], Yii::app()->params['user']['community']['wallet']['id']);
		$transfer->scenario = "charging";

		if(!$transfer->transfer()){
			echo "\n";
			$errors = $transfer->getErrors();
			echo "Errors: ".json_encode($errors);
			$this->assertFalse(true);
			return;
		}
		echo "\n - Success";
		$this->assertTrue(true);
	}

}