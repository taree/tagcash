<?php
class WalletTest extends CDbTestCase
{
	public $fixtures=array(
		//'transfers'=>'WalletTransfers',
		//'balances'=>'WalletBalances',
	);

	public function testTransfer(){
		echo "\n\nCase: Transfer";
		$transfer = new WalletTransfers;
		$transfer->transfer_narration = 'Amount: 2, To user: 14';
		$transfer->transfer_from_amount = 2;
		$transfer->transfer_from_type = 1;
		$transfer->transfer_from_id = Yii::app()->params['user']['id'];
		$transfer->transfer_to_type = 1;
		$transfer->transfer_to_id = Yii::app()->params['user2']['id'];
		$transfer->setWallets(Yii::app()->params['user']['wallet']['id'], Yii::app()->params['user2']['wallet']['id']);
		if(!$transfer->transfer()){
			echo "\nErrors: ".json_encode($transfer->seeErrors());
			$user = Users::model()->findbyPk(Yii::app()->params['user']['id']);
			echo "\nUser: ".$user->user_firstname;
			$balance = WalletBalances::model()->findByAttributes(array(
					"balance_id"=>Yii::app()->params['user']['id'],
					"balance_type"=>1,
					"wallet_type_id"=>2
				));
			echo "\nBalance: ".$balance->balance_amount;

			$this->assertFalse(true);
			return;
		}
		echo "\n - Success";
		$this->assertTrue(true);
	}

}