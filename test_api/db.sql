-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 10, 2017 at 02:55 PM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `contact_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `country_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `country_longname` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `country_callingcode` varchar(8) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `timezone_id` int(11) DEFAULT NULL,
  `currency_code` char(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_block_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 - nothing blocked, 1 - wallet blocked',
  `wallet_id` int(5) DEFAULT NULL COMMENT 'links to the wallet id that will automatically be created for a user if they set their country to this',
  PRIMARY KEY (`id`),
  KEY `timezone_id` (`timezone_id`),
  KEY `currency_code` (`currency_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_code`, `country_name`, `country_longname`, `country_callingcode`, `timezone_id`, `currency_code`, `country_block_status`, `wallet_id`) VALUES
(1, 'AF', 'Afghanistan', 'Islamic Republic of Afghanistan', '93', 88, NULL, 0, NULL),
(2, 'AX', 'Aland Islands', '&Aring;land Islands', '358', 65, NULL, 0, NULL),
(3, 'AL', 'Albania', 'Republic of Albania', '355', 58, NULL, 0, NULL),
(4, 'DZ', 'Algeria', 'People''s Democratic Republic of Algeria', '213', 59, NULL, 0, NULL),
(5, 'AS', 'American Samoa', 'American Samoa', '1+684', 3, NULL, 0, NULL),
(6, 'AD', 'Andorra', 'Principality of Andorra', '376', 50, NULL, 0, NULL),
(7, 'AO', 'Angola', 'Republic of Angola', '244', 59, NULL, 0, NULL),
(8, 'AI', 'Anguilla', 'Anguilla', '1+264', 24, NULL, 0, NULL),
(9, 'AQ', 'Antarctica', 'Antarctica', '672', 137, NULL, 0, NULL),
(10, 'AG', 'Antigua and Barbuda', 'Antigua and Barbuda', '1+268', 25, NULL, 0, NULL),
(11, 'AR', 'Argentina', 'Argentine Republic', '54', 29, NULL, 0, NULL),
(12, 'AM', 'Armenia', 'Republic of Armenia', '374', 87, NULL, 0, NULL),
(13, 'AW', 'Aruba', 'Aruba', '297', 26, NULL, 0, NULL),
(14, 'AU', 'Australia', 'Commonwealth of Australia', '61', 131, NULL, 0, NULL),
(15, 'AT', 'Austria', 'Republic of Austria', '43', 57, NULL, 0, NULL),
(16, 'AZ', 'Azerbaijan', 'Republic of Azerbaijan', '994', 84, NULL, 0, NULL),
(17, 'BS', 'Bahamas', 'Commonwealth of The Bahamas', '1+242', 19, NULL, 0, NULL),
(18, 'BH', 'Bahrain', 'Kingdom of Bahrain', '973', 76, NULL, 0, NULL),
(19, 'BD', 'Bangladesh', 'People''s Republic of Bangladesh', '880', 100, NULL, 0, NULL),
(20, 'BB', 'Barbados', 'Barbados', '1+246', 26, NULL, 0, NULL),
(21, 'BY', 'Belarus', 'Republic of Belarus', '375', 81, NULL, 0, NULL),
(22, 'BE', 'Belgium', 'Kingdom of Belgium', '32', 46, NULL, 0, NULL),
(23, 'BZ', 'Belize', 'Belize', '501', 12, NULL, 0, NULL),
(24, 'BJ', 'Benin', 'Republic of Benin', '229', 59, NULL, 0, NULL),
(25, 'BM', 'Bermuda', 'Bermuda Islands', '1+441', 23, NULL, 0, NULL),
(26, 'BT', 'Bhutan', 'Kingdom of Bhutan', '975', 102, NULL, 0, NULL),
(27, 'BO', 'Bolivia', 'Plurinational State of Bolivia', '591', 25, NULL, 0, NULL),
(28, 'BQ', 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', '599', 23, NULL, 0, NULL),
(29, 'BA', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', '387', 54, NULL, 0, NULL),
(30, 'BW', 'Botswana', 'Republic of Botswana', '267', 70, NULL, 0, NULL),
(31, 'BV', 'Bouvet Island', 'Bouvet Island', 'NONE', 37, NULL, 0, NULL),
(32, 'BR', 'Brazil', 'Federative Republic of Brazil', '55', 28, NULL, 0, NULL),
(33, 'IO', 'British Indian Ocean Territory', 'British Indian Ocean Territory', '246', 99, NULL, 0, NULL),
(34, 'BN', 'Brunei', 'Brunei Darussalam', '673', 112, NULL, 0, NULL),
(35, 'BG', 'Bulgaria', 'Republic of Bulgaria', '359', 72, NULL, 0, NULL),
(36, 'BF', 'Burkina Faso', 'Burkina Faso', '226', 35, NULL, 0, NULL),
(37, 'BI', 'Burundi', 'Republic of Burundi', '257', 64, NULL, 0, NULL),
(38, 'KH', 'Cambodia', 'Kingdom of Cambodia', '855', 104, NULL, 0, NULL),
(39, 'CM', 'Cameroon', 'Republic of Cameroon', '237', 59, NULL, 0, NULL),
(40, 'CA', 'Canada', 'Canada', '1', 19, NULL, 0, NULL),
(41, 'CV', 'Cape Verde', 'Republic of Cape Verde', '238', 34, NULL, 0, NULL),
(42, 'KY', 'Cayman Islands', 'The Cayman Islands', '1+345', 20, NULL, 0, NULL),
(43, 'CF', 'Central African Republic', 'Central African Republic', '236', 59, NULL, 0, NULL),
(44, 'TD', 'Chad', 'Republic of Chad', '235', 59, NULL, 0, NULL),
(45, 'CL', 'Chile', 'Republic of Chile', '56', 26, NULL, 0, NULL),
(46, 'CN', 'China', 'People''s Republic of China', '86', 108, NULL, 0, NULL),
(47, 'CX', 'Christmas Island', 'Christmas Island', '61', 106, NULL, 0, NULL),
(48, 'CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', '61', 103, NULL, 0, NULL),
(49, 'CO', 'Colombia', 'Republic of Colombia', '57', 18, NULL, 0, NULL),
(50, 'KM', 'Comoros', 'Union of the Comoros', '269', 78, NULL, 0, NULL),
(51, 'CG', 'Congo', 'Republic of the Congo', '242', 59, NULL, 0, NULL),
(52, 'CK', 'Cook Islands', 'Cook Islands', '682', 4, NULL, 0, NULL),
(53, 'CR', 'Costa Rica', 'Republic of Costa Rica', '506', 12, NULL, 0, NULL),
(54, 'CI', 'Cote d''ivoire (Ivory Coast)', 'Republic of C&ocirc;te D''Ivoire (Ivory Coast)', '225', 35, NULL, 0, NULL),
(55, 'HR', 'Croatia', 'Republic of Croatia', '385', 60, NULL, 0, NULL),
(56, 'CU', 'Cuba', 'Republic of Cuba', '53', 21, NULL, 0, NULL),
(57, 'CW', 'Curacao', 'Cura&ccedil;ao', '599', 25, NULL, 0, NULL),
(58, 'CY', 'Cyprus', 'Republic of Cyprus', '357', 67, NULL, 0, NULL),
(59, 'CZ', 'Czech Republic', 'Czech Republic', '420', 52, NULL, 0, NULL),
(60, 'CD', 'Democratic Republic of the Congo', 'Democratic Republic of the Congo', '243', 59, NULL, 0, NULL),
(61, 'DK', 'Denmark', 'Kingdom of Denmark', '45', 48, NULL, 0, NULL),
(62, 'DJ', 'Djibouti', 'Republic of Djibouti', '253', 78, NULL, 0, NULL),
(63, 'DM', 'Dominica', 'Commonwealth of Dominica', '1+767', 23, NULL, 0, NULL),
(64, 'DO', 'Dominican Republic', 'Dominican Republic', '1+809, 8', 23, NULL, 0, NULL),
(65, 'EC', 'Ecuador', 'Republic of Ecuador', '593', 22, NULL, 0, NULL),
(66, 'EG', 'Egypt', 'Arab Republic of Egypt', '20', 63, NULL, 0, NULL),
(67, 'SV', 'El Salvador', 'Republic of El Salvador', '503', 12, NULL, 0, NULL),
(68, 'GQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', '240', 59, NULL, 0, NULL),
(69, 'ER', 'Eritrea', 'State of Eritrea', '291', 78, NULL, 0, NULL),
(70, 'EE', 'Estonia', 'Republic of Estonia', '372', 65, NULL, 0, NULL),
(71, 'ET', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', '251', 78, NULL, 0, NULL),
(72, 'FK', 'Falkland Islands (Malvinas)', 'The Falkland Islands (Malvinas)', '500', 23, NULL, 0, NULL),
(73, 'FO', 'Faroe Islands', 'The Faroe Islands', '298', 37, NULL, 0, NULL),
(74, 'FJ', 'Fiji', 'Republic of Fiji', '679', 137, NULL, 0, NULL),
(75, 'FI', 'Finland', 'Republic of Finland', '358', 65, NULL, 0, NULL),
(76, 'FR', 'France', 'French Republic', '33', 51, NULL, 0, NULL),
(77, 'GF', 'French Guiana', 'French Guiana', '594', 31, NULL, 0, NULL),
(78, 'PF', 'French Polynesia', 'French Polynesia', '689', 5, NULL, 0, NULL),
(79, 'TF', 'French Southern Territories', 'French Southern Territories', NULL, 90, NULL, 0, NULL),
(80, 'GA', 'Gabon', 'Gabonese Republic', '241', 59, NULL, 0, NULL),
(81, 'GM', 'Gambia', 'Republic of The Gambia', '220', 40, NULL, 0, NULL),
(82, 'GE', 'Georgia', 'Georgia', '995', 86, NULL, 0, NULL),
(83, 'DE', 'Germany', 'Federal Republic of Germany', '49', 43, NULL, 0, NULL),
(84, 'GH', 'Ghana', 'Republic of Ghana', '233', 37, NULL, 0, NULL),
(85, 'GI', 'Gibraltar', 'Gibraltar', '350', 50, NULL, 0, NULL),
(86, 'GR', 'Greece', 'Hellenic Republic', '30', 61, NULL, 0, NULL),
(87, 'GL', 'Greenland', 'Greenland', '299', 31, NULL, 0, NULL),
(88, 'GD', 'Grenada', 'Grenada', '1+473', 23, NULL, 0, NULL),
(89, 'GP', 'Guadaloupe', 'Guadeloupe', '590', 23, NULL, 0, NULL),
(90, 'GU', 'Guam', 'Guam', '1+671', 127, NULL, 0, NULL),
(91, 'GT', 'Guatemala', 'Republic of Guatemala', '502', 12, NULL, 0, NULL),
(92, 'GG', 'Guernsey', 'Guernsey', '44', 37, NULL, 0, NULL),
(93, 'GN', 'Guinea', 'Republic of Guinea', '224', 37, NULL, 0, NULL),
(94, 'GW', 'Guinea-Bissau', 'Republic of Guinea-Bissau', '245', 37, NULL, 0, NULL),
(95, 'GY', 'Guyana', 'Co-operative Republic of Guyana', '592', 25, NULL, 0, NULL),
(96, 'HT', 'Haiti', 'Republic of Haiti', '509', 19, NULL, 0, NULL),
(97, 'HM', 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'NONE', 91, NULL, 0, NULL),
(98, 'HN', 'Honduras', 'Republic of Honduras', '504', 15, NULL, 0, NULL),
(99, 'HK', 'Hong Kong', 'Hong Kong', '852', 110, NULL, 0, NULL),
(100, 'HU', 'Hungary', 'Hungary', '36', 47, NULL, 0, NULL),
(101, 'IS', 'Iceland', 'Republic of Iceland', '354', 37, NULL, 0, NULL),
(102, 'IN', 'India', 'Republic of India', '91', 96, 'INR', 0, NULL),
(103, 'ID', 'Indonesia', 'Republic of Indonesia', '62', 106, NULL, 0, NULL),
(104, 'IR', 'Iran', 'Islamic Republic of Iran', '98', 82, NULL, 0, NULL),
(105, 'IQ', 'Iraq', 'Republic of Iraq', '964', 75, NULL, 0, NULL),
(106, 'IE', 'Ireland', 'Ireland', '353', 37, NULL, 0, NULL),
(107, 'IM', 'Isle of Man', 'Isle of Man', '44', 37, NULL, 0, NULL),
(108, 'IL', 'Israel', 'State of Israel', '972', 67, NULL, 0, NULL),
(109, 'IT', 'Italy', 'Italian Republic', '39', 53, NULL, 0, NULL),
(110, 'JM', 'Jamaica', 'Jamaica', '1+876', 19, NULL, 0, NULL),
(111, 'JP', 'Japan', 'Japan', '81', 121, NULL, 0, NULL),
(112, 'JE', 'Jersey', 'The Bailiwick of Jersey', '44', 37, NULL, 0, NULL),
(113, 'JO', 'Jordan', 'Hashemite Kingdom of Jordan', '962', 67, NULL, 0, NULL),
(114, 'KZ', 'Kazakhstan', 'Republic of Kazakhstan', '7', 99, NULL, 0, NULL),
(115, 'KE', 'Kenya', 'Republic of Kenya', '254', 78, NULL, 0, NULL),
(116, 'KI', 'Kiribati', 'Republic of Kiribati', '686', 141, NULL, 0, NULL),
(117, 'XK', 'Kosovo', 'Republic of Kosovo', '381', 60, NULL, 0, NULL),
(118, 'KW', 'Kuwait', 'State of Kuwait', '965', 76, NULL, 0, NULL),
(119, 'KG', 'Kyrgyzstan', 'Kyrgyz Republic', '996', 101, NULL, 0, NULL),
(120, 'LA', 'Laos', 'Lao People''s Democratic Republic', '856', 104, NULL, 0, NULL),
(121, 'LV', 'Latvia', 'Republic of Latvia', '371', 71, NULL, 0, NULL),
(122, 'LB', 'Lebanon', 'Republic of Lebanon', '961', 67, NULL, 0, NULL),
(123, 'LS', 'Lesotho', 'Kingdom of Lesotho', '266', 63, NULL, 0, NULL),
(124, 'LR', 'Liberia', 'Republic of Liberia', '231', 40, NULL, 0, NULL),
(125, 'LY', 'Libya', 'Libya', '218', 59, NULL, 0, NULL),
(126, 'LI', 'Liechtenstein', 'Principality of Liechtenstein', '423', 51, NULL, 0, NULL),
(127, 'LT', 'Lithuania', 'Republic of Lithuania', '370', 62, NULL, 0, NULL),
(128, 'LU', 'Luxembourg', 'Grand Duchy of Luxembourg', '352', 43, NULL, 0, NULL),
(129, 'MO', 'Macao', 'The Macao Special Administrative Region', '853', 110, NULL, 0, NULL),
(130, 'MK', 'Macedonia', 'The Former Yugoslav Republic of Macedonia', '389', 55, NULL, 0, NULL),
(131, 'MG', 'Madagascar', 'Republic of Madagascar', '261', 78, NULL, 0, NULL),
(132, 'MW', 'Malawi', 'Republic of Malawi', '265', 63, NULL, 0, NULL),
(133, 'MY', 'Malaysia', 'Malaysia', '60', 112, NULL, 0, NULL),
(134, 'MV', 'Maldives', 'Republic of Maldives', '960', 91, NULL, 0, NULL),
(135, 'ML', 'Mali', 'Republic of Mali', '223', 35, NULL, 0, NULL),
(136, 'MT', 'Malta', 'Republic of Malta', '356', 53, NULL, 0, NULL),
(137, 'MH', 'Marshall Islands', 'Republic of the Marshall Islands', '692', 139, NULL, 0, NULL),
(138, 'MQ', 'Martinique', 'Martinique', '596', 23, NULL, 0, NULL),
(139, 'MR', 'Mauritania', 'Islamic Republic of Mauritania', '222', 40, NULL, 0, NULL),
(140, 'MU', 'Mauritius', 'Republic of Mauritius', '230', 83, NULL, 0, NULL),
(141, 'YT', 'Mayotte', 'Mayotte', '262', 78, NULL, 0, NULL),
(142, 'MX', 'Mexico', 'United Mexican States', '52', 15, NULL, 0, NULL),
(143, 'FM', 'Micronesia', 'Federated States of Micronesia', '691', 133, NULL, 0, NULL),
(144, 'MD', 'Moldova', 'Republic of Moldova', '373', 68, NULL, 0, NULL),
(145, 'MC', 'Monaco', 'Principality of Monaco', '377', 51, NULL, 0, NULL),
(146, 'MN', 'Mongolia', 'Mongolia', '976', 116, NULL, 0, NULL),
(147, 'ME', 'Montenegro', 'Montenegro', '382', 45, NULL, 0, NULL),
(148, 'MS', 'Montserrat', 'Montserrat', '1+664', 23, NULL, 0, NULL),
(149, 'MA', 'Morocco', 'Kingdom of Morocco', '212', 35, NULL, 0, NULL),
(150, 'MZ', 'Mozambique', 'Republic of Mozambique', '258', 70, NULL, 0, NULL),
(151, 'MM', 'Myanmar (Burma)', 'Republic of the Union of Myanmar', '95', 103, NULL, 0, NULL),
(152, 'NA', 'Namibia', 'Republic of Namibia', '264', 59, NULL, 0, NULL),
(153, 'NR', 'Nauru', 'Republic of Nauru', '674', 136, NULL, 0, NULL),
(154, 'NP', 'Nepal', 'Federal Democratic Republic of Nepal', '977', 97, NULL, 0, NULL),
(155, 'NL', 'Netherlands', 'Kingdom of the Netherlands', '31', 41, NULL, 0, NULL),
(156, 'NC', 'New Caledonia', 'New Caledonia', '687', 134, NULL, 0, NULL),
(157, 'NZ', 'New Zealand', 'New Zealand', '64', 136, NULL, 0, NULL),
(158, 'NI', 'Nicaragua', 'Republic of Nicaragua', '505', 12, NULL, 0, NULL),
(159, 'NE', 'Niger', 'Republic of Niger', '227', 59, NULL, 0, NULL),
(160, 'NG', 'Nigeria', 'Federal Republic of Nigeria', '234', 59, NULL, 0, NULL),
(161, 'NU', 'Niue', 'Niue', '683', 3, NULL, 0, NULL),
(162, 'NF', 'Norfolk Island', 'Norfolk Island', '672', 19, NULL, 0, NULL),
(163, 'KP', 'North Korea', 'Democratic People''s Republic of Korea', '850', 120, NULL, 0, NULL),
(164, 'MP', 'Northern Mariana Islands', 'Northern Mariana Islands', '1+670', 127, NULL, 0, NULL),
(165, 'NO', 'Norway', 'Kingdom of Norway', '47', 65, NULL, 0, NULL),
(166, 'OM', 'Oman', 'Sultanate of Oman', '968', 85, NULL, 0, NULL),
(167, 'PK', 'Pakistan', 'Islamic Republic of Pakistan', '92', 90, NULL, 0, NULL),
(168, 'PW', 'Palau', 'Republic of Palau', '680', 121, NULL, 0, NULL),
(169, 'PS', 'Palestine', 'State of Palestine (or Occupied Palestinian Territory)', '970', 67, NULL, 0, NULL),
(170, 'PA', 'Panama', 'Republic of Panama', '507', 22, NULL, 0, NULL),
(171, 'PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', '675', 130, NULL, 0, NULL),
(172, 'PY', 'Paraguay', 'Republic of Paraguay', '595', 25, NULL, 0, NULL),
(173, 'PE', 'Peru', 'Republic of Peru', '51', 21, NULL, 0, NULL),
(174, 'PH', 'Philippines', 'Republic of the Phillipines', '63', 115, 'PHP', 0, 1),
(175, 'PN', 'Pitcairn', 'Pitcairn', 'NONE', 6, NULL, 0, NULL),
(176, 'PL', 'Poland', 'Republic of Poland', '48', 51, NULL, 0, NULL),
(177, 'PT', 'Portugal', 'Portuguese Republic', '351', 38, NULL, 0, NULL),
(178, 'PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', '1+939', 23, NULL, 0, NULL),
(179, 'QA', 'Qatar', 'State of Qatar', '974', 76, NULL, 0, NULL),
(180, 'RE', 'Reunion', 'R&eacute;union', '262', 85, NULL, 0, NULL),
(181, 'RO', 'Romania', 'Romania', '40', 62, NULL, 0, NULL),
(182, 'RU', 'Russia', 'Russian Federation', '7', 77, NULL, 0, NULL),
(183, 'RW', 'Rwanda', 'Republic of Rwanda', '250', 63, NULL, 0, NULL),
(184, 'BL', 'Saint Barthelemy', 'Saint Barth&eacute;lemy', '590', 26, NULL, 0, NULL),
(185, 'SH', 'Saint Helena', 'Saint Helena, Ascension and Tristan da Cunha', '290', 37, NULL, 0, NULL),
(186, 'KN', 'Saint Kitts and Nevis', 'Federation of Saint Christopher and Nevis', '1+869', 26, NULL, 0, NULL),
(187, 'LC', 'Saint Lucia', 'Saint Lucia', '1+758', 26, NULL, 0, NULL),
(188, 'MF', 'Saint Martin', 'Saint Martin', '590', 26, NULL, 0, NULL),
(189, 'PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', '508', 31, NULL, 0, NULL),
(190, 'VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', '1+784', 23, NULL, 0, NULL),
(191, 'WS', 'Samoa', 'Independent State of Samoa', '685', 3, NULL, 0, NULL),
(192, 'SM', 'San Marino', 'Republic of San Marino', '378', 53, NULL, 0, NULL),
(193, 'ST', 'Sao Tome and Principe', 'Democratic Republic of S&atilde;o Tom&eacute; and Pr&iacute;ncipe', '239', 38, NULL, 0, NULL),
(194, 'SA', 'Saudi Arabia', 'Kingdom of Saudi Arabia', '966', 79, NULL, 0, NULL),
(195, 'SN', 'Senegal', 'Republic of Senegal', '221', 38, NULL, 0, NULL),
(196, 'RS', 'Serbia', 'Republic of Serbia', '381', 45, NULL, 0, NULL),
(197, 'SC', 'Seychelles', 'Republic of Seychelles', '248', 86, NULL, 0, NULL),
(198, 'SL', 'Sierra Leone', 'Republic of Sierra Leone', '232', 37, NULL, 0, NULL),
(199, 'SG', 'Singapore', 'Republic of Singapore', '65', 112, NULL, 0, NULL),
(200, 'SX', 'Sint Maarten', 'Sint Maarten', '1+721', 23, NULL, 0, NULL),
(201, 'SK', 'Slovakia', 'Slovak Republic', '421', 45, NULL, 0, NULL),
(202, 'SI', 'Slovenia', 'Republic of Slovenia', '386', 49, NULL, 0, NULL),
(203, 'SB', 'Solomon Islands', 'Solomon Islands', '677', 135, NULL, 0, NULL),
(204, 'SO', 'Somalia', 'Somali Republic', '252', 78, NULL, 0, NULL),
(205, 'ZA', 'South Africa', 'Republic of South Africa', '27', 64, NULL, 0, NULL),
(206, 'GS', 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands', '500', 32, NULL, 0, NULL),
(207, 'KR', 'South Korea', 'Republic of Korea', '82', 120, NULL, 0, NULL),
(208, 'SS', 'South Sudan', 'Republic of South Sudan', '211', 78, NULL, 0, NULL),
(209, 'ES', 'Spain', 'Kingdom of Spain', '34', 50, NULL, 0, NULL),
(210, 'LK', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', '94', 93, NULL, 0, NULL),
(211, 'SD', 'Sudan', 'Republic of the Sudan', '249', 32, NULL, 0, NULL),
(212, 'SR', 'Suriname', 'Republic of Suriname', '597', 28, NULL, 0, NULL),
(213, 'SJ', 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', '47', 28, NULL, 0, NULL),
(214, 'SZ', 'Swaziland', 'Kingdom of Swaziland', '268', 65, NULL, 0, NULL),
(215, 'SE', 'Sweden', 'Kingdom of Sweden', '46', 56, NULL, 0, NULL),
(216, 'CH', 'Switzerland', 'Swiss Confederation', '41', 44, NULL, 0, NULL),
(217, 'SY', 'Syria', 'Syrian Arab Republic', '963', 67, NULL, 0, NULL),
(218, 'TW', 'Taiwan', 'Republic of China (Taiwan)', '886', 115, NULL, 0, NULL),
(219, 'TJ', 'Tajikistan', 'Republic of Tajikistan', '992', 92, NULL, 0, NULL),
(220, 'TZ', 'Tanzania', 'United Republic of Tanzania', '255', 78, NULL, 0, NULL),
(221, 'TH', 'Thailand', 'Kingdom of Thailand', '66', 104, 'THB', 0, 1),
(222, 'TL', 'Timor-Leste (East Timor)', 'Democratic Republic of Timor-Leste', '670', 122, NULL, 0, NULL),
(223, 'TG', 'Togo', 'Togolese Republic', '228', 37, NULL, 0, NULL),
(224, 'TK', 'Tokelau', 'Tokelau', '690', 141, NULL, 0, NULL),
(225, 'TO', 'Tonga', 'Kingdom of Tonga', '676', 141, NULL, 0, NULL),
(226, 'TT', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', '1+868', 26, NULL, 0, NULL),
(227, 'TN', 'Tunisia', 'Republic of Tunisia', '216', 59, NULL, 0, NULL),
(228, 'TR', 'Turkey', 'Republic of Turkey', '90', 19, NULL, 0, NULL),
(229, 'TM', 'Turkmenistan', 'Turkmenistan', '993', 90, NULL, 0, NULL),
(230, 'TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', '1+649', 90, NULL, 0, NULL),
(231, 'TV', 'Tuvalu', 'Tuvalu', '688', 137, NULL, 0, NULL),
(232, 'UG', 'Uganda', 'Republic of Uganda', '256', 78, NULL, 0, NULL),
(233, 'UA', 'Ukraine', 'Ukraine', '380', 68, NULL, 0, NULL),
(234, 'AE', 'United Arab Emirates', 'United Arab Emirates', '971', 83, NULL, 0, NULL),
(235, 'GB', 'United Kingdom', 'United Kingdom of Great Britain and Nothern Ireland', '44', 39, NULL, 0, NULL),
(236, 'US', 'United States', 'United States of America', '1', 19, NULL, 0, NULL),
(237, 'UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'NONE', 139, NULL, 0, NULL),
(238, 'UY', 'Uruguay', 'Eastern Republic of Uruguay', '598', 29, NULL, 0, NULL),
(239, 'UZ', 'Uzbekistan', 'Republic of Uzbekistan', '998', 92, NULL, 0, NULL),
(240, 'VU', 'Vanuatu', 'Republic of Vanuatu', '678', 135, NULL, 0, NULL),
(241, 'VA', 'Vatican City', 'State of the Vatican City', '39', 53, NULL, 0, NULL),
(242, 'VE', 'Venezuela', 'Bolivarian Republic of Venezuela', '58', 88, NULL, 0, NULL),
(243, 'VN', 'Vietnam', 'Socialist Republic of Vietnam', '84', 105, NULL, 0, NULL),
(244, 'VG', 'Virgin Islands, British', 'British Virgin Islands', '1+284', 23, NULL, 0, NULL),
(245, 'VI', 'Virgin Islands, US', 'Virgin Islands of the United States', '1+340', 23, NULL, 0, NULL),
(246, 'WF', 'Wallis and Futuna', 'Wallis and Futuna', '681', 137, NULL, 0, NULL),
(247, 'EH', 'Western Sahara', 'Western Sahara', '212', 40, NULL, 0, NULL),
(248, 'YE', 'Yemen', 'Republic of Yemen', '967', 79, NULL, 0, NULL),
(249, 'ZM', 'Zambia', 'Republic of Zambia', '260', 64, NULL, 0, NULL),
(250, 'ZW', 'Zimbabwe', 'Republic of Zimbabwe', '263', 64, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `module_list`
--

CREATE TABLE IF NOT EXISTS `module_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `module_list`
--

INSERT INTO `module_list` (`id`, `name`, `active`) VALUES
(1, 'Cash In', 'Y'),
(2, 'Cash Out', 'Y'),
(3, 'Transfer', 'Y'),
(4, 'Buy/Sell Load', 'Y'),
(5, 'Pay Bills', 'Y'),
(6, 'Request/QR', 'Y'),
(7, 'Vouchers', 'Y'),
(8, 'Agent', 'Y'),
(9, 'Home Services', 'Y'),
(10, 'Buy Gifts', 'Y'),
(11, 'Points Creation', 'Y'),
(12, 'Codes', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `module_list_via_country_or_wallet`
--

CREATE TABLE IF NOT EXISTS `module_list_via_country_or_wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `wallet_id` int(11) DEFAULT NULL,
  `module_id` int(11) NOT NULL,
  `type` varchar(1) NOT NULL COMMENT '1=user module 2= community modules',
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  KEY `country_id` (`country_id`),
  KEY `module_id_2` (`module_id`),
  KEY `wallet_id` (`wallet_id`),
  KEY `wallet_id_2` (`wallet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `module_list_via_country_or_wallet`
--

INSERT INTO `module_list_via_country_or_wallet` (`id`, `country_id`, `wallet_id`, `module_id`, `type`) VALUES
(1, 164, NULL, 1, '1'),
(2, 164, 1, 2, '1'),
(3, 164, 2, 1, '2'),
(4, 164, 2, 2, '1'),
(5, 165, 1, 1, '1'),
(6, 165, 1, 2, '1'),
(7, 165, 2, 1, '1'),
(8, 165, 2, 2, '1');

-- --------------------------------------------------------

--
-- Table structure for table `oauthclient_details`
--

CREATE TABLE IF NOT EXISTS `oauthclient_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `community_id` int(11) DEFAULT NULL COMMENT 'If NULL then system app',
  `client_id` char(40) CHARACTER SET utf8 NOT NULL,
  `oauth_granttypes` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Comma separated grant types ',
  `login_type` tinyint(1) DEFAULT NULL COMMENT '0 = system, 1 = user, 2 = community, 3 = admin, 4 = reseller',
  `login_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `community_id` (`community_id`,`client_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Table for storing the mapping between communities and clients/apps' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `oauthclient_details`
--

INSERT INTO `oauthclient_details` (`id`, `community_id`, `client_id`, `oauth_granttypes`, `login_type`, `login_id`) VALUES
(1, NULL, '02b501563d5df564', 'authorization_code,implicit,client_credentials,password', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauthtoken_details`
--

CREATE TABLE IF NOT EXISTS `oauthtoken_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oauth_token` char(40) NOT NULL,
  `oauth_perspective_id` int(11) DEFAULT NULL,
  `login_from` varchar(20) DEFAULT NULL,
  `device_id` varchar(30) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `oauth_token` (`oauth_token`),
  KEY `oauth_perspective_id` (`oauth_perspective_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Additional details about the tokens like active perspective ' AUTO_INCREMENT=15 ;

--
-- Dumping data for table `oauthtoken_details`
--

INSERT INTO `oauthtoken_details` (`id`, `oauth_token`, `oauth_perspective_id`, `login_from`, `device_id`, `time`) VALUES
(1, 'k6hDSUu6Mx8jUCvj0PzCYNlKIMT8sPjQrzAcvfm8', NULL, NULL, NULL, '2017-05-10 08:48:12'),
(2, 'cXvEEjKmPk48aMgNYlqI3pzPwzXqGCbc8Lco7VRF', NULL, NULL, NULL, '2017-05-10 08:49:08'),
(3, 'AchkUKQg42R2isibsJXhiRePKSHijreczwRwfC2K', NULL, NULL, NULL, '2017-05-10 08:51:08'),
(4, 'ZUcsIjIiKcr33Hbb72HRYQC1IwuXdmPZR3uXcEUm', NULL, NULL, NULL, '2017-05-10 08:53:06'),
(5, 'TePg7IIZ4JtizsEo8eFKh7wh2QhRkvc6wa6cBQe9', NULL, NULL, NULL, '2017-05-10 08:53:58'),
(6, 'He9cIIKsmKoZ1NXXd82FH1n3GMIi6SfRbgwokdH3', NULL, NULL, NULL, '2017-05-10 08:54:46'),
(7, '2H3ISevciuLI0vnQ9myo8E82wCVgKVtQsRPxarvF', NULL, NULL, NULL, '2017-05-10 08:55:18'),
(8, 'TLClBciEMEu6B79AAlOISRceA7AReR8Def0Tb4G1', NULL, NULL, NULL, '2017-05-10 08:56:05'),
(9, 'JgkR3elIOHSYvfMnZWJtpDxMsw7V3mJtjisVQAVs', NULL, NULL, NULL, '2017-05-10 09:16:34'),
(10, 'JVqcb8PQaMRYMHChAyWsEEJXWkb09rJmVtHCrJgN', NULL, NULL, NULL, '2017-05-10 09:17:36'),
(11, 'mayilBrYr0Nfq0jD7ciW8fE8AJM7ZvQohKmNk32s', NULL, NULL, NULL, '2017-05-10 09:18:19'),
(12, 'UEmuzoWCkxh3TSHMkwrb3ltLcygL1xSRGs6OKduO', NULL, NULL, NULL, '2017-05-10 09:19:04'),
(13, '6GMfJS76Uz1hpOF7kElgtVjdzQOtnW4tGPqwv0IB', NULL, NULL, NULL, '2017-05-10 09:21:18'),
(14, 'sYBuH2VLBx898G0C1e5U3NP3w1hfiucru6IJfBEa', NULL, NULL, NULL, '2017-05-10 09:23:30');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` char(40) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `auto_approve` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_oacl_clse_clid` (`secret`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `secret`, `name`, `auto_approve`) VALUES
('02b501563d5df564', 'babc91513d470eaf7bfafa4768fc4fdb', 'woo', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_client_endpoints`
--

CREATE TABLE IF NOT EXISTS `oauth_client_endpoints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` char(40) NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i_oaclen_clid` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `oauth_client_endpoints`
--

INSERT INTO `oauth_client_endpoints` (`id`, `client_id`, `redirect_uri`) VALUES
(86, '02b501563d5df564', 'http://tagcash.my');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_scopes` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `scope` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_oasc_sc` (`scope`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `oauth_scopes`
--

INSERT INTO `oauth_scopes` (`id`, `scope`, `name`, `description`) VALUES
(3, 'user.settings', 'Settings', 'Update your settings, avatar image etc'),
(4, 'user.contacts', 'Contacts', 'Access your contacts, accept contact requests, send invitations'),
(5, 'user.block', 'Block', 'Access your blocked list. Block users and communities'),
(6, 'user.communities', 'Communities', 'Join communities, view joined communities, create new communities'),
(7, 'user.rewards', 'Rewards', 'Access reward balance and transactions'),
(8, 'user.rfid', 'RFID Tags', 'Bind new RFID tags, view bound RFID tags, activate or deactivate existing RFID tags'),
(9, 'user.reviews', 'Reviews', 'Post reviews on behalf of you'),
(10, 'user.messages', 'Messages', 'View or send messages on your behalf'),
(11, 'user.deals', 'Deals', 'Access your deals, take deals, redeem deals'),
(12, 'user.profiles', 'Profiles', 'Access all your profiles and edit them'),
(13, 'community.settings', 'Settings', 'View and update your community''s settings, image etc'),
(14, 'community.rewards', 'Rewards', 'Access reward balance and transactions of all your communities'),
(15, 'community.rfid', 'RFID Tags', 'Bind new RFID tags, view bound RFID tags, activate or deactivate existing RFID tags'),
(16, 'community.messages', 'Messages', 'View or send messages on your community''s behalf'),
(17, 'community.members', 'Members', 'Add, edit and view members in your community'),
(18, 'community.roles', 'Roles', 'Create, edit and delete roles in all communities'),
(19, 'community.deals', 'Deals', 'Create, edit, assign, redeem deals'),
(20, 'community.tagbind', 'Tagbind', 'listAll, details'),
(21, 'user.wallets', 'Wallets', 'access user wallets'),
(22, 'community.wallets', 'Wallets', 'access community wallets'),
(23, 'user.services', 'Services', 'Services'),
(24, 'community.services', 'Services', 'Services');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_sessions`
--

CREATE TABLE IF NOT EXISTS `oauth_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` char(40) NOT NULL,
  `owner_type` enum('user','client') NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i_uase_clid_owty_owid` (`client_id`,`owner_type`,`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `oauth_sessions`
--

INSERT INTO `oauth_sessions` (`id`, `client_id`, `owner_type`, `owner_id`) VALUES
(1, '02b501563d5df564', 'user', '1'),
(2, '02b501563d5df564', 'user', '1'),
(3, '02b501563d5df564', 'user', '1'),
(4, '02b501563d5df564', 'user', '1'),
(5, '02b501563d5df564', 'user', '1'),
(6, '02b501563d5df564', 'user', '1'),
(7, '02b501563d5df564', 'user', '1'),
(8, '02b501563d5df564', 'user', '1'),
(9, '02b501563d5df564', 'user', '1'),
(10, '02b501563d5df564', 'user', '1'),
(11, '02b501563d5df564', 'user', '1'),
(12, '02b501563d5df564', 'user', '1'),
(13, '02b501563d5df564', 'user', '1'),
(14, '02b501563d5df564', 'user', '1'),
(15, '02b501563d5df564', 'user', '1');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_access_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_session_access_tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(10) unsigned NOT NULL,
  `access_token` char(40) NOT NULL,
  `access_token_expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_oaseacto_acto_seid` (`access_token`,`session_id`),
  KEY `f_oaseto_seid` (`session_id`),
  KEY `access_token` (`access_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `oauth_session_access_tokens`
--

INSERT INTO `oauth_session_access_tokens` (`id`, `session_id`, `access_token`, `access_token_expires`) VALUES
(1, 1, 'M9qxGjj7ZD4qneMlbMocaCtStcaEXQQGkKJX9bt4', 1495010829),
(2, 2, 'k6hDSUu6Mx8jUCvj0PzCYNlKIMT8sPjQrzAcvfm8', 1495010892),
(3, 3, 'cXvEEjKmPk48aMgNYlqI3pzPwzXqGCbc8Lco7VRF', 1495010947),
(4, 4, 'AchkUKQg42R2isibsJXhiRePKSHijreczwRwfC2K', 1495011068),
(5, 5, 'ZUcsIjIiKcr33Hbb72HRYQC1IwuXdmPZR3uXcEUm', 1495011186),
(6, 6, 'TePg7IIZ4JtizsEo8eFKh7wh2QhRkvc6wa6cBQe9', 1495011237),
(7, 7, 'He9cIIKsmKoZ1NXXd82FH1n3GMIi6SfRbgwokdH3', 1495011285),
(8, 8, '2H3ISevciuLI0vnQ9myo8E82wCVgKVtQsRPxarvF', 1495011317),
(9, 9, 'TLClBciEMEu6B79AAlOISRceA7AReR8Def0Tb4G1', 1495011365),
(10, 10, 'JgkR3elIOHSYvfMnZWJtpDxMsw7V3mJtjisVQAVs', 1495012594),
(11, 11, 'JVqcb8PQaMRYMHChAyWsEEJXWkb09rJmVtHCrJgN', 1495012645),
(12, 12, 'mayilBrYr0Nfq0jD7ciW8fE8AJM7ZvQohKmNk32s', 1495012699),
(13, 13, 'UEmuzoWCkxh3TSHMkwrb3ltLcygL1xSRGs6OKduO', 1495012744),
(14, 14, '6GMfJS76Uz1hpOF7kElgtVjdzQOtnW4tGPqwv0IB', 1495012878),
(15, 15, 'sYBuH2VLBx898G0C1e5U3NP3w1hfiucru6IJfBEa', 1495013010);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_authcodes`
--

CREATE TABLE IF NOT EXISTS `oauth_session_authcodes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(10) unsigned NOT NULL,
  `auth_code` char(40) NOT NULL,
  `auth_code_expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_authcode_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_session_authcode_scopes` (
  `oauth_session_authcode_id` int(10) unsigned NOT NULL,
  `scope_id` smallint(5) unsigned NOT NULL,
  KEY `oauth_session_authcode_id` (`oauth_session_authcode_id`),
  KEY `scope_id` (`scope_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_redirects`
--

CREATE TABLE IF NOT EXISTS `oauth_session_redirects` (
  `session_id` int(10) unsigned NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_refresh_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_session_refresh_tokens` (
  `session_access_token_id` int(10) unsigned NOT NULL,
  `refresh_token` char(40) NOT NULL,
  `refresh_token_expires` int(10) unsigned NOT NULL,
  `client_id` char(40) NOT NULL,
  PRIMARY KEY (`session_access_token_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oauth_session_refresh_tokens`
--

INSERT INTO `oauth_session_refresh_tokens` (`session_access_token_id`, `refresh_token`, `refresh_token_expires`, `client_id`) VALUES
(1, 'WCyayYLtRJHVUkHBbognMIjdkVGsBcXiWhiqraFT', 1495010829, '02b501563d5df564'),
(2, '4PvJPo5O7t0ZuOTCyXxsoIOXWBK8kRSjcefNwb3Z', 1495010892, '02b501563d5df564'),
(3, 'wyW3CqKqfKrM5048dLRGQOC3Bkr6UkctSK9AjuKb', 1495010948, '02b501563d5df564'),
(4, 'QdTLag3MA5kNMfvW9GkoJKPO4HMqI49YEkmGJpX4', 1495011068, '02b501563d5df564'),
(5, 'aQJsrBMBUWa2dydynVc2AZkxcGvHG1QAVZVENi8K', 1495011186, '02b501563d5df564'),
(6, 'YLEE8ETgm4MTyagEzyAAhvMh6Ut15PARKhnC2Afy', 1495011238, '02b501563d5df564'),
(7, 'iDsaC0YxqwxTckZ25EWfcKqTxURWwFd2xwWOVdWV', 1495011286, '02b501563d5df564'),
(8, 'YeEE4C0s2tw9ptM0cJcg0nIfcJwXTI34BXaTmwpX', 1495011318, '02b501563d5df564'),
(9, 'ks9tI9AYRlAyna34dFjznDqIDBNpgJ2iujoDoYSJ', 1495011365, '02b501563d5df564'),
(10, 'pQRcufFneV7d2Ao17ywl00LDrh9FeuE2SkcTHcKF', 1495012594, '02b501563d5df564'),
(11, 'KcFMunHh150kU4yFymYGd8HY9ZeaUeIVrTl7gzpL', 1495012645, '02b501563d5df564'),
(12, 'CUCDlXUWRXYysckOTuTJUD2x0grHimxzrTusuuHr', 1495012699, '02b501563d5df564'),
(13, 'bX6d3zQ8FD5P3bEvARRCo5BTLlaVbbHWPu8ABmMi', 1495012744, '02b501563d5df564'),
(14, 'b3QEt9lziexEKmGOrBBaQEXkeLkBqUucwQM9TMrV', 1495012878, '02b501563d5df564'),
(15, 'lpq3BG7d4RP8JAXZU04u4OyF9NGZU8N9T34oaEVt', 1495013010, '02b501563d5df564');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_token_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_session_token_scopes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_access_token_id` int(10) unsigned DEFAULT NULL,
  `scope_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_setosc_setoid_scid` (`session_access_token_id`,`scope_id`),
  KEY `f_oasetosc_scid` (`scope_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE IF NOT EXISTS `site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ECPayBillsUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `7Connect_turl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `7Connect_iurl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `7Connect_merchantId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `7Connect_transactionkey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Using this name for emails',
  `site_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_email_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web_email_no_reply` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifications` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - disable, 1 - email notifications, 2 - push notifications, 3 - all',
  `country_id_default` int(11) NOT NULL,
  `salt1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ppt_percentage` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT 'Percentage of amount the referrer gets',
  `admin_id` int(11) NOT NULL,
  `cashout_charge_percentage` decimal(4,2) NOT NULL DEFAULT '0.00',
  `conversion_charge_percentage` decimal(4,2) NOT NULL DEFAULT '0.00',
  `php_backup_server` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `php_backup_clientid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `php_backup_clientsecret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tito_id` int(11) NOT NULL,
  `ppt_id` int(11) NOT NULL,
  `ppc_id` int(11) NOT NULL,
  `ppr_id` int(11) NOT NULL,
  `ddr_id` int(11) NOT NULL,
  `commission_id` int(11) NOT NULL,
  `wallet_id_default` int(11) DEFAULT NULL,
  `twilio_sid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twilio_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twilio_from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referral_amount` decimal(4,2) NOT NULL DEFAULT '0.00',
  `wallet_transfer_monthly_limit` int(11) NOT NULL DEFAULT '100000',
  `blockchain_guid` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `blockchain_main_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `blockchain_xpub` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `blockchain_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ECPayUrl` text COLLATE utf8_unicode_ci NOT NULL,
  `ECPayAccountId` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ECPayUsername` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ECPayPassword` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ECPayBranchId` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `DragonpayAccountId` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `DragonpayPassword` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `DragonpayUrl` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Stripe_Client_Secret` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `Stripe_Client_ID` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `Stripe_Publishable_Key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `paypal_apiUsername` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `paypal_apiPassword` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `paypal_apiSignature` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `xend_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'http://v2.xend.com.ph/',
  `xend_appid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'caa2243af7f2d719ebca9551af6d8db04adfb9ee',
  `xend_account_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01061600001',
  `xend_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '$2y$10$BVfjT3KNSixwnu9cY9jd6ulaRKM9lgPmrqCOU2LI44Ozl9VywhF22',
  `gcm_api_key` text COLLATE utf8_unicode_ci NOT NULL,
  `blockchain_pesos_user_id` int(11) NOT NULL,
  `blockchain_pesos_user_type` int(1) NOT NULL,
  `mobile_payment_commnity_id` int(11) NOT NULL COMMENT '1208 will be community id will recive payments , while using credit/Paybills call',
  `goferPaymentCommunityId` int(11) NOT NULL DEFAULT '1351',
  PRIMARY KEY (`id`),
  KEY `site_settings_ibfk_countries` (`country_id_default`),
  KEY `site_settings_ibfk_administrators` (`admin_id`),
  KEY `site_settings_ibfk_tito_community` (`tito_id`),
  KEY `site_settings_ibfk_ppt_community` (`ppt_id`),
  KEY `site_settings_ibfk_ppc_community` (`ppc_id`),
  KEY `site_settings_ibfk_ppr_community` (`ppr_id`),
  KEY `site_settings_ibfk_ddr_community` (`ddr_id`),
  KEY `site_settings_ibfk_commission_community` (`commission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `ECPayBillsUrl`, `7Connect_turl`, `7Connect_iurl`, `7Connect_merchantId`, `7Connect_transactionkey`, `name`, `site_url`, `api_url`, `admin_email`, `admin_email_password`, `web_email`, `web_email_no_reply`, `notifications`, `country_id_default`, `salt1`, `salt2`, `ppt_percentage`, `admin_id`, `cashout_charge_percentage`, `conversion_charge_percentage`, `php_backup_server`, `php_backup_clientid`, `php_backup_clientsecret`, `tito_id`, `ppt_id`, `ppc_id`, `ppr_id`, `ddr_id`, `commission_id`, `wallet_id_default`, `twilio_sid`, `twilio_token`, `twilio_from`, `referral_amount`, `wallet_transfer_monthly_limit`, `blockchain_guid`, `blockchain_main_password`, `blockchain_xpub`, `blockchain_key`, `ECPayUrl`, `ECPayAccountId`, `ECPayUsername`, `ECPayPassword`, `ECPayBranchId`, `DragonpayAccountId`, `DragonpayPassword`, `DragonpayUrl`, `Stripe_Client_Secret`, `Stripe_Client_ID`, `Stripe_Publishable_Key`, `paypal_apiUsername`, `paypal_apiPassword`, `paypal_apiSignature`, `xend_url`, `xend_appid`, `xend_account_no`, `xend_password`, `gcm_api_key`, `blockchain_pesos_user_id`, `blockchain_pesos_user_type`, `mobile_payment_commnity_id`, `goferPaymentCommunityId`) VALUES
(1, 'http://ecpay.dyndns-web.com/ecpnbillspayment/Service1.asmx?WSDL', 'https://pay.7-eleven.com.ph/transact', 'https://pay.7-eleven.com.ph/inquire', 'tagcash', '7', 'Tagcash', 'http://tagcash.com', 'https://api.tagcash.com', 'info@tagcash.com', '7', 'info@tagcash.com', 'info@tagcash.com', 3, 236, '7', '7', 70.00, 8, 1.00, 0.00, 'http://122.53.166.82/tcash_api', '7', '7', 713, 703, 705, 706, 707, 2, NULL, '7', '7', '+15409310049', 0.00, 1000000, '7-eac4-417b-bbc1-7af49e8e71b1', 'r3dumbr3ll@12', '7', '3bde33e2-49b3-4962-9296-a0a78b5b72b1', 'http://ecpay.dyndns-web.com/wstopup/WSTopUp.asmx?WSDL', '1107', 'TL_REG', 'reDcIxcJ', '8865', 'TAGCASH', 'Y5eDF3j4', 'http://test.dragonpay.ph/', '7', '7', 'pk_test_qMh9MUiAppPFpC87HXOkjT1Pz7', '7', '7', '7', 'http://v2.xend.com.ph/', '7', '01061600001', '7', '7', 0, 3, 7, 7);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_nickname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_gender` int(1) DEFAULT NULL COMMENT '1 = Male, 2 = female, 3 = ts',
  `user_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_altemail` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_emailtemp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_emailchangecode` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_verified` int(1) NOT NULL DEFAULT '0' COMMENT '1=email verified 	2=sms verified 	4=passport verified 8=utility bill 16=KYC',
  `user_activationkey` varchar(128) COLLATE utf8_unicode_ci DEFAULT '',
  `user_forgotcode` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_smscode` int(5) DEFAULT NULL COMMENT 'SMS verification code the user will be receiving via sms',
  `user_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_status` int(1) NOT NULL DEFAULT '1' COMMENT '0 = banned, 1 = active, 2 = inactive',
  `user_dob` date DEFAULT NULL,
  `user_mobile` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_altmobile` int(11) DEFAULT NULL,
  `user_address1` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_address2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_city` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_state` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT '236',
  `timezone_id` int(11) NOT NULL DEFAULT '19',
  `user_zipcode` int(5) DEFAULT NULL,
  `user_registeredip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_loginstatus` int(1) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `user_autoadd` int(1) NOT NULL DEFAULT '0' COMMENT '0 = ppl need my approval for adding me. 1= can add me automatically',
  `user_pin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_credits` decimal(19,2) NOT NULL DEFAULT '0.00' COMMENT 'Credits this guy has',
  `user_referrer` int(11) DEFAULT NULL COMMENT 'Id of the user / community who referred this user',
  `user_referrertype` int(1) DEFAULT NULL COMMENT '1 = User, 2 = Community',
  `user_referrer_rewardstatus` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- paid, 0-not paid ;paid on successfull user mobile verification',
  `user_defaultcommunity_id` int(11) DEFAULT NULL COMMENT 'If the user wants to automatically login as a community. The community id can be specified here',
  `user_reseller` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=reseller',
  `profile_id` int(11) DEFAULT NULL COMMENT 'Foreign key from socialinfo',
  `user_allow_charging` int(1) NOT NULL DEFAULT '0',
  `user_allowphoto` int(1) NOT NULL DEFAULT '0' COMMENT '1=enable sharing of photo; 0=disable sharing of photo',
  `membership_plan` int(1) NOT NULL DEFAULT '0' COMMENT '0=bronze; 1=silver; 2=gold',
  `wallet_enabled` int(1) DEFAULT NULL,
  `user_autoapprove_rfid` int(11) NOT NULL DEFAULT '0',
  `user_smsverification_status` tinyint(1) NOT NULL DEFAULT '0',
  `user_smscode_date_last` timestamp NULL DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `access_status` int(11) NOT NULL DEFAULT '1' COMMENT '0=private; 1=public',
  `user_rating` float NOT NULL DEFAULT '0',
  `chef` int(11) NOT NULL DEFAULT '0',
  `kyc_reward` int(11) NOT NULL DEFAULT '0',
  `registration_method` int(11) DEFAULT NULL COMMENT '1=TAG; 2=Facebook; 3=Google',
  `userkyc_date_verified` date DEFAULT NULL,
  `player_points` double NOT NULL DEFAULT '0',
  `matchmaker` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `load_discount` decimal(19,2) NOT NULL DEFAULT '0.00',
  `last_game_slot_played` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'UTC time in millisecond',
  `keyword` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `pre_approved` int(11) NOT NULL DEFAULT '0' COMMENT '	1=pre approved, 0=not pre approved',
  `fb_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'chatfule fb_id purpose',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`user_email`),
  UNIQUE KEY `user_mobile` (`user_mobile`),
  KEY `status` (`user_status`),
  KEY `country_id` (`country_id`),
  KEY `timezone_id` (`timezone_id`),
  KEY `user_referrer` (`user_referrer`),
  KEY `user_defaultcommunity_id` (`user_defaultcommunity_id`),
  KEY `social_id` (`profile_id`),
  KEY `users_ibfk_states` (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Details of all users except the site admins' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_firstname`, `user_lastname`, `user_nickname`, `user_gender`, `user_password`, `user_email`, `user_altemail`, `user_emailtemp`, `user_emailchangecode`, `user_verified`, `user_activationkey`, `user_forgotcode`, `user_smscode`, `user_created_at`, `user_status`, `user_dob`, `user_mobile`, `user_altmobile`, `user_address1`, `user_address2`, `user_city`, `user_state`, `country_id`, `timezone_id`, `user_zipcode`, `user_registeredip`, `user_loginstatus`, `last_login`, `user_autoadd`, `user_pin`, `user_credits`, `user_referrer`, `user_referrertype`, `user_referrer_rewardstatus`, `user_defaultcommunity_id`, `user_reseller`, `profile_id`, `user_allow_charging`, `user_allowphoto`, `membership_plan`, `wallet_enabled`, `user_autoapprove_rfid`, `user_smsverification_status`, `user_smscode_date_last`, `state_id`, `access_status`, `user_rating`, `chef`, `kyc_reward`, `registration_method`, `userkyc_date_verified`, `player_points`, `matchmaker`, `latitude`, `longitude`, `load_discount`, `last_game_slot_played`, `keyword`, `online`, `pre_approved`, `fb_id`) VALUES
(1, 'deepu', 'thomas', '', 1, 'c707931fea026ca0657199539652cd310edd8f46caaffa7ee6fc67d84fb9aa95fd12fa6f851f28012c3017ee3deef372b365a59c77d44289cefedb77dd8c7c3d', 'deeputhomas@test.net', NULL, NULL, NULL, 0, '5278e47ef76742921119411ee6c3bdf3edd8d63a', NULL, 26351, '2016-09-15 11:25:27', 1, '0000-00-00', '12345678901553', NULL, '', NULL, NULL, NULL, 174, 115, NULL, '127.0.0.1', NULL, '2017-01-05 04:17:41', 0, NULL, 0.00, NULL, NULL, 0, 1453, 0, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, 1, NULL, 0, '0', NULL, NULL, 0.00, NULL, NULL, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_preferences`
--

CREATE TABLE IF NOT EXISTS `user_preferences` (
  `user_id` int(11) NOT NULL,
  `pref_pin_login` int(1) NOT NULL DEFAULT '0' COMMENT '1 - enable pin login',
  `pref_wallet_id` int(11) DEFAULT NULL,
  `pref_currency` int(1) NOT NULL DEFAULT '0' COMMENT '0 - reward points, 1 - wallet',
  `pref_wallet_cash_in` decimal(19,8) NOT NULL DEFAULT '0.00000000',
  `pref_wallet_cash_out` decimal(19,8) NOT NULL DEFAULT '0.00000000',
  `pref_isdcode` int(11) NOT NULL DEFAULT '63',
  `save_prev_login` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_preferences`
--

INSERT INTO `user_preferences` (`user_id`, `pref_pin_login`, `pref_wallet_id`, `pref_currency`, `pref_wallet_cash_in`, `pref_wallet_cash_out`, `pref_isdcode`, `save_prev_login`) VALUES
(1, 0, 1, 0, 0.00000000, 0.00000000, 63, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_balances`
--

CREATE TABLE IF NOT EXISTS `wallet_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `balance_id` int(11) NOT NULL,
  `balance_type` int(1) NOT NULL COMMENT '1=user, 2=community, 3=System',
  `balance_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `balance_amount` decimal(19,8) NOT NULL DEFAULT '0.00000000',
  `balance_promised` decimal(19,8) unsigned DEFAULT '0.00000000' COMMENT 'This is the uncleared or amount in escrow. Should never be more than the balance_amount',
  `wallet_type_id` int(3) NOT NULL,
  `masterbank_id` int(11) NOT NULL DEFAULT '1',
  `auto_convert` int(11) NOT NULL DEFAULT '0',
  `auto_convert_to_wallet` int(11) DEFAULT NULL,
  `receiving_address` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`balance_id`,`balance_type`,`wallet_type_id`),
  UNIQUE KEY `id` (`id`),
  KEY `wallet_type_id` (`wallet_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13447 ;

--
-- Dumping data for table `wallet_balances`
--

INSERT INTO `wallet_balances` (`id`, `balance_id`, `balance_type`, `balance_created_at`, `balance_amount`, `balance_promised`, `wallet_type_id`, `masterbank_id`, `auto_convert`, `auto_convert_to_wallet`, `receiving_address`) VALUES
(13446, 1, 1, '2017-05-10 08:53:58', 0.00000000, 0.00000000, 1, 1, 0, NULL, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
