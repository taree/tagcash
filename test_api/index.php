<?php
// phpinfo();
# require 'maintenance.php'; exit;
date_default_timezone_set('Asia/Manila');

require dirname(__FILE__).'/protected/extensions/vendor/autoload.php';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

require dirname(__FILE__).'/protected/extensions/vendor/autoload.php';

if(file_exists(dirname(__FILE__).'/override.php')){
	include dirname(__FILE__).'/override.php';
}

// change the following paths if necessary
$yii=dirname(__FILE__).'/../../yii1/yii-1.1.16/yii.php';

if(file_exists(dirname(__FILE__).'/protected/config/main_production.php')){
	$config=dirname(__FILE__).'/protected/config/main_production.php';
        
}
elseif(file_exists(dirname(__FILE__).'/protected/config/main_dev.php')){
	$config=dirname(__FILE__).'/protected/config/main_dev.php';
// remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG',true);
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
}

else{
	$config=dirname(__FILE__).'/protected/config/main.php';
}



require_once($yii);
Yii::createWebApplication($config)->run();
